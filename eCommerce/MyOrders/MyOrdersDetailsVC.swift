//
//  MyOrdersDetailsVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 16/08/21.
//

import UIKit
import SwiftyJSON
import SDWebImage

class MyOrdersDetailsVC: UIViewController {
    
    // Order History details
    @IBOutlet weak var lbl_orderDate: UILabel!
    @IBOutlet weak var lbl_PaymentType: UILabel!
    @IBOutlet weak var lbl_OrderNo: UILabel!
    
    @IBOutlet weak var Tableview_OrderListing: UITableView!
    @IBOutlet weak var lbl_ShipingAddress: UILabel!
    @IBOutlet weak var lbl_Notes: UILabel!
    @IBOutlet weak var Height_Tableview: NSLayoutConstraint!
    
    
    @IBOutlet weak var lbl_discountString: UILabel!
    @IBOutlet weak var lbl_Total: UILabel!
    @IBOutlet weak var lbl_Discount: UILabel!
    @IBOutlet weak var lbl_ShippingCharge: UILabel!
    @IBOutlet weak var lbl_tax: UILabel!
    @IBOutlet weak var lbl_Subtotal: UILabel!
    
    @IBOutlet weak var lbl_note: UILabel!
    @IBOutlet weak var View_Note: CornerView!
    @IBOutlet weak var height_Notes: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lblstr_billingaddress: UILabel!
    @IBOutlet weak var lblstr_Orderinfo: UILabel!
    @IBOutlet weak var lblstr_Subtotal: UILabel!
    @IBOutlet weak var lblstr_Tax: UILabel!
    @IBOutlet weak var lblstr_Shipping: UILabel!
    @IBOutlet weak var lblstr_Total: UILabel!
    @IBOutlet weak var lblstr_note: UILabel!
    var OrderHistoryData = [String:String]()
    var OrderListArray = [JSON]()
    var order_number = String()
    var Cancelorder_id = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "Order Details".localiz()
        self.lblstr_billingaddress.text = "Billing / Shiping address".localiz()
        self.lblstr_Orderinfo.text = "Order Info".localiz()
        self.lblstr_Subtotal.text = "Subtotal".localiz()
        self.lblstr_Tax.text = "Tax".localiz()
        self.lblstr_Shipping.text = "Shipping".localiz()
        self.lblstr_Total.text = "Total".localiz()
        self.lblstr_note.text = "Note".localiz()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let urlString = API_URL + "orderdetails"
        let params: NSDictionary = ["order_number":order_number]
        self.Webservice_OrderDetails(url: urlString, params: params)
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
    
}
//MARK: Tableview Methods
extension MyOrdersDetailsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rect = CGRect(origin: CGPoint(x: 0,y: 0), size: CGSize(width: self.Tableview_OrderListing.bounds.size.width, height: self.Tableview_OrderListing.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.textColor = UIColor.lightGray
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "POPPINS-REGULAR", size: 15)!
        messageLabel.sizeToFit()
        self.Tableview_OrderListing.backgroundView = messageLabel
        if self.OrderListArray.count == 0 {
            messageLabel.text = "No data found".localiz()
        }
        else {
            messageLabel.text = ""
        }
        return self.OrderListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 155
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_OrderListing.dequeueReusableCell(withIdentifier: "CartCell") as! CartCell
        
        cornerRadius(viewName: cell.img_Product, radius: 4.0)
        let data = self.OrderListArray[indexPath.row]
        let ItemPrice = data["qty"].doubleValue * data["price"].doubleValue
        let Qty_Price = formatter.string(for: ItemPrice)!
        let shipping_cost = formatter.string(for: data["shipping_cost"].stringValue.toDouble)!
        let tax = formatter.string(for: data["tax"].stringValue.toDouble)!
        //let discount_amount = formatter.string(for: data["discount_amount"].stringValue.toDouble)!
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
        {
            cell.lbl_Price.text = "\(Qty_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
            cell.lbl_ShipingCost.text = "\(shipping_cost)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
            cell.lbl_Tax.text = "\(tax)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
           // cell.lbl_Discount.text = "\(discount_amount)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
        }
        else {
            
            cell.lbl_Price.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Qty_Price)"
            cell.lbl_ShipingCost.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(shipping_cost)"
            cell.lbl_Tax.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(tax)"
           // cell.lbl_Discount.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(discount_amount)"
            
        }
        
        cell.lbl_ProductName.text = data["product_name"].stringValue
        
        if data["variation"].stringValue == ""
        {
            cell.lbl_Weight.text = " - "
        }
        else{
            cell.lbl_Weight.text = "\(data["attribute"].stringValue) : \(data["variation"].stringValue)"
        }
        
        cell.lbl_qty.text = "Qty : \(data["qty"].stringValue)"
        cell.img_Product.sd_imageTransition = .fade
        let items = indexPath.item % 6
        cell.img_Product.sd_setImage(with: URL(string: data["image_url"].stringValue), placeholderImage: UIImage(named: "b_0\(items)"))
        if data["status"].stringValue == "5"
        {
            cell.lbl_Cancel.text = "Item Cancelled"
            cell.lbl_Cancel.isHidden = false
        }
        else if data["status"].stringValue == "7"
        {
            cell.lbl_Cancel.text = "Return Request"
            cell.lbl_Cancel.isHidden = false
        }
        else{
            cell.lbl_Cancel.isHidden = true
        }
        
        return cell
    }
        func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
            let data = self.OrderListArray[indexPath.row]
            if data["status"].stringValue == "5" || data["status"].stringValue == "7"
            {
                return false
            }
            else{
                return true
            }
        }
    
        func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
            let data = self.OrderListArray[indexPath.row]
//            let DeleteItem = UIContextualAction(style: .normal, title: "Cancel") {  (contextualAction, view, boolValue) in
//
//                let alertVC = UIAlertController(title: "", message: "Are you sure you want to cancel item?".localiz(), preferredStyle: .alert)
//                let yesAction = UIAlertAction(title: "Yes".localiz(), style: .default) { (action) in
//
//                }
//                let noAction = UIAlertAction(title: "No".localiz(), style: .destructive)
//                alertVC.addAction(noAction)
//                alertVC.addAction(yesAction)
//                self.present(alertVC,animated: true,completion: nil)
//
//            }
            let MoreItem = UIContextualAction(style: .normal, title: "More") {  (contextualAction, view, boolValue) in
                self.ActionSheet(data: data)
            }
//            DeleteItem.backgroundColor = UIColor.red
            let swipeActions = UISwipeActionsConfiguration(actions: [MoreItem])
    
            return swipeActions
        }
    
    func ActionSheet(data:JSON){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Cancel Item", style: .default , handler:{ (UIAlertAction)in
            self.Cancelorder_id = data["id"].stringValue
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConformationAlertVC") as! ConformationAlertVC
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.deleget = self
            vc.isSelectedTypes = "4"
            self.present(vc,animated: true,completion: nil)
         }))
     
        alert.addAction(UIAlertAction(title: "Track Order", style: .default , handler:{ (UIAlertAction)in
            let vc = self.storyboard?.instantiateViewController(identifier: "TrackOrderVC") as! TrackOrderVC
            vc.order_id = data["id"].stringValue
            vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
        }))
        if data["status"].stringValue == "4"
        {
            alert.addAction(UIAlertAction(title: "Return Request", style: .default , handler:{ (UIAlertAction)in
                let vc = self.storyboard?.instantiateViewController(identifier: "ReturnOrderVC") as! ReturnOrderVC
                vc.order_id = data["id"].stringValue
                vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
            }))
        }
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
           
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = OrderListArray[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
        vc.item_id = data["product_id"].stringValue
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
}
extension MyOrdersDetailsVC
{
    func Webservice_OrderDetails(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let ResponseData = jsonResponse!["order_info"].dictionaryValue
                    let tax_Price = formatter.string(for: ResponseData["tax"]!.stringValue.toDouble)!
                    let subtotal_Price = formatter.string(for: ResponseData["subtotal"]!.stringValue.toDouble)!
                    let shipping_cost_Price = formatter.string(for: ResponseData["shipping_cost"]!.stringValue.toDouble)!
                    let grand_total = formatter.string(for: ResponseData["grand_total"]!.stringValue.toDouble)!
                    let discount_amount = formatter.string(for: ResponseData["discount_amount"]!.stringValue.toDouble)!
                    
                    let coupon_name = ResponseData["coupon_name"]!.stringValue
                    
                    if ResponseData["coupon_name"]!.stringValue == ""
                    {
                        self.lbl_discountString.text = "Discount"
                    }
                    else{
                        self.lbl_discountString.text = "Discount (\(coupon_name))"
                    }
                    
                    self.lbl_ShipingAddress.text! = "\(ResponseData["full_name"]!.stringValue) \n \(ResponseData["street_address"]!.stringValue) \(ResponseData["landmark"]!.stringValue)- \(ResponseData["pincode"]!.stringValue)\n\(ResponseData["mobile"]!.stringValue)\n\(ResponseData["email"]!.stringValue)"
                    
                    if ResponseData["order_notes"]!.stringValue == ""
                    {
//                        self.height_Notes.constant = 0.0
                        self.lbl_note.isHidden = true
                        self.View_Note.isHidden = true
                    }
                    else{
//                        self.height_Notes.constant = 100.0
                        self.lbl_note.isHidden = false
                        self.View_Note.isHidden = false
                        self.lbl_Notes.text = ResponseData["order_notes"]!.stringValue
                    }
                    
                    self.lbl_OrderNo.text = "Order ID : \(ResponseData["order_number"]!.stringValue)"
                    if ResponseData["payment_type"]!.stringValue == "1"
                    {
                        self.lbl_PaymentType.text = "Payment Type : Cash"
                    }
                    else if ResponseData["payment_type"]!.stringValue == "2"
                    {
                        self.lbl_PaymentType.text = "Payment Type : Wallet"
                    }
                    else if ResponseData["payment_type"]!.stringValue == "3"
                    {
                        self.lbl_PaymentType.text = "Payment Type : RazorPay"
                    }
                    else if ResponseData["payment_type"]!.stringValue == "4"
                    {
                        self.lbl_PaymentType.text = "Payment Type : Stripe"
                    }
                    else if ResponseData["payment_type"]!.stringValue == "5"
                    {
                        self.lbl_PaymentType.text = "Payment Type : Flutterwave"
                    }
                    else if ResponseData["payment_type"]!.stringValue == "6"
                    {
                        self.lbl_PaymentType.text = "Payment Type : Paystack"
                    }
                    
                    let setdate = DateFormater.getBirthDateStringFromDateString(givenDate:ResponseData["date"]!.stringValue)
                    self.lbl_orderDate.text = setdate
                    
                    if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                    {
                        
                        self.lbl_tax.text = "\(tax_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                        self.lbl_Subtotal.text = "\(subtotal_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                        self.lbl_ShippingCharge.text = "\(shipping_cost_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                        
                        
                        if ResponseData["coupon_name"]!.stringValue == ""
                        {
                            self.lbl_Discount.text = "-0.00\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                            self.lbl_Total.text = "\(grand_total)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                        }
                        else
                        {
                            self.lbl_Discount.text = "-\(discount_amount)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                            self.lbl_Total.text = "\(grand_total)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                        }
                        
                        
                    }
                    else {
                        
                        self.lbl_tax.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(tax_Price)"
                        self.lbl_Subtotal.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(subtotal_Price)"
                        self.lbl_ShippingCharge.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(shipping_cost_Price)"
                        if ResponseData["coupon_name"]!.stringValue == ""
                        {
                            self.lbl_Discount.text = "-\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))0.00"
                            self.lbl_Total.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(grand_total)"
                        }
                        else
                        {
                            self.lbl_Discount.text = "-\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(discount_amount)"
                            self.lbl_Total.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(grand_total)"
                        }
                        
                    }
                    
                    self.OrderListArray = jsonResponse!["order_data"].arrayValue
                    if self.OrderListArray.count == 0
                    {
                        self.Height_Tableview.constant = 155
                    }
                    else
                    {
                        self.Height_Tableview.constant = CGFloat(self.OrderListArray.count * 155)
                    }
                    self.Tableview_OrderListing.delegate = self
                    self.Tableview_OrderListing.dataSource = self
                    self.Tableview_OrderListing.reloadData()
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_ItemCancel(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let urlString = API_URL + "orderdetails"
                    let params: NSDictionary = ["order_number":self.order_number]
                    self.Webservice_OrderDetails(url: urlString, params: params)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
extension MyOrdersDetailsVC : DismissAlertDeleget
{
    func DimissSucess() {
        let urlString = API_URL + "cancelorder"
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"order_id":self.Cancelorder_id,"status":"5"]
        self.Webservice_ItemCancel(url: urlString, params: params)
    }
    
    
}
