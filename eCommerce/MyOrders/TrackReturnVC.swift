//
//  TrackReturnVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 23/10/21.
//

import UIKit
import SwiftyJSON
import SDWebImage

class TrackReturnVC: UIViewController {
    
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_qty: UILabel!
    @IBOutlet weak var lbl_size: UILabel!
    @IBOutlet weak var lbl_itemName: UILabel!
    @IBOutlet weak var img_item: UIImageView!
    
    @IBOutlet weak var img_circle_1: UIImageView!
    @IBOutlet weak var img_circle_2: UIImageView!
    @IBOutlet weak var img_circle_3: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    
    
    
    @IBOutlet weak var lbl_Line_1: UILabel!
    @IBOutlet weak var lbl_Line_2: UILabel!
    
    
    @IBOutlet weak var lbl_Date_1: UILabel!
    @IBOutlet weak var lbl_Date_2: UILabel!
    @IBOutlet weak var lbl_Date_3: UILabel!
    
    @IBOutlet weak var lbl_trackOrderid: UILabel!
    var order_id = String()
    
    @IBOutlet weak var lbl_subtitle1: UILabel!
    @IBOutlet weak var lbl_Title1: UILabel!
    
    @IBOutlet weak var lbl_subtitle2: UILabel!
    @IBOutlet weak var lbl_Title2: UILabel!
    
    @IBOutlet weak var lbl_subtitle3: UILabel!
    @IBOutlet weak var lbl_Title3: UILabel!
    
    @IBOutlet weak var lbl_strReturn: UILabel!
    @IBOutlet weak var lbl_Returncomment: UILabel!
    var vendor_id = String()
    var product_id = String()
    var product_image = String()
    var productName = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "Track Order".localiz()
        self.lbl_Title1.text = "Return request created".localiz()
        self.lbl_subtitle1.text = "We have received your order".localiz()
        self.lbl_Title2.text = "Order return accepted".localiz()
        self.lbl_subtitle2.text = "You order has been accepted".localiz()
        
        self.lbl_Title3.text = "Order return completed".localiz()
        self.lbl_subtitle3.text = "Your package off for completed".localiz()
        
        self.lbl_strReturn.text = "Vendor Comment".localiz()
        
        self.lbl_Returncomment.isHidden = true
        self.lbl_strReturn.isHidden = true
        cornerRadius(viewName: self.img_item, radius: 4.0)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let urlString = API_URL + "trackorder"
        let params: NSDictionary = ["order_id":self.order_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
        self.Webservice_Ordertrack(url: urlString, params: params)
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
}
extension TrackReturnVC
{
    func Webservice_Ordertrack(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let ResponseData = jsonResponse!["order_info"].dictionaryValue
                    self.product_id = ResponseData["product_id"]!.stringValue
                    self.vendor_id = ResponseData["vendor_id"]!.stringValue
                    self.productName = ResponseData["product_name"]!.stringValue
                    self.product_image = ResponseData["image_url"]!.stringValue
                    
                    let ItemPrice = ResponseData["qty"]!.doubleValue * ResponseData["price"]!.doubleValue
                    let Qty_Price = formatter.string(for: ItemPrice)!
                    if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                    {
                        self.lbl_Price.text = "\(Qty_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    }
                    else {
                        
                        self.lbl_Price.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Qty_Price)"
                    }
                    
                    self.lbl_itemName.text = ResponseData["product_name"]!.stringValue
                    
                    if ResponseData["variation"]!.stringValue == ""
                    {
                        self.lbl_size.text = "Size : - "
                    }
                    else{
                        self.lbl_size.text = "Size : \(ResponseData["variation"]!.stringValue)"
                    }
                    self.lbl_qty.text = "Qty : \(ResponseData["qty"]!.stringValue)"
                    self.img_item.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    self.img_item.sd_setImage(with: URL(string: ResponseData["image_url"]!.stringValue), placeholderImage: UIImage(named: "ic_placeholder"))
                    if ResponseData["created_at"]!.stringValue != ""
                    {
                        let date_1 = DateFormater.getFullDateStringFromString(givenDate:ResponseData["created_at"]!.stringValue)
                        self.lbl_Date_1.text = date_1
                    }
                    else{
                        self.lbl_Date_1.text = ""
                    }
                    
                    if ResponseData["confirmed_at"]!.stringValue != ""
                    {
                        let date_2 = DateFormater.getFullDateStringFromString(givenDate:ResponseData["confirmed_at"]!.stringValue)
                        self.lbl_Date_2.text = date_2
                    }
                    else{
                        self.lbl_Date_2.text = ""
                    }
                    if ResponseData["shipped_at"]!.stringValue != ""
                    {
                        let date_3 = DateFormater.getFullDateStringFromString(givenDate:ResponseData["shipped_at"]!.stringValue)
                        self.lbl_Date_3.text = date_3
                    }
                    else{
                        self.lbl_Date_3.text = ""
                    }
                    
                    if ResponseData["status"]!.stringValue == "7"
                    {
                        self.img_circle_1.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_2.image = UIImage(systemName: "circle")
                        self.img_circle_3.image = UIImage(systemName: "circle")
                       
                        
                        self.lbl_Line_1.backgroundColor = UIColor.lightGray
                        self.lbl_Line_2.backgroundColor = UIColor.lightGray
                        
                        
                    }
                    else if ResponseData["status"]!.stringValue == "8"
                    {
                        self.img_circle_1.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_2.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_3.image = UIImage(systemName: "circle")
                        
                        self.lbl_Line_1.backgroundColor = UIColor.systemGreen
                        self.lbl_Line_2.backgroundColor = UIColor.systemGreen
                        
                        
                    }
                    else if ResponseData["status"]!.stringValue == "9"
                    {
                        self.img_circle_1.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_2.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_3.image = UIImage(systemName: "circle.circle.fill")
                        
                        self.lbl_Line_1.backgroundColor = UIColor.systemGreen
                        self.lbl_Line_2.backgroundColor = UIColor.systemGreen
                        
                        
                    }
                    else if ResponseData["status"]!.stringValue == "10"
                    {
                        self.img_circle_1.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_2.image = UIImage(systemName: "circle")
                        self.img_circle_3.image = UIImage(systemName: "circle")
                        
                        self.lbl_Line_1.backgroundColor = UIColor.lightGray
                        self.lbl_Line_2.backgroundColor = UIColor.lightGray
                        
                        self.lbl_Title2.text! = "Order return rejected"
                        self.lbl_subtitle2.text! = "You order has been rejected"
                        self.lbl_Title2.textColor = UIColor.systemRed
                        
                    }
                    if ResponseData["vendor_comment"]!.stringValue == ""
                    {
                        self.lbl_Returncomment.isHidden = true
                        self.lbl_strReturn.isHidden = true
                    }
                    else{
                        
                        self.lbl_Returncomment.isHidden = false
                        self.lbl_strReturn.isHidden = false
                        self.lbl_Returncomment.text = ResponseData["vendor_comment"]!.stringValue
                    }
                    
                    self.lbl_trackOrderid.text = "Return Order id #\(ResponseData["return_number"]!.stringValue)"
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
