//
//  ReturnOrderVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 22/10/21.
//

import UIKit
import SDWebImage
import SwiftyJSON
class ReturnListCell: UITableViewCell {
    
    @IBOutlet weak var img_checkmark: UIImageView!
    @IBOutlet weak var lbl_reson: UILabel!
}
class ReturnOrderVC: UIViewController {

    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_qty: UILabel!
    @IBOutlet weak var lbl_size: UILabel!
    @IBOutlet weak var lbl_itemName: UILabel!
    @IBOutlet weak var img_item: UIImageView!
    
    @IBOutlet weak var lblstr_comment: UILabel!
    @IBOutlet weak var lblstr_returningtime: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var text_comment: UITextView!
    @IBOutlet weak var TableView_ReturnresonList: UITableView!
    @IBOutlet weak var btn_returnrequest: UIButton!
    var order_id = String()
    var vendor_id = String()
    var product_id = String()
    var product_image = String()
    var productName = String()
    var reasonList = [[String:String]]()
    var isSelectreson = String()
    var ReturnRequest = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "Return Request".localiz()
        self.lblstr_comment.text = "comments".localiz()
        self.lblstr_returningtime.text = "Why are you returning this item?".localiz()
        self.btn_returnrequest.setTitle("Submit Return Request".localiz(), for: .normal)
        
        self.isSelectreson = "0"
        cornerRadius(viewName: self.img_item, radius: 4.0)
        self.text_comment.delegate = self
        self.text_comment.text = "Write Comments (Optional)".localiz()
        self.text_comment.textColor = UIColor.lightGray
        setBorder(viewName: text_comment, borderwidth: 1, borderColor: UIColor.lightGray.cgColor, cornerRadius: 6.0)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let urlString = API_URL + "returnconditions"
        let params: NSDictionary = ["order_id":self.order_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
        self.Webservice_ReturnRequestDetails(url: urlString, params: params)
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    @IBAction func btnTap_SubmitRequest(_ sender: UIButton) {
        if self.isSelectreson == "0"
        {
            showAlertMessage(titleStr: "", messageStr: "Please select returning request reason")
        }
        else
        {
            if self.text_comment.text == "Write Comments (Optional)"
            {
                self.text_comment.text = ""
            }
            let urlString = API_URL + "returnrequest"
            let params: NSDictionary = ["order_id":self.order_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"return_reason":self.ReturnRequest,"comment":self.text_comment.text!,"status":"7"]
            self.Webservice_SubmitReturnRequest(url: urlString, params: params)
        }
    }
    
}
extension ReturnOrderVC : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write Comments (Optional)"
            textView.textColor = UIColor.lightGray
        }
    }
}
//MARK: Tableview Methods
extension ReturnOrderVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reasonList.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TableView_ReturnresonList.dequeueReusableCell(withIdentifier: "ReturnListCell") as! ReturnListCell
        let data = self.reasonList[indexPath.row]
        if data["isSelect"]! == "0"
        {
            cell.img_checkmark.isHidden = true
        }
        else{
            cell.img_checkmark.isHidden = false
            self.isSelectreson = "1"
            self.ReturnRequest = data["return_conditions"]!
        }
        cell.lbl_reson.text = data["return_conditions"]!
        return cell
    }
 

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for i in 0..<self.reasonList.count {
            var payment = self.reasonList[i]
            payment["isSelect"] = "0"
            self.reasonList.remove(at: i)
            self.reasonList.insert(payment, at: i)
        }
        var data = self.reasonList[indexPath.row]
        data["isSelect"]! = "1"
        self.reasonList.remove(at: indexPath.row)
        self.reasonList.insert(data, at: indexPath.row)
        
        self.TableView_ReturnresonList.reloadData()
    }
    
}
extension ReturnOrderVC
{
    func Webservice_ReturnRequestDetails(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let ResponseData = jsonResponse!["order_info"].dictionaryValue
                    self.product_id = ResponseData["product_id"]!.stringValue
                    self.vendor_id = ResponseData["vendor_id"]!.stringValue
                    self.productName = ResponseData["product_name"]!.stringValue
                    self.product_image = ResponseData["image_url"]!.stringValue
                    
                    let ItemPrice = ResponseData["qty"]!.doubleValue * ResponseData["price"]!.doubleValue
                    let Qty_Price = formatter.string(for: ItemPrice)!
                    if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                    {
                        self.lbl_Price.text = "\(Qty_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    }
                    else {
                        
                        self.lbl_Price.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Qty_Price)"
                    }
                    self.lbl_itemName.text = ResponseData["product_name"]!.stringValue
                    if ResponseData["variation"]!.stringValue == ""
                    {
                        self.lbl_size.text = "Size : - "
                    }
                    else{
                        self.lbl_size.text = "Size : \(ResponseData["variation"]!.stringValue)"
                    }
                    self.lbl_qty.text = "Qty : \(ResponseData["qty"]!.stringValue)"
                    self.img_item.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    self.img_item.sd_setImage(with: URL(string: ResponseData["image_url"]!.stringValue), placeholderImage: UIImage(named: "ic_placeholder"))
                     
                    let reasondata = jsonResponse!["data"].arrayValue
                    for data in reasondata
                    {
                        let obj = ["return_conditions":data["return_conditions"].stringValue,"isSelect":"0"]
                        self.reasonList.append(obj)
                    }
                    self.TableView_ReturnresonList.delegate = self
                    self.TableView_ReturnresonList.dataSource = self
                    self.TableView_ReturnresonList.reloadData()
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_SubmitReturnRequest(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    self.dismiss(animated: false)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
