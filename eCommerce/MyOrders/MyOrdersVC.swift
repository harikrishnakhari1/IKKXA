//
//  MyOrdersVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 07/08/21.
//

import UIKit
import SwiftyJSON

class MyOrderCell: UITableViewCell {
    
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_paymentType: UILabel!
    @IBOutlet weak var lbl_mainPrice: UILabel!
    @IBOutlet weak var lbl_orderid: UILabel!
    
}
class MyOrdersVC: UIViewController {

    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var Tableview_MyOrderList: UITableView!
    var pageIndex = 1
    var lastIndex = 0
    var OrderHistoryListArray = [[String:String]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "My Order List".localiz()
        self.Tableview_MyOrderList.tableFooterView = UIView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
            appNavigation.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.windows[0].rootViewController = appNavigation
        }
        else {
            let urlString = API_URL + "orderhistory"
            let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
            self.Webservice_OrderHistory(url: urlString, params: params)
        }
        
    }
    
}

//MARK: Tableview Methods
extension MyOrdersVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)

        imagedata.contentMode = .scaleAspectFit
        self.Tableview_MyOrderList.backgroundView = imagedata
        if self.OrderHistoryListArray.count == 0 {
            imagedata.image = UIImage(named: "ic_Checklist-rafiki")
        }
        else {
            imagedata.image = UIImage(named: "")
        }
        return self.OrderHistoryListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_MyOrderList.dequeueReusableCell(withIdentifier: "MyOrderCell") as! MyOrderCell
        let data = self.OrderHistoryListArray[indexPath.row]
        
        cell.lbl_orderid.text = "Order ID : \(data["order_number"]!)"
        if data["payment_type"] == "1"
        {
            cell.lbl_paymentType.text = "Payment Type : Cash"
        }
        else if data["payment_type"] == "2"
        {
            cell.lbl_paymentType.text = "Payment Type : Wallet"
        }
        else if data["payment_type"] == "3"
        {
            cell.lbl_paymentType.text = "Payment Type : RazorPay"
        }
        else if data["payment_type"] == "4"
        {
            cell.lbl_paymentType.text = "Payment Type : Stripe"
        }
        else if data["payment_type"] == "5"
        {
            cell.lbl_paymentType.text = "Payment Type : Flutterwave"
        }
        else if data["payment_type"] == "6"
        {
            cell.lbl_paymentType.text = "Payment Type : Paystack"
        }
        
        
        
        let setdate = DateFormater.getBirthDateStringFromDateString(givenDate:data["date"]!)
        cell.lbl_date.text = setdate
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
        {
            cell.lbl_mainPrice.text = "\(data["grand_total"]!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
        }
        else {
            
            cell.lbl_mainPrice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(data["grand_total"]!)"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.OrderHistoryListArray.count - 1 {
            if self.pageIndex != self.lastIndex {
                self.pageIndex = self.pageIndex + 1
                if self.OrderHistoryListArray.count != 0 {
                    let urlString = API_URL + "orderhistory?page=\(self.pageIndex)"
                    let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                    self.Webservice_OrderHistory(url: urlString, params: params)
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.OrderHistoryListArray[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(identifier: "MyOrdersDetailsVC") as! MyOrdersDetailsVC
        vc.order_number = data["order_number"]!
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
        
    }
    
}
extension MyOrdersVC
{
    func Webservice_OrderHistory(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let ResponseData = jsonResponse!["data"].dictionaryValue
                    if self.pageIndex == 1 {
                        self.lastIndex = Int(ResponseData["last_page"]!.stringValue)!
                        self.OrderHistoryListArray.removeAll()
                    }
                    let ListData = ResponseData["data"]!.arrayValue
                    for data in ListData
                    {
                        let Price = formatter.string(for: data["grand_total"].stringValue.toDouble)!
                        let ListObj = ["id":data["id"].stringValue,"order_number":data["order_number"].stringValue,"payment_type":data["payment_type"].stringValue,"status":data["status"].stringValue,"date":data["date"].stringValue,"grand_total":Price]
                        self.OrderHistoryListArray.append(ListObj)
                    }
                    
                    self.Tableview_MyOrderList.delegate = self
                    self.Tableview_MyOrderList.dataSource = self
                    self.Tableview_MyOrderList.reloadData()
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
