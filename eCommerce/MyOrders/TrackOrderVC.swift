//
//  TrackOrderVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 10/09/21.
//

import UIKit
import SwiftyJSON
import SDWebImage

class TrackOrderVC: UIViewController {
    
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_qty: UILabel!
    @IBOutlet weak var lbl_size: UILabel!
    @IBOutlet weak var lbl_itemName: UILabel!
    @IBOutlet weak var img_item: UIImageView!
    
    @IBOutlet weak var img_circle_1: UIImageView!
    @IBOutlet weak var img_circle_2: UIImageView!
    @IBOutlet weak var img_circle_3: UIImageView!
    @IBOutlet weak var img_circle_4: UIImageView!
    
    
    @IBOutlet weak var lbl_Line_1: UILabel!
    @IBOutlet weak var lbl_Line_2: UILabel!
    @IBOutlet weak var lbl_Line_3: UILabel!
    
    @IBOutlet weak var lbl_Date_1: UILabel!
    @IBOutlet weak var lbl_Date_2: UILabel!
    @IBOutlet weak var lbl_Date_3: UILabel!
    @IBOutlet weak var lbl_Date_4: UILabel!
    
    @IBOutlet weak var btn_Addreview: UIButton!
    @IBOutlet weak var lbl_title: UILabel!
    
    @IBOutlet weak var lblstr_status1: UILabel!
    @IBOutlet weak var lblstr_status2: UILabel!
    @IBOutlet weak var lblstr_status3: UILabel!
    @IBOutlet weak var lblstr_status4: UILabel!
    
    @IBOutlet weak var lblstr_SubtitleStatus1: UILabel!
    @IBOutlet weak var lblstr_SubtitleStatus2: UILabel!
    @IBOutlet weak var lblstr_SubtitleStatus3: UILabel!
    @IBOutlet weak var lblstr_SubtitleStatus4: UILabel!
    
    var order_id = String()
    var vendor_id = String()
    var product_id = String()
    var product_image = String()
    var productName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "Track Order".localiz()
        self.lblstr_status1.text = "Order placed".localiz()
        self.lblstr_status2.text = "Confirmed".localiz()
        self.lblstr_status3.text = "Order shipped".localiz()
        self.lblstr_status4.text = "Delivered".localiz()
        self.lblstr_SubtitleStatus1.text! = "We have received your order".localiz()
        self.lblstr_SubtitleStatus2.text! = "You order has been confirmed".localiz()
        self.lblstr_SubtitleStatus3.text! = "Your package off for delivery".localiz()
        self.lblstr_SubtitleStatus4.text! = "You order has been delivered".localiz()
        self.btn_Addreview.setTitle("Add Rating & Review".localiz(), for: .normal)
        
        self.btn_Addreview.isHidden = true
        cornerRadius(viewName: self.img_item, radius: 4.0)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let urlString = API_URL + "trackorder"
        let params: NSDictionary = ["order_id":self.order_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
        self.Webservice_Ordertrack(url: urlString, params: params)
    }
    
    @IBAction func btnTap_Addreview(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "WritereviewVC") as! WritereviewVC
        vc.product_id = self.product_id
        vc.product_image = self.product_image
        vc.vendor_id = self.vendor_id
        vc.productName = self.productName
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
}
extension TrackOrderVC
{
    func Webservice_Ordertrack(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let ResponseData = jsonResponse!["order_info"].dictionaryValue
                    self.product_id = ResponseData["product_id"]!.stringValue
                    self.vendor_id = ResponseData["vendor_id"]!.stringValue
                    self.productName = ResponseData["product_name"]!.stringValue
                    self.product_image = ResponseData["image_url"]!.stringValue
                    
                    let ItemPrice = ResponseData["qty"]!.doubleValue * ResponseData["price"]!.doubleValue
                    let Qty_Price = formatter.string(for: ItemPrice)!
                    if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                    {
                        self.lbl_Price.text = "\(Qty_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    }
                    else {
                        
                        self.lbl_Price.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Qty_Price)"
                    }
                    
                    self.lbl_itemName.text = ResponseData["product_name"]!.stringValue
                    
                    if ResponseData["variation"]!.stringValue == ""
                    {
                        self.lbl_size.text = "Size : - "
                    }
                    else{
                        self.lbl_size.text = "Size : \(ResponseData["variation"]!.stringValue)"
                    }
                    self.lbl_qty.text = "Qty : \(ResponseData["qty"]!.stringValue)"
                    self.img_item.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    self.img_item.sd_setImage(with: URL(string: ResponseData["image_url"]!.stringValue), placeholderImage: UIImage(named: "ic_placeholder"))
                    
                    if ResponseData["created_at"]!.stringValue != ""
                    {
                        let date_1 = DateFormater.getFullDateStringFromString(givenDate:ResponseData["created_at"]!.stringValue)
                        self.lbl_Date_1.text = date_1
                    }
                    else
                    {
                        self.lbl_Date_1.text = ""
                    }
                    
                    if ResponseData["confirmed_at"]!.stringValue != ""
                    {
                        let date_2 = DateFormater.getFullDateStringFromString(givenDate:ResponseData["confirmed_at"]!.stringValue)
                        self.lbl_Date_2.text = date_2
                    }
                    else{
                        self.lbl_Date_2.text = ""
                    }
                    
                    if ResponseData["shipped_at"]!.stringValue != ""
                    {
                        let date_3 = DateFormater.getFullDateStringFromString(givenDate:ResponseData["shipped_at"]!.stringValue)
                        self.lbl_Date_3.text = date_3
                    }
                    else{
                        self.lbl_Date_3.text = ""
                    }
                    
                    if ResponseData["delivered_at"]!.stringValue != ""
                    {
                        let date_4 = DateFormater.getFullDateStringFromString(givenDate:ResponseData["delivered_at"]!.stringValue)
                        self.lbl_Date_4.text = date_4
                    }
                    else{
                        self.lbl_Date_4.text = ""
                    }
                    
                    if ResponseData["status"]!.stringValue == "1"
                    {
                        self.img_circle_1.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_2.image = UIImage(systemName: "circle")
                        self.img_circle_3.image = UIImage(systemName: "circle")
                        self.img_circle_4.image = UIImage(systemName: "circle")
                        
                        self.lbl_Line_1.backgroundColor = UIColor.lightGray
                        self.lbl_Line_2.backgroundColor = UIColor.lightGray
                        self.lbl_Line_3.backgroundColor = UIColor.lightGray
                        
                    }
                    else if ResponseData["status"]!.stringValue == "2"
                    {
                        self.img_circle_1.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_2.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_3.image = UIImage(systemName: "circle")
                        self.img_circle_4.image = UIImage(systemName: "circle")
                        
                        self.lbl_Line_1.backgroundColor = UIColor.systemGreen
                        self.lbl_Line_2.backgroundColor = UIColor.systemGreen
                        self.lbl_Line_3.backgroundColor = UIColor.lightGray
                        
                    }
                    else if ResponseData["status"]!.stringValue == "3"
                    {
                        self.img_circle_1.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_2.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_3.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_4.image = UIImage(systemName: "circle")
                        
                        self.lbl_Line_1.backgroundColor = UIColor.systemGreen
                        self.lbl_Line_2.backgroundColor = UIColor.systemGreen
                        self.lbl_Line_3.backgroundColor = UIColor.lightGray
                        
                    }
                    else if ResponseData["status"]!.stringValue == "4" || ResponseData["status"]!.stringValue == "7" || ResponseData["status"]!.stringValue == "8" || ResponseData["status"]!.stringValue == "9" || ResponseData["status"]!.stringValue == "10"
                    {
                        self.img_circle_1.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_2.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_3.image = UIImage(systemName: "circle.circle.fill")
                        self.img_circle_4.image = UIImage(systemName: "circle.circle.fill")
                        
                        self.lbl_Line_1.backgroundColor = UIColor.systemGreen
                        self.lbl_Line_2.backgroundColor = UIColor.systemGreen
                        self.lbl_Line_3.backgroundColor = UIColor.systemGreen
                        
                        if jsonResponse!["ratting"].stringValue == "0"
                        {
                            self.btn_Addreview.isHidden = false
                        }
                        else
                        {
                            self.btn_Addreview.isHidden = true
                        }
                        
                    }
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
