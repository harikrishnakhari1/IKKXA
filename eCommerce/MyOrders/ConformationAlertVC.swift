//
//  ConformationAlertVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 28/10/21.
//

import UIKit
protocol DismissAlertDeleget {
    func DimissSucess()
}
class ConformationAlertVC: UIViewController {
    
    @IBOutlet weak var lbl_Subtitle: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var btn_Cancel: UIButton!
    var deleget :DismissAlertDeleget!
    var isSelectedTypes = String()
    
    @IBOutlet weak var btn_GotoCart: UIButton!
    @IBOutlet weak var img_image: UIImageView!
    @IBOutlet weak var btn_ContinueShoppping: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btn_GotoCart.setTitle("Go to Cart".localiz(), for: .normal)
        self.btn_ContinueShoppping.setTitle("Continue Shopping".localiz(), for: .normal)

        self.btn_GotoCart.isHidden = true
        self.btn_ContinueShoppping.isHidden = true
        self.btn_Cancel.isHidden = true
        cornerRadius(viewName: btn_Cancel, radius: 4.0)
        cornerRadius(viewName: btn_GotoCart, radius: 4.0)
        cornerRadius(viewName: btn_ContinueShoppping, radius: 4.0)
        if self.isSelectedTypes == "1"
        {
            self.btn_GotoCart.isHidden = true
            self.btn_ContinueShoppping.isHidden = true
            self.btn_Cancel.isHidden = false
            self.img_image.image = UIImage.init(named: "ic_close")
            self.lbl_title.text = "Remove Product".localiz()
            self.lbl_Subtitle.text = "Are you sure to remove this product from your cart?".localiz()
            self.btn_Cancel.setTitle("Proceed".localiz(), for: .normal)
        }
        else if self.isSelectedTypes == "2"
        {
            self.btn_GotoCart.isHidden = true
            self.btn_ContinueShoppping.isHidden = true
            self.btn_Cancel.isHidden = false
            self.lbl_title.text = "Remove Favourite".localiz()
            self.img_image.image = UIImage.init(named: "ic_close")
            self.lbl_Subtitle.text = "Are you sure to remove this product from your favourite list?".localiz()
            self.btn_Cancel.setTitle("Proceed".localiz(), for: .normal)
        }
        else if self.isSelectedTypes == "3"
        {
            self.btn_GotoCart.isHidden = true
            self.btn_ContinueShoppping.isHidden = true
            self.btn_Cancel.isHidden = false
            self.lbl_title.text = "Remove Address".localiz()
            self.img_image.image = UIImage.init(named: "ic_close")
            self.lbl_Subtitle.text = "Are you sure to remove this address from your address list?".localiz()
            self.btn_Cancel.setTitle("Proceed".localiz(), for: .normal)
        }
        else if self.isSelectedTypes == "4"
        {
            self.btn_GotoCart.isHidden = true
            self.btn_ContinueShoppping.isHidden = true
            self.btn_Cancel.isHidden = false
            self.lbl_title.text = "Cancel Product".localiz()
            self.img_image.image = UIImage.init(named: "ic_close")
            self.lbl_Subtitle.text = "Are you sure to cancel this product from your order?".localiz()
            self.btn_Cancel.setTitle("Proceed".localiz(), for: .normal)
        }
        else if self.isSelectedTypes == "5"
        {
            self.btn_GotoCart.isHidden = false
            self.btn_ContinueShoppping.isHidden = false
            self.btn_Cancel.isHidden = true
            self.img_image.image = UIImage.init(named: "ic_sucess")
            self.lbl_title.text = "Success!".localiz()
            self.lbl_Subtitle.text = "Product added successfully into your cart".localiz()
            self.btn_Cancel.setTitle("Proceed", for: .normal)
        }
        
        
    }
    
    @IBAction func btnTap_ContinueShopping(_ sender: UIButton) {
        
        self.dismiss(animated: false) {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            objVC.modalPresentationStyle = .fullScreen
            self.present(objVC, animated: true)

        }
    }
    @IBAction func btnTap_GotoCart(_ sender: UIButton) {
        self.dismiss(animated: false) {
            self.deleget.DimissSucess()
        }
        
    }
    @IBAction func btnTap_Dismiss(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnTap_Cancelitem(_ sender: UIButton) {
        self.dismiss(animated: false) {
            self.deleget.DimissSucess()
        }
    }
}
