//
//  ItemDetailsVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 02/07/21.
//

import UIKit
import SwiftyJSON
import SDWebImage
import ImageSlideshow
class SizeCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_Size: UILabel!
    @IBOutlet weak var cell_view: UIView!
    
}


class ItemDetailsVC: UIViewController {
    
    @IBOutlet weak var img_Slider: ImageSlideshow!
    @IBOutlet weak var CollectionView_itemsmayLikeList: UICollectionView!
    @IBOutlet weak var CollectionView_SizeList: UICollectionView!
    @IBOutlet weak var lbl_TitleName: UILabel!
    
    @IBOutlet weak var lbl_Rating: UILabel!
    @IBOutlet weak var lbl_instock: UILabel!
    @IBOutlet weak var lbl_sku: UILabel!
    @IBOutlet weak var lbl_DiscountPrice: UILabel!
    @IBOutlet weak var lbl_MainPrice: UILabel!
    @IBOutlet weak var lbl_Category: UILabel!
    @IBOutlet weak var lbl_ProductName: UILabel!
    
    @IBOutlet weak var lbl_EstimatedTime: UILabel!
    @IBOutlet weak var btn_reviews: UIButton!
    @IBOutlet weak var btn_Description: UIButton!
    @IBOutlet weak var lbl_ShippingCharge: UILabel!
    @IBOutlet weak var btn_returnText: UIButton!
    
    @IBOutlet weak var lblstr_youmayalso: UILabel!
    @IBOutlet weak var lbl_returnline: UILabel!
    // Size View
    @IBOutlet weak var lbl_SelectSize: UILabel!
    @IBOutlet weak var View_Size: UIView!
    @IBOutlet weak var Height_SizeView: NSLayoutConstraint!
    
    
    
    
    @IBOutlet weak var btn_Addtocart: UIButton!
    @IBOutlet weak var btn_Wishlist: UIButton!
    // Store View
    @IBOutlet weak var View_Store: UIView!
    @IBOutlet weak var img_Store: UIImageView!
    @IBOutlet weak var btn_Visitstore: UIButton!
    @IBOutlet weak var lbl_storeName: UILabel!
    @IBOutlet weak var lbl_storeRatting: UILabel!
    @IBOutlet weak var lbl_inclusivetaxes: UILabel!
    
    @IBOutlet weak var view_Return: UIView!
    @IBOutlet weak var height_Returnview: NSLayoutConstraint!
    
    @IBOutlet weak var CollectionViewRelated_Height: NSLayoutConstraint!
    
    @IBOutlet weak var btn_return_policy_txt: UIButton!
    //    var SizeArray = [String]()
//    var ColorArray = [[String:String]]()
    var Product_id = String()
    var item_id = String()
    var VariationsArray = [[String:String]]()
    var Store_id = String()
    var Descriaption = String()
    var is_wishlist = String()
    var image_name = String()
    var productImages = [SDWebImageSource]()
    var FirstProductimage = String()
    var RelatedProductsArray = [JSON]()
    var Product_Price = String()
    var Product_Varication = String()
    var Product_Image = String()
    var shipping_cost = String()
    var Product_Tax = String()
    var TaxType = String()
    var Tax = String()
    var ReturnValuetext = String()
    var store_image_url = String()
    var discountPriceHideFlag : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        print(item_id)
        self.lblstr_youmayalso.text = "You May also like".localiz()
        self.btn_Description.setTitle("Description".localiz(), for: .normal)
        self.btn_reviews.setTitle("Ratings & Reviews".localiz(), for: .normal)
        //self.btn_returnText.setTitle("10 days return Policy".localiz(), for: .normal)
        self.btn_Visitstore.setTitle("Visit Store".localiz(), for: .normal)
        self.btn_Addtocart.setTitle("Add to Cart".localiz(), for: .normal)

        self.btn_return_policy_txt.setTitle("Return Policy".localiz(), for: .normal)
        
        setBorder(viewName: self.btn_Wishlist, borderwidth: 1, borderColor: UIColor.init(named: "App_color")!.cgColor, cornerRadius: 4.0)
        cornerRadius(viewName: self.btn_Addtocart, radius: 4.0)
        cornerRadius(viewName: self.img_Store, radius: self.img_Store.frame.height / 2)
        self.view_Return.isHidden = true
        self.height_Returnview.constant = 0.0
        self.lbl_returnline.isHidden = true
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "en" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "N/A" {
            self.btn_Description.titleLabel!.textAlignment = .left
            self.btn_reviews.titleLabel!.textAlignment = .left
            self.btn_returnText.titleLabel!.textAlignment = .left

        }
        else {
            self.btn_Description.titleLabel!.textAlignment = .right
            self.btn_reviews.titleLabel!.textAlignment = .right
            self.btn_returnText.titleLabel!.textAlignment = .right

        }
        
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.Product_id = item_id
        let urlString = API_URL + "productdetails"
        let params: NSDictionary = ["product_id":self.Product_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
        self.Webservice_ProductDetails(url: urlString,params: params)
        
        self.lbl_TitleName.isHidden = true
        self.lbl_ProductName.isHidden = true
        self.lbl_sku.isHidden = true
        self.lbl_Category.isHidden = true
        self.lbl_EstimatedTime.isHidden = true
        self.lbl_SelectSize.isHidden = true
        self.lbl_ShippingCharge.isHidden = true
        self.lbl_MainPrice.isHidden = true
        self.lbl_DiscountPrice.isHidden = true
        self.lbl_inclusivetaxes.isHidden = true
    }
    
}
// MARK:- Button Actions
extension ItemDetailsVC
{
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    @IBAction func btnTap_WishList(_ sender: UIButton) {
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A"
        {
            UserDefaults.standard.set("", forKey:UD_userId)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
            appNavigation.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.windows[0].rootViewController = appNavigation
        }
        else
        {
            if self.is_wishlist == "1"
            {
                let urlString = API_URL + "removefromwishlist"
                let params: NSDictionary = ["product_id":self.Product_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_RemoveWishList(url: urlString,params: params)
            }
            else{
                let urlString = API_URL + "addtowishlist"
                let params: NSDictionary = ["product_id":self.Product_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_AddtoWishList(url: urlString,params: params)
            }
        }
        
        
    }
    @IBAction func btnTap_AddtoCart(_ sender: UIButton) {
        
       
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A"
        {
            UserDefaults.standard.set("", forKey:UD_userId)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            objVC.isFromCart = true
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            UIApplication.shared.windows[0].rootViewController = nav
        }
        else
        {
                self.btn_Addtocart.isEnabled = true
                let Shipping_Price = formatter.string(for: self.shipping_cost.toDouble)!
                let Tax_Price = formatter.string(for: self.Product_Tax.toDouble)!
                
                let urlString = API_URL + "addtocart"
                let params: NSDictionary = ["product_id":self.Product_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"vendor_id":self.Store_id,"product_name":self.lbl_TitleName.text!,"qty":"1","price":self.Product_Price.replacingOccurrences(of: ",", with: ""),"variation":self.Product_Varication,"image":self.Product_Image,"shipping_cost":Shipping_Price,"tax":Tax_Price,"attribute":self.lbl_SelectSize.text!]
                self.Webservice_AddtoCart(url: urlString,params: params)
           
        }
        
        
    }
    
    @IBAction func btnTap_Descripation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "DescripationsVC") as! DescripationsVC
        vc.DescriptionText = self.Descriaption
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    @IBAction func btnTap_Review(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ItemReviewListVC") as! ItemReviewListVC
        vc.Product_id = self.Product_id
        vc.vendor_id = self.Store_id
        vc.product_image = self.FirstProductimage
        vc.item_name = self.lbl_TitleName.text!
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    @IBAction func btnTap_ReturnPolicy(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ReturnPolicyVC") as! ReturnPolicyVC
        vc.ReturnValue = self.ReturnValuetext
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    
    @IBAction func btnTap_VisitStore(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "StoreitemListVC") as! StoreitemListVC
        vc.vendor_id = self.Store_id
        vc.StoreName = self.lbl_storeName.text!
        vc.store_rate = self.lbl_storeRatting.text!
        vc.imageURL = self.store_image_url
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
}
// MARK:- UICollectionViewDelegate UICollectionViewDataSource Methods
extension ItemDetailsVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.CollectionView_SizeList
        {
            return VariationsArray.count
        }
        else{
            return self.RelatedProductsArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.CollectionView_SizeList
        {
            let cell = self.CollectionView_SizeList.dequeueReusableCell(withReuseIdentifier: "SizeCell", for: indexPath) as! SizeCell
            let data = self.VariationsArray[indexPath.item]
            cell.lbl_Size.text = data["variation"]!
            
            if data["isSelectSize"] == "1"
            {
                setBorder(viewName: cell.cell_view, borderwidth: 0.8, borderColor: UIColor.init(named: "App_color")!.cgColor, cornerRadius: 6.0)
                cell.cell_view.backgroundColor = APP_COLOR3
            }else{
                setBorder(viewName: cell.cell_view, borderwidth: 0.8, borderColor: UIColor.lightGray.cgColor, cornerRadius: 6.0)
                cell.cell_view.backgroundColor = UIColor.white
            }
            return cell
        }
        else
        {
            
            let cell = self.CollectionView_itemsmayLikeList.dequeueReusableCell(withReuseIdentifier: "MostpopularCell", for: indexPath) as! MostpopularCell
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.cornerRadius = 8.0
            cell.layer.borderWidth = 0.4
            let data = self.RelatedProductsArray[indexPath.item]
            let productimage = data["productimage"].dictionaryValue
            let variation = data["variation"].dictionaryValue
            let is_variation = data["is_variation"].stringValue
            let is_wishlist = data["is_wishlist"].stringValue
            let avg_ratting = data["rattings"].arrayValue
            if avg_ratting.count != 0
            {
                let ProductRatting = formatterRatting.string(for: avg_ratting[0]["avg_ratting"].stringValue.toDouble)
                cell.lbl_ratingcount.text = "\(ProductRatting!)"
            }
            else{
                cell.lbl_ratingcount.text = "0.0"
            }
            if is_wishlist == "1"
            {
                cell.btn_favorite.setImage(UIImage(named: "ic_heartfill"), for: .normal)
                cell.btn_favorite.tintColor = UIColor.red
                
            }
            else{
                cell.btn_favorite.setImage(UIImage(named: "ic_heart"), for: .normal)
                cell.btn_favorite.tintColor = UIColor.darkGray
            }
            
          
            
            
            if is_variation == "1"
            {
                if variation.count != 0
                {
                    let ProductPrice = formatter.string(for: variation["price"]!.stringValue.toDouble)

                    let ProductDiscountPrice = formatter.string(for: variation["discounted_variation_price"]!.stringValue.toDouble)
                    if(ProductPrice == ProductDiscountPrice){
                        if(discountPriceHideFlag){
                            discountPriceHideFlag = true
                            self.lbl_DiscountPrice.isHidden = true
                        }
                    }
                    if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                    {
                        cell.lbl_itemprice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                        cell.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
                    }
                    else {
                        cell.lbl_itemprice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                        cell.lbl_DiscountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
                    }
                }
               
                
            }
            else{
                let ProductPrice = formatter.string(for: data["product_price"].stringValue.toDouble)
            
                let ProductDiscountPrice = formatter.string(for: data["discounted_price"].stringValue.toDouble)
               
                
                if(self.Product_Price == ProductDiscountPrice){
                    if(discountPriceHideFlag){
                        discountPriceHideFlag = true
                        self.lbl_DiscountPrice.isHidden = true
                    }
                }
                if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                {
                    cell.lbl_itemprice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    cell.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
                }
                else{
                   
                    cell.lbl_itemprice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                    cell.lbl_DiscountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
                }
                
            }
            cell.lbl_itemName.text = data["product_name"].stringValue
            
            cell.img_item.sd_imageTransition = .fade
            let items = indexPath.item % 6
            cell.img_item.sd_setImage(with: URL(string: productimage["image_url"]!.stringValue), placeholderImage: UIImage(named: "b_0\(items)"))
            
            cell.btn_favorite.tag = indexPath.row
            cell.btn_favorite.addTarget(self, action:#selector(btnTap_Favorites_TrendingNow), for: .touchUpInside)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.CollectionView_SizeList
        {
            return CGSize(width: 60, height: 45)
        }
        else{
            return CGSize(width: (UIScreen.main.bounds.width - 30.0) / 2, height: ((UIScreen.main.bounds.width - 30.0) / 2) * 1.90)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.CollectionView_SizeList
        {
            var variationDataArray = [[String:String]]()
            variationDataArray = self.VariationsArray
            self.VariationsArray.removeAll()
            for data in  variationDataArray
            {
                let obj = ["id":data["id"]!,"color":data["color"]!,"price":data["price"]!,"discounted_variation_price":data["discounted_variation_price"]!,"variation":data["variation"]!,"qty":data["qty"]!,"isSelectSize":"0"]
                self.VariationsArray.append(obj)
            }
            
            var variationdata = self.VariationsArray[indexPath.item]
            self.Product_Varication = variationdata["variation"]!
            variationdata["isSelectSize"] = "1"
            self.VariationsArray.remove(at: indexPath.item)
            self.VariationsArray.insert(variationdata, at: indexPath.item)
            
//            let colordata = self.VariationsArray[indexPath.item]["color"]!.components(separatedBy: ",")
//            self.ColorArray.removeAll()
//            for data in colordata
//            {
//                let obj = ["color":data,"isSelectColor":"0"]
//                self.ColorArray.append(obj)
//            }
            self.CollectionView_SizeList.reloadData()
            
            if variationdata["qty"]! == "0"
            {
                self.lbl_instock.text = "Out-stock".localiz()
                self.lbl_instock.textColor = UIColor.systemRed
                self.btn_Addtocart.isEnabled = false
                self.btn_Addtocart.backgroundColor = UIColor.lightGray
            }
            else{
                self.lbl_instock.text = "In-stock".localiz()
                self.lbl_instock.textColor = UIColor.systemGreen
                self.btn_Addtocart.isEnabled = true
                self.btn_Addtocart.backgroundColor = UIColor.init(named: "App_color")
            }
            
            self.Product_Price = formatter.string(for: variationdata["price"]!.toDouble)!
            let ProductDiscountPrice = formatter.string(for: variationdata["discounted_variation_price"]!.toDouble)
            
           
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                self.lbl_MainPrice.text = "\(self.Product_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                self.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
            }
            else{
                self.lbl_MainPrice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(self.Product_Price)"
                self.lbl_DiscountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
            }
            if self.TaxType == "amount"
            {
                self.Product_Tax = self.Tax
            }
            else
            {
                let productTax = Double(self.Product_Price.replacingOccurrences(of: ",", with: ""))! * (Double(self.Tax)! / 100)
                self.Product_Tax = "\(productTax)"
            }
            
            let Tax_Price = formatter.string(for: self.Product_Tax.toDouble)!
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                if Tax_Price == "0.00"
                {
                    self.lbl_inclusivetaxes.text! = "Inclusive All Taxes"
                    self.lbl_inclusivetaxes.textColor = UIColor.systemGreen
                }
                else
                {
                    self.lbl_inclusivetaxes.text! = "\(Tax_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency)) Additional Tax"
                    self.lbl_inclusivetaxes.textColor = UIColor.systemRed
                }
            }
            else
            {
                if Tax_Price == "0.00"
                {
                    self.lbl_inclusivetaxes.text! = "Inclusive All Taxes"
                    self.lbl_inclusivetaxes.textColor = UIColor.systemGreen
                }
                else{
                    self.lbl_inclusivetaxes.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Tax_Price) Additional Tax"
                    self.lbl_inclusivetaxes.textColor = UIColor.systemRed
                }
                
            }
            
        }
        else if collectionView == self.CollectionView_itemsmayLikeList
        {
            let data = self.RelatedProductsArray[indexPath.item]
            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
            vc.item_id = data["id"].stringValue
            vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
        }
    }
    @objc func btnTap_Favorites_TrendingNow(sender:UIButton!) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A"
        {
            UserDefaults.standard.set("", forKey:UD_userId)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            UIApplication.shared.windows[0].rootViewController = nav
        }
        else
        {
            let data = self.RelatedProductsArray[sender.tag]
            let is_wishlist = data["is_wishlist"].stringValue
            if is_wishlist == "1"
            {
                let urlString = API_URL + "removefromwishlist"
                let params: NSDictionary = ["product_id":data["id"].stringValue,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_RemoveWishListRelated(url: urlString,params: params, sender: sender.tag)
            }
            else{
                let urlString = API_URL + "addtowishlist"
                let params: NSDictionary = ["product_id":data["id"].stringValue,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_AddtoWishListRelated(url: urlString,params: params, sender: sender.tag)
            }
        }
    }
    
}
//MARK: Webservices
extension ItemDetailsVC {
    func Webservice_ProductDetails(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) { [self](_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    self.lbl_TitleName.isHidden = false
                    self.lbl_ProductName.isHidden = false
                    self.lbl_sku.isHidden = false
                    self.lbl_Category.isHidden = false
                    self.lbl_EstimatedTime.isHidden = false
                    self.lbl_SelectSize.isHidden = false
                    self.lbl_ShippingCharge.isHidden = false
                    self.lbl_MainPrice.isHidden = false
                    self.lbl_DiscountPrice.isHidden = false
                    self.lbl_inclusivetaxes.isHidden = true
                    
                    let Productdata = jsonResponse!["data"].dictionaryValue
                    self.lbl_TitleName.text = Productdata["product_name"]!.stringValue
                    self.lbl_ProductName.text = Productdata["product_name"]!.stringValue
                    self.lbl_sku.text = "SKU : \(Productdata["sku"]!.stringValue)"
                    self.lbl_Category.text = "\(Productdata["category_name"]!.stringValue.uppercased()) | \(Productdata["subcategory_name"]!.stringValue.uppercased()) | \(Productdata["innersubcategory_name"]!.stringValue.uppercased())"
                    let is_variation = Productdata["is_variation"]!.stringValue
                    self.Descriaption = Productdata["description"]!.stringValue
                    
                    self.ReturnValuetext = jsonResponse!["returnpolicy"]["return_policies"].stringValue
                    self.is_wishlist = Productdata["is_wishlist"]!.stringValue
                    self.shipping_cost = Productdata["shipping_cost"]!.stringValue
                    self.lbl_EstimatedTime.text = "EST. Shipping Time".localiz() + " : \(Productdata["est_shipping_days"]!.stringValue) " + "Day".localiz()
                    
                    self.lbl_SelectSize.text = Productdata["attribute"]!.stringValue
                    
                    if Productdata["free_shipping"]!.stringValue == "1"
                    {
                        self.lbl_ShippingCharge.text = "Free Shipping"
                    }
                    else if Productdata["free_shipping"]!.stringValue == "2"
                    {
                        let Shipping_Price = formatter.string(for: self.shipping_cost.toDouble)!
                        if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                        {
                            self.lbl_ShippingCharge.text = "Shipping Charge : \(Shipping_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                        }
                        else{
                            self.lbl_ShippingCharge.text = "Shipping Charge : \(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Shipping_Price)"
                        }
                    }
                    if Productdata["is_return"]!.stringValue == "2"
                    {
                        self.view_Return.isHidden = true
                        self.height_Returnview.constant = 0.0
                        self.lbl_returnline.isHidden = true
                    }
                    else if Productdata["is_return"]!.stringValue == "1"
                    {
                        self.view_Return.isHidden = false
                        self.height_Returnview.constant = 45.0
                        self.lbl_returnline.isHidden = false
                        // By shoaib only need "return policy" uncomment for future use
                       // self.btn_returnText.setTitle("\(Productdata["return_days"]!.stringValue) Days return Policy", for: .normal)
                        self.btn_returnText.setTitle("Return Policy".localiz(), for: .normal)
                    }
                    
                    let productImages = Productdata["productimages"]!.arrayValue
                    self.FirstProductimage = productImages[0]["image_url"].stringValue
                    self.Product_Image = productImages[0]["image_name"].stringValue
                    if productImages.count != 0 {
                        self.image_name = productImages[0]["image_url"].stringValue
                    }
                    self.productImages.removeAll()
                    for image in productImages {
                        let imageSource = SDWebImageSource(url: URL(string: image["image_url"].stringValue)!)
                        self.productImages.append(imageSource)
                    }
                    
                    self.imageSliderData()
                    
                    
                    let rattings = Productdata["rattings"]!.arrayValue
                    if rattings.count == 0
                    {
                        self.lbl_Rating.text = "0.0"
                    }
                    else{
                        let ProductRatting = formatterRatting.string(for: rattings[0]["avg_ratting"].stringValue.toDouble)
                        self.lbl_Rating.text = "\(ProductRatting!)"
                    }
                    if self.is_wishlist == "1"
                    {
                        self.btn_Wishlist.setImage(UIImage(named: "ic_heartfill"), for: .normal)
                        self.btn_Wishlist.tintColor = UIColor.red
                        
                    }
                    else{
                        self.btn_Wishlist.setImage(UIImage(named: "ic_heart"), for: .normal)
                        self.btn_Wishlist.tintColor = UIColor.darkGray
                    }
                    if is_variation == "1"
                    {
                        self.View_Size.isHidden = false
                        self.Height_SizeView.constant = 100.0
                        
                        self.VariationsArray.removeAll()
                        let VariationsData = Productdata["variations"]!.arrayValue
                        
                        if VariationsData.count != 0
                        {
                            self.Product_Varication = VariationsData[0]["variation"].stringValue
                            for i in 0..<VariationsData.count
                            {
                                if i == 0
                                {
                                    
                                    let obj = ["id":VariationsData[i]["id"].stringValue,"color":VariationsData[i]["color"].stringValue,"price":VariationsData[i]["price"].stringValue,"discounted_variation_price":VariationsData[i]["discounted_variation_price"].stringValue,"variation":VariationsData[i]["variation"].stringValue,"qty":VariationsData[i]["qty"].stringValue,"isSelectSize":"1"]
                                    self.VariationsArray.append(obj)
                                }
                                else{
                                    let obj = ["id":VariationsData[i]["id"].stringValue,"color":VariationsData[i]["color"].stringValue,"price":VariationsData[i]["price"].stringValue,"discounted_variation_price":VariationsData[i]["discounted_variation_price"].stringValue,"variation":VariationsData[i]["variation"].stringValue,"qty":VariationsData[i]["qty"].stringValue,"isSelectSize":"0"]
                                    self.VariationsArray.append(obj)
                                }
                                
                            }
                            
//                            let colordata = VariationsData[0]["color"].stringValue.components(separatedBy: ",")
//                            for data in colordata
//                            {
//                                let obj = ["color":data,"isSelectColor":"0"]
//                                self.ColorArray.append(obj)
//                            }
                            
                            
                            if VariationsData[0]["qty"].stringValue == "0"
                            {
                                self.lbl_instock.text = "Out-stock".localiz()
                                self.lbl_instock.textColor = UIColor.systemRed
                                self.btn_Addtocart.isEnabled = false
                                self.btn_Addtocart.backgroundColor = UIColor.lightGray
                            }
                            else{
                                self.lbl_instock.text = "In-stock".localiz()
                                self.lbl_instock.textColor = UIColor.systemGreen
                                self.btn_Addtocart.isEnabled = true
                                self.btn_Addtocart.backgroundColor = UIColor.init(named: "App_color")
                                
                            }
                            self.Product_Price = formatter.string(for: VariationsData[0]["price"].stringValue.toDouble)!
                            let ProductDiscountPrice = formatter.string(for: VariationsData[0]["discounted_variation_price"].stringValue.toDouble)
                            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                            {
                                self.lbl_MainPrice.text = "\(self.Product_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                                self.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
                            }
                            else{
                                self.lbl_MainPrice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(self.Product_Price)"
                                self.lbl_DiscountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
                            }
                            self.CollectionView_SizeList.delegate = self
                            self.CollectionView_SizeList.dataSource = self
                            self.CollectionView_SizeList.reloadData()
                            
                        }
                    }
                    else if is_variation == "0"{
                        self.View_Size.isHidden = true
                        self.Height_SizeView.constant = 0.0
                        self.Product_Varication = ""
                        if Productdata["product_qty"]!.stringValue == "0"
                        {
                            self.lbl_instock.text = "Out-stock".localiz()
                            self.lbl_instock.textColor = UIColor.systemRed
                            self.btn_Addtocart.isEnabled = false
                            self.btn_Addtocart.backgroundColor = UIColor.lightGray
                        }
                        else{
                            self.lbl_instock.text = "In-stock".localiz()
                            self.lbl_instock.textColor = UIColor.systemGreen
                            self.btn_Addtocart.isEnabled = true
                            self.btn_Addtocart.backgroundColor = UIColor.init(named: "App_color")
                        }
                        self.Product_Price = formatter.string(for: Productdata["product_price"]!.stringValue.toDouble)!
                        let ProductDiscountPrice = formatter.string(for: Productdata["discounted_price"]!.stringValue.toDouble)
                        if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                        {
                            self.lbl_MainPrice.text = "\(self.Product_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                            self.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
                        }
                        else{
                            self.lbl_MainPrice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(self.Product_Price)"
                            self.lbl_DiscountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
                        }
                    }
                    
                    self.TaxType = Productdata["tax_type"]!.stringValue
                    self.Tax = Productdata["tax"]!.stringValue
                    if(self.Tax.isEmpty || self.Tax == "null"){
                        self.Tax = "0"
                        print("tax empty")
                    }else{
                        self.Tax = Productdata["tax"]!.stringValue
                    }
                        
                    if self.TaxType == "amount"
                    {
                        self.Product_Tax = self.Tax
                    }
                    else
                    {
                        let productTax = Double(self.Product_Price.replacingOccurrences(of: ",", with: ""))! * (Double(self.Tax)! / 100)
                        self.Product_Tax = "\(productTax)"
                    }
                    
                    let Tax_Price = formatter.string(for: self.Product_Tax.toDouble)!
                    if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                    {
                        if Tax_Price == "0.00"
                        {
                            self.lbl_inclusivetaxes.text! = "Inclusive All Taxes"
                            self.lbl_inclusivetaxes.textColor = UIColor.systemGreen
                        }
                        else
                        {
                            self.lbl_inclusivetaxes.text! = "\(Tax_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency)) Additional Tax"
                            self.lbl_inclusivetaxes.textColor = UIColor.systemRed
                        }
                        
                        
                    }
                    else
                    {
                        if Tax_Price == "0.00"
                        {
                            self.lbl_inclusivetaxes.text! = "Inclusive All Taxes"
                            self.lbl_inclusivetaxes.textColor = UIColor.systemGreen
                        }
                        else{
                            self.lbl_inclusivetaxes.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Tax_Price) Additional Tax"
                            self.lbl_inclusivetaxes.textColor = UIColor.systemRed
                        }
                        
                    }
                    
                    let vendors = jsonResponse!["vendors"].dictionaryValue
                    self.lbl_storeName.text = vendors["name"]!.stringValue
                    self.img_Store.sd_imageTransition = .fade
                    self.img_Store.sd_setImage(with: URL(string: vendors["image_url"]!.stringValue), placeholderImage: UIImage(named: "b_00"))
                    self.store_image_url = vendors["image_url"]!.stringValue
                 
                    
                    
                    self.Store_id = vendors["id"]!.stringValue
                    
                    let storeratting = vendors["rattings"]!.arrayValue
                    if storeratting.count == 0
                    {
                        self.lbl_storeRatting.text = "0.0"
                    }
                    else{
                        let ProductRatting = formatterRatting.string(for: storeratting[0]["avg_ratting"].stringValue.toDouble)
                        self.lbl_storeRatting.text = "\(ProductRatting!)"
                    }
                    
                    self.RelatedProductsArray = jsonResponse!["related_products"].arrayValue
                    
                    self.CollectionView_itemsmayLikeList.delegate = self
                    self.CollectionView_itemsmayLikeList.dataSource = self
                    self.CollectionView_itemsmayLikeList.reloadData()
                    self.CollectionViewRelated_Height.constant = ((UIScreen.main.bounds.width - 30.0) / 2) * 1.90
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    
    func Webservice_AddtoWishList(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    self.is_wishlist = "1"
                    if self.is_wishlist == "1"
                    {
                        self.btn_Wishlist.setImage(UIImage(named: "ic_heartfill"), for: .normal)
                        self.btn_Wishlist.tintColor = UIColor.red
                        
                    }
                    else{
                        self.btn_Wishlist.setImage(UIImage(named: "ic_heart"), for: .normal)
                        self.btn_Wishlist.tintColor = UIColor.darkGray
                    }
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_RemoveWishList(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    self.is_wishlist = "0"
                    if self.is_wishlist == "1"
                    {
                        self.btn_Wishlist.setImage(UIImage(named: "ic_heartfill"), for: .normal)
                        self.btn_Wishlist.tintColor = UIColor.red
                        
                    }
                    else{
                        self.btn_Wishlist.setImage(UIImage(named: "ic_heart"), for: .normal)
                        self.btn_Wishlist.tintColor = UIColor.darkGray
                    }
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_AddtoWishListRelated(url:String, params:NSDictionary,sender:Int) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    var data = self.RelatedProductsArray[sender]
                    data["is_wishlist"].stringValue = "1"
                    self.RelatedProductsArray.remove(at: sender)
                    self.RelatedProductsArray.insert(data, at: sender)
                    self.CollectionView_itemsmayLikeList.reloadData()
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_RemoveWishListRelated(url:String, params:NSDictionary,sender:Int) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    var data = self.RelatedProductsArray[sender]
                    data["is_wishlist"].stringValue = "0"
                    self.RelatedProductsArray.remove(at: sender)
                    self.RelatedProductsArray.insert(data, at: sender)
                    self.CollectionView_itemsmayLikeList.reloadData()
                    
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    
    func Webservice_AddtoCart(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    
//                    let alertVC = UIAlertController(title: "", message: "Item Added to cart", preferredStyle: .alert)
//                    let yesAction = UIAlertAction(title: "Continue Shopping", style: .default) { (action) in
//
//                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                        let objVC = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//                        let TabViewController = storyBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
//                        let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
//                        appNavigation.setNavigationBarHidden(true, animated: true)
//                        UIApplication.shared.windows[0].rootViewController = TabViewController
//
//                    }
//                    let noAction = UIAlertAction(title: "Go to Cart", style: .default) { (action) in
//                        self.tabBarController!.selectedIndex = 2
//
//                    }
//                    alertVC.addAction(yesAction)
//                    alertVC.addAction(noAction)
//                    self.present(alertVC,animated: true,completion: nil)
//
                    
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConformationAlertVC") as! ConformationAlertVC
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle = .crossDissolve
                    vc.isSelectedTypes = "5"
                    vc.deleget = self
                    self.present(vc,animated: true,completion: nil)
                    
                    
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    
    func imageSliderData() {
        self.img_Slider.slideshowInterval = 3.0
        self.img_Slider.pageIndicatorPosition = .init(horizontal: .center, vertical: .customBottom(padding: 10.0))
        self.img_Slider.contentScaleMode = UIView.ContentMode.scaleAspectFit
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.img_Slider.pageIndicator = pageControl
        self.img_Slider.setImageInputs(self.productImages)
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapImage))
        self.img_Slider.addGestureRecognizer(recognizer)
    }
    @objc func didTapImage() {
        self.img_Slider.presentFullScreenController(from: self)
    }
}
extension ItemDetailsVC : DismissAlertDeleget
{
    func DimissSucess() {
        self.dismiss(animated: false, completion: nil)
    }
    
    
}
