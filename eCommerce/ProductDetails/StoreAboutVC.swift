//
//  StoreAboutVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 02/07/21.
//

import UIKit
import SwiftyJSON

class StoreAboutVC: UIViewController {

    @IBOutlet weak var lbl_storeRate: UILabel!
    @IBOutlet weak var lbl_storeName: UILabel!
    @IBOutlet weak var img_store: UIImageView!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var lbl_Phone: UILabel!
    @IBOutlet weak var lbl_addresss: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    var vendordetails = [String:JSON]()
    var imageURL = String()
    var storeName = String()
    var store_rate = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "About".localiz()
        cornerRadius(viewName: img_store, radius: self.img_store.frame.height / 2)
        
        self.lbl_storeName.text = self.storeName
        self.img_store.sd_imageTransition = .fade
        self.img_store.sd_setImage(with: URL(string: self.imageURL), placeholderImage: UIImage(named: "b_00"))
        self.lbl_email.text = vendordetails["email"]!.stringValue
        self.lbl_Phone.text = vendordetails["mobile"]!.stringValue
        self.lbl_addresss.text = vendordetails["store_address"]!.stringValue
        let rates = vendordetails["rattings"]!["avg_ratting"].stringValue
        if rates == ""
        {
            self.lbl_storeRate.text = "0.0"
        }
        else
        {
            self.lbl_storeRate.text = rates
        }
        
        
    }
    
    @IBAction func btnTap_Close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
