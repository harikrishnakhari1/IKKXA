//
//  DescripationsVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 02/07/21.
//

import UIKit

class DescripationsVC: UIViewController {

    @IBOutlet weak var text_Description: UITextView!
    var DescriptionText = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.text_Description.text = self.DescriptionText
       
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
}
