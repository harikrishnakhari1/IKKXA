//
//  ItemReviewListVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 02/07/21.
//

import UIKit
import SwiftyJSON
import SDWebImage
class itemReviewListcell: UITableViewCell {
    
    @IBOutlet weak var lbl_comment: UILabel!
    @IBOutlet weak var img_star: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var img_Userprofile: UIImageView!
    
    @IBOutlet weak var lbl_date: UILabel!
}
class ItemReviewListVC: UIViewController {
    
    @IBOutlet weak var Tableview_ItemReviewList: UITableView!
    
    @IBOutlet weak var img_Ratings: UIImageView!
    @IBOutlet weak var lbl_BasedRatings: UILabel!
    @IBOutlet weak var lbl_Ratings: UILabel!
    @IBOutlet weak var progress_5: UIProgressView!
    @IBOutlet weak var progress_4: UIProgressView!
    @IBOutlet weak var progress_3: UIProgressView!
    @IBOutlet weak var progress_2: UIProgressView!
    @IBOutlet weak var progress_1: UIProgressView!
    @IBOutlet weak var View_Review: CardViewMaster!
    @IBOutlet weak var Height_Review: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_title: UILabel!
    var Product_id = String()
    var product_image = String()
    var vendor_id = String()
    var ReviewsArray = [[String:String]]()
    var item_name = String()
    var pageIndex = 1
    var lastIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "Ratings & Reviews".localiz()
        self.View_Review.isHidden = true
        self.Height_Review.constant = 0.0
        self.Tableview_ItemReviewList.tableFooterView = UIView()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let urlString = API_URL + "productreview?page=\(self.pageIndex)"
        let params: NSDictionary = ["product_id":self.Product_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
        self.Webservice_ProductReviewList(url: urlString,params: params)
    }
    
    
}
//MARK: Button Actions
extension ItemReviewListVC
{
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
}
//MARK: Tableview Methods
extension ItemReviewListVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)

        imagedata.contentMode = .scaleAspectFit
        self.Tableview_ItemReviewList.backgroundView = imagedata
        if self.ReviewsArray.count == 0 {
            imagedata.image = UIImage(named: "ic_OnlineReview-rafiki")
        }
        else {
            imagedata.image = UIImage(named: "")
            self.View_Review.isHidden = false
            self.Height_Review.constant = 200.0
        }
        return self.ReviewsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_ItemReviewList.dequeueReusableCell(withIdentifier: "itemReviewListcell") as! itemReviewListcell
        cornerRadius(viewName: cell.img_Userprofile, radius: cell.img_Userprofile.frame.height / 2)
        let data = self.ReviewsArray[indexPath.row]
        
        
        cell.img_Userprofile.sd_imageTransition = .fade
        let items = indexPath.item % 6
        cell.img_Userprofile.sd_setImage(with: URL(string: data["image_url"]!), placeholderImage: UIImage(named: "b_0\(items)"))
        
        
        cell.lbl_name.text = data["user_name"]!
        let setdate = DateFormater.getBirthDateStringFromDateString(givenDate:data["date"]!)
        cell.lbl_date.text = setdate
        cell.lbl_comment.text = data["comment"]!
        cell.img_star.image = UIImage.init(named: "0\(data["ratting"]!)")
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.ReviewsArray.count - 1 {
            if self.pageIndex != self.lastIndex {
                self.pageIndex = self.pageIndex + 1
                if self.ReviewsArray.count != 0 {
                    let urlString = API_URL + "productreview?page=\(self.pageIndex)"
                    let params: NSDictionary = ["product_id":self.Product_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                    self.Webservice_ProductReviewList(url: urlString,params: params)
                }
            }
        }
    }
}
//MARK: Webservices
extension ItemReviewListVC {
    func Webservice_ProductReviewList(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let reviewdata = jsonResponse!["reviews"].dictionaryValue
                    self.img_Ratings.image = UIImage.init(named: "0\(reviewdata["avg_ratting"]!.stringValue.first!)")
                    self.lbl_Ratings.text = "\(reviewdata["avg_ratting"]!.stringValue) / 5"
                    self.lbl_BasedRatings.text = "Based on \(reviewdata["total"]!.stringValue) Ratings & Reviews"
                    if reviewdata["total"]!.stringValue != "0"
                    {
                        let progress_5 = reviewdata["five_ratting"]!.floatValue / reviewdata["total"]!.floatValue
                        self.progress_5.setProgress(Float(progress_5), animated: true)
                        
                        let progress_4 = reviewdata["four_ratting"]!.floatValue / reviewdata["total"]!.floatValue
                        self.progress_4.setProgress(Float(progress_4), animated: true)
                        
                        let progress_3 = reviewdata["three_ratting"]!.floatValue / reviewdata["total"]!.floatValue
                        self.progress_3.setProgress(Float(progress_3), animated: true)
                        
                        let progress_2 = reviewdata["two_ratting"]!.floatValue / reviewdata["total"]!.floatValue
                        self.progress_2.setProgress(Float(progress_2), animated: true)
                        
                        let progress_1 = reviewdata["one_ratting"]!.floatValue / reviewdata["total"]!.floatValue
                        self.progress_1.setProgress(Float(progress_1), animated: true)
                    }
                    else
                    {
                        self.img_Ratings.image = UIImage.init(named: "0\(0)")
                    }
                    
                    
                    
                    let reviewData = jsonResponse!["all_review"].dictionaryValue
                    if self.pageIndex == 1 {
                        self.lastIndex = Int(reviewData["last_page"]!.stringValue)!
                        self.ReviewsArray.removeAll()
                    }
                    let ReviewListData = reviewData["data"]!.arrayValue
                    for data in ReviewListData
                    {
                        let usersData = data["users"].dictionaryValue
                        let Obj = ["user_id":data["user_id"].stringValue,"ratting":data["ratting"].stringValue,"comment":data["comment"].stringValue,"date":data["date"].stringValue,"user_name":usersData["name"]!.stringValue,"image_url":usersData["image_url"]!.stringValue]
                        self.ReviewsArray.append(Obj)
                    }
                    
                    self.Tableview_ItemReviewList.delegate = self
                    self.Tableview_ItemReviewList.dataSource = self
                    self.Tableview_ItemReviewList.reloadData()
                    
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
