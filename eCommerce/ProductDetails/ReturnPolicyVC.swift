//
//  ReturnPolicyVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 15/09/21.
//

import UIKit

class ReturnPolicyVC: UIViewController {

    @IBOutlet weak var textview_Return: UITextView!
    var ReturnValue = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.textview_Return.text = self.ReturnValue
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }

}
