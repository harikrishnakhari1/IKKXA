//
//  WritereviewVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 02/07/21.
//

import UIKit
import SwiftyJSON
import SDWebImage

class WritereviewVC: UIViewController {
    
    @IBOutlet weak var textview_Writereview: UITextView!
    
    @IBOutlet weak var lbl_itemName: UILabel!
    @IBOutlet weak var btn_Submit: UIButton!
    @IBOutlet weak var img_item: UIImageView!
    @IBOutlet weak var view_ratings: FloatRatingView!
    @IBOutlet weak var lbl_title: UILabel!
    var liveStr = String()
    var vendor_id = String()
    var product_id = String()
    var product_image = String()
    var productName = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "Rattings & Reviews".localiz()
        self.btn_Submit.setTitle("Submit".localized(), for: .normal)
        
        self.view_ratings.delegate = self
        self.lbl_itemName.text = self.productName
        self.textview_Writereview.delegate = self
        self.textview_Writereview.text = "Write your review".localized()
        self.textview_Writereview.textColor = UIColor.lightGray
        setBorder(viewName: textview_Writereview, borderwidth: 1, borderColor: UIColor.lightGray.cgColor, cornerRadius: 6.0)
        cornerRadius(viewName: self.img_item, radius: 8.0)
        self.img_item.sd_imageTransition = .fade
        self.img_item.sd_setImage(with: URL(string: self.product_image), placeholderImage: UIImage(named: "b_00"))
        
        
    }
    
    
}
extension WritereviewVC
{
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    @IBAction func btnTap_Submit(_ sender: UIButton) {
        if self.textview_Writereview.text! == "" || self.textview_Writereview.text! == "Write your review".localiz() {
            showAlertMessage(titleStr: "", messageStr: "Please enter all details".localiz())
        }
        else {
            let urlString = API_URL + "addratting"
            let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                        "ratting":self.liveStr,
                                        "comment":self.textview_Writereview.text!,"vendor_id":self.vendor_id,"product_id":self.product_id]
            self.Webservice_WriteReview(url: urlString, params: params)
        }
    }
}
extension WritereviewVC : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write your review"
            textView.textColor = UIColor.lightGray
        }
    }
}
//MARK: Functions
extension WritereviewVC: FloatRatingViewDelegate {
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        self.liveStr = String(format: "%.f", self.view_ratings.rating)
    }
}

//MARK: Webservices
extension WritereviewVC {
    func Webservice_WriteReview(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    self.dismiss(animated: false)
                }
                else {
                    self.dismiss(animated: false) {
                        showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                    }
                }
            }
        }
    }
}
