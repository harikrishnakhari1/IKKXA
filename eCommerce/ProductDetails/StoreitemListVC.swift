//
//  StoreitemListVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 02/07/21.
//

import UIKit
import SwiftyJSON
import SDWebImage
import ImageSlideshow
class StoreitemListCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Rating: UILabel!
    @IBOutlet weak var img_Like: UIButton!
    @IBOutlet weak var lbl_discountPrice: UILabel!
    @IBOutlet weak var lbl_MainPrice: UILabel!
    @IBOutlet weak var lbl_itemName: UILabel!
    @IBOutlet weak var img_Item: UIImageView!
}
class StoreitemListVC: UIViewController {
    @IBOutlet weak var img_Slider: ImageSlideshow!
    
    @IBOutlet weak var btn_about: UIButton!
    @IBOutlet weak var Tableview_StoreitemList: UITableView!
    @IBOutlet weak var lbl_titleName: UILabel!
    var StoreListArray = [[String:String]]()
    var vendor_id = String()
    var pageIndex = 1
    var lastIndex = 0
    var vendordetails = [String:JSON]()
    var productImages = [SDWebImageSource]()
    var StoreName = String()
    var imageURL = String()
    var store_rate = String()
    var image_name = String()
    
    @IBOutlet weak var height_Banner: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.height_Banner.constant = 0.0
        self.img_Slider.isHidden = true
        self.btn_about.isHidden = true
        self.lbl_titleName.text = self.StoreName
        self.Tableview_StoreitemList.tableFooterView = UIView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.pageIndex = 1
        self.lastIndex = 0
        let urlString = API_URL + "vendorproducts?page=\(self.pageIndex)"
        let params: NSDictionary = ["vendor_id":vendor_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
        self.Webservice_StoreitemList(url: urlString,params: params)
    }
}
extension StoreitemListVC
{
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    @IBAction func btnTap_About(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "StoreAboutVC") as! StoreAboutVC
        vc.vendordetails = self.vendordetails
        vc.imageURL = self.imageURL
        vc.storeName = self.StoreName
        vc.store_rate = self.store_rate
                vc.modalPresentationStyle = .overFullScreen
        //        vc.modalTransitionStyle = .crossDissolve
        self.present(vc,animated: true,completion: nil)
    }
}
//MARK: Tableview Methods
extension StoreitemListVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)

        imagedata.contentMode = .scaleAspectFit
        self.Tableview_StoreitemList.backgroundView = imagedata
        if self.StoreListArray.count == 0 {
            imagedata.image = UIImage(named: "ic_Nodata")
            
        }
        else {
            imagedata.image = UIImage(named: "")
        }
        return self.StoreListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_StoreitemList.dequeueReusableCell(withIdentifier: "StoreitemListCell") as! StoreitemListCell
        cornerRadius(viewName: cell.img_Item, radius: 4.0)
        let data = self.StoreListArray[indexPath.item]
        let is_variation = data["is_variation"]!
        let is_wishlist = data["is_wishlist"]!
        if is_wishlist == "1"
        {
            cell.img_Like.setImage(UIImage(named: "ic_heartfill"), for: .normal)
            cell.img_Like.tintColor = UIColor.red
            
        }
        else{
            cell.img_Like.setImage(UIImage(named: "ic_heart"), for: .normal)
            cell.img_Like.tintColor = UIColor.lightGray
        }
        if is_variation == "1"
        {
            let ProductPrice = formatter.string(for: data["variation_price"]!.toDouble)
            let ProductDiscountPrice = formatter.string(for: data["discounted_variation_price"]!.toDouble)
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                cell.lbl_MainPrice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                cell.lbl_discountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
            }
            else {
                cell.lbl_MainPrice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                cell.lbl_discountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
            }
            
        }
        else{
            let ProductPrice = formatter.string(for: data["product_price"]!.toDouble)
            let ProductDiscountPrice = formatter.string(for: data["discounted_price"]!.toDouble)
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                cell.lbl_MainPrice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                cell.lbl_discountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
            }
            else{
                cell.lbl_MainPrice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                cell.lbl_discountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
            }
        }
        cell.lbl_itemName.text = data["product_name"]!
        
        cell.img_Item.sd_imageTransition = .fade
        let items = indexPath.item % 6
        cell.img_Item.sd_setImage(with: URL(string: data["image_url"]!), placeholderImage: UIImage(named: "b_0\(items)"))
        cell.img_Like.tag = indexPath.row
        cell.img_Like.addTarget(self, action:#selector(btnTap_Favorites), for: .touchUpInside)
        cell.lbl_Rating.text = data["arg_ratting"]!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.StoreListArray.count - 1 {
            if self.pageIndex != self.lastIndex {
                self.pageIndex = self.pageIndex + 1
                if self.StoreListArray.count != 0 {
                    let urlString = API_URL + "vendorproducts?page=\(self.pageIndex)"
                    let params: NSDictionary = ["vendor_id":vendor_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                    self.Webservice_StoreitemList(url: urlString,params: params)
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.StoreListArray[indexPath.item]
        let vc = storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
        vc.item_id = data["id"]!
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
        
    }
    @objc func btnTap_Favorites(sender:UIButton!) {
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A"
        {
            UserDefaults.standard.set("", forKey:UD_userId)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            UIApplication.shared.windows[0].rootViewController = nav
        }
        else
        {
            let data = self.StoreListArray[sender.tag]
            let is_wishlist = data["is_wishlist"]!
            if is_wishlist == "1"
            {
                let urlString = API_URL + "removefromwishlist"
                let params: NSDictionary = ["product_id":data["id"]!,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_RemoveWishList(url: urlString,params: params, sender: sender.tag)
            }
            else{
                let urlString = API_URL + "addtowishlist"
                let params: NSDictionary = ["product_id":data["id"]!,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_AddtoWishList(url: urlString,params: params, sender: sender.tag)
            }
        }
    }
    func imageSliderData() {
        self.img_Slider.slideshowInterval = 3.0
        self.img_Slider.pageIndicatorPosition = .init(horizontal: .center, vertical: .customBottom(padding: 10.0))
        self.img_Slider.contentScaleMode = UIView.ContentMode.scaleAspectFill
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.img_Slider.pageIndicator = pageControl
        self.img_Slider.setImageInputs(self.productImages)
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapImage))
        self.img_Slider.addGestureRecognizer(recognizer)
    }
    @objc func didTapImage() {
        self.img_Slider.presentFullScreenController(from: self)
    }
}
extension StoreitemListVC
{
    func Webservice_StoreitemList(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let StoreData = jsonResponse!["data"].dictionaryValue
                    let productImages = jsonResponse!["banners"].arrayValue
                    if productImages.count != 0 {
                        self.image_name = productImages[0]["image_url"].stringValue
                        self.height_Banner.constant = 100.0
                        self.img_Slider.isHidden = false
                    }
                    else
                    {
                        self.height_Banner.constant = 0.0
                        self.img_Slider.isHidden = true
                    }
                    self.productImages.removeAll()
                    for image in productImages {
                        let imageSource = SDWebImageSource(url: URL(string: image["image_url"].stringValue)!)
                        self.productImages.append(imageSource)
                    }
                    
                    self.imageSliderData()
                    
                    if self.pageIndex == 1 {
                        self.lastIndex = Int(StoreData["last_page"]!.stringValue)!
                        self.StoreListArray.removeAll()
                    }
                    let StoreListData = StoreData["data"]!.arrayValue
                    for data in StoreListData
                    {
                        let imageUrl = data["productimage"].dictionaryValue
                        let variation = data["variation"].dictionaryValue
                        let rattings = data["rattings"].arrayValue
                        var arg_rattings = String()
                        
                        if rattings.count == 0
                        {
                            arg_rattings = "0.0"
                        }
                        else{
                            let ProductRatting = formatterRatting.string(for: rattings[0]["avg_ratting"].stringValue.toDouble)
                            arg_rattings = ProductRatting!
                        }
                        var StoreListObj = [String:String]()
                        if variation.count != 0
                        {
                            StoreListObj = ["id":data["id"].stringValue,"product_name":data["product_name"].stringValue,"product_price":data["product_price"].stringValue,"discounted_price":data["discounted_price"].stringValue,"is_variation":data["is_variation"].stringValue,"is_wishlist":data["is_wishlist"].stringValue,"image_url":imageUrl["image_url"]!.stringValue,"variation_id":variation["id"]!.stringValue,"variation_price":variation["price"]!.stringValue,"discounted_variation_price":variation["discounted_variation_price"]!.stringValue,"variation":variation["variation"]!.stringValue,"variation_qty":variation["qty"]!.stringValue,"arg_ratting":arg_rattings]
                        }
                        else{
                            StoreListObj = ["id":data["id"].stringValue,"product_name":data["product_name"].stringValue,"product_price":data["product_price"].stringValue,"discounted_price":data["discounted_price"].stringValue,"is_variation":data["is_variation"].stringValue,"is_wishlist":data["is_wishlist"].stringValue,"image_url":imageUrl["image_url"]!.stringValue,"variation_id":"","variation_price":"","discounted_variation_price":"","variation":"","variation_qty":"","arg_ratting":"0.0"]
                            
                        }
                        self.StoreListArray.append(StoreListObj)
                    }
                    self.vendordetails = jsonResponse!["vendordetails"].dictionaryValue
                    if self.vendordetails.count == 0
                    {
                        self.btn_about.isHidden = true
                        
                    }
                    else{
                        self.btn_about.isHidden = false
                    }
                    
                    
                    self.Tableview_StoreitemList.delegate = self
                    self.Tableview_StoreitemList.dataSource = self
                    self.Tableview_StoreitemList.reloadData()
                }
                else if responseCode == "0" {
                    self.Tableview_StoreitemList.delegate = self
                    self.Tableview_StoreitemList.dataSource = self
                    self.Tableview_StoreitemList.reloadData()
                }
                else
                {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_AddtoWishList(url:String, params:NSDictionary,sender:Int) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    var data = self.StoreListArray[sender]
                    data["is_wishlist"]! = "1"
                    self.StoreListArray.remove(at: sender)
                    self.StoreListArray.insert(data, at: sender)
                    self.Tableview_StoreitemList.reloadData()
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_RemoveWishList(url:String, params:NSDictionary,sender:Int) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    
                    var data = self.StoreListArray[sender]
                    data["is_wishlist"]! = "0"
                    self.StoreListArray.remove(at: sender)
                    self.StoreListArray.insert(data, at: sender)
                    self.Tableview_StoreitemList.reloadData()
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
