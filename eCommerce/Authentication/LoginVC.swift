//
//  LoginVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 24/04/21.
//

import UIKit
import SwiftyJSON
import GoogleSignIn
import FacebookCore
import FacebookLogin
import FBSDKCoreKit
import FBSDKLoginKit

class LoginVC: UIViewController {
    
    var isFromCart = false
    
    //MARK: Outlets
    @IBOutlet weak var Password_view: UIView!
    @IBOutlet weak var Email_View: UIView!
    @IBOutlet weak var txt_Password: UITextField!
    @IBOutlet weak var txt_Email: UITextField!
    @IBOutlet weak var btn_login: UIButton!
    @IBOutlet weak var btn_showPassword: UIButton!
    @IBOutlet weak var facebook_view: UIView!
    @IBOutlet weak var google_view: UIView!
    @IBOutlet weak var btn_skip: UIButton!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_welcome: UILabel!
    @IBOutlet weak var btn_Forgotpassword: UIButton!
    @IBOutlet weak var lbl_strLogin: UILabel!
    @IBOutlet weak var btn_SignUp: UIButton!
    @IBOutlet weak var lbl_donthaveaccount: UILabel!
    
    
    //MARK: Veriable
    
    
    //MARK: Viewcontroller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lbl_welcome.text = "Welcome".localiz()
        self.lbl_title.text = "Login".localiz()
        self.btn_skip.setTitle("   "+"Skip & Continue".localiz()+"   ", for: .normal)
        self.btn_login.setTitle("Login".localiz(), for: .normal)
        self.lbl_strLogin.text = "Please login to your account".localiz()
        self.btn_SignUp.setTitle("Signup".localiz(), for: .normal)
        self.btn_Forgotpassword.setTitle("Forgot Password?".localiz(), for: .normal)
        self.txt_Email.placeholder = "Mobile".localiz()
        self.txt_Password.placeholder = "Password".localiz()
        
        cornerRadius(viewName: self.Email_View, radius: 6)
        cornerRadius(viewName: self.Password_view, radius: 6)
        cornerRadius(viewName: self.btn_login, radius: 6.0)
        self.btn_showPassword.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
        cornerRadius(viewName: self.google_view, radius: 6.0)
        cornerRadius(viewName: self.facebook_view, radius: 6.0)
        cornerRadius(viewName: self.btn_skip, radius: 6.0)
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "en" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "N/A" {
            self.txt_Email.textAlignment = .left
            self.txt_Password.textAlignment = .left
        }
        else {
            self.txt_Email.textAlignment = .right
            self.txt_Password.textAlignment = .right
        }
    }
    
}

//MARK: Actions
extension LoginVC {
    @IBAction func btnTap_ShowPassword(_ sender: UIButton) {
        if self.btn_showPassword.image(for: .normal) == UIImage(systemName: "eye.slash.fill") {
            self.btn_showPassword.setImage(UIImage(systemName: "eye.fill"), for: .normal)
            self.txt_Password.isSecureTextEntry = false
        }
        else {
            self.txt_Password.isSecureTextEntry = true
            self.btn_showPassword.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
        }
    }
    
    @IBAction func btnTap_forgotPassword(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ForgotPassword") as! ForgotPassword
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    
    @IBAction func btnTap_Signup(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "SignupVC") as! SignupVC
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    
    @IBAction func btnTap_Google(_ sender: UIButton) {
        self.GoogleLogin()
    }
    
    @IBAction func btnTap_Facebook(_ sender: UIButton) {
        self.FacebookLogin()
    }
    
    @IBAction func btnTap_Login(_ sender: UIButton) {
       
        if self.txt_Email.text! == "" || self.txt_Password.text! == "" {
            showAlertMessage(titleStr: "", messageStr: "Please enter the mobile number & password.".localiz())
        }
        else if ((self.txt_Email.text!.starts(with: "0"))) {
            showAlertMessage(titleStr: "", messageStr: "Mobile number should not start from 0 ".localiz())
        }
        else if self.txt_Email.text!.isNumeric == false {
            showAlertMessage(titleStr: "", messageStr: "Please enter valid Mobile number".localiz())
        }
        else {
            let urlString = API_URL + "login"
            let params: NSDictionary = ["email": "+966" + self.txt_Email.text!,
                                        "password":self.txt_Password.text!,
                                        "token":UserDefaultManager.getStringFromUserDefaults(key: UD_fcmToken)]
            self.Webservice_Login(url: urlString, params: params)
        }
    }
    
    @IBAction func btnTap_skip(_ sender: UIButton) {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userName)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        objVC.modalPresentationStyle = .fullScreen
        self.present(objVC, animated: false)

    }
}

//MARK: Functions
extension LoginVC {
    func FacebookLogin() {
        UserDefaults.standard.set(key_facebook, forKey: key_Type)
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.email], viewController: nil) { (loginResult) in
            switch loginResult {
            case .success( _, _, _):
                let dictParamaters = ["fields":"id, name, email"]
                let request: GraphRequest = GraphRequest.init(graphPath: "me", parameters: dictParamaters)
                request.start { (connection, result, error) in
                    let responseData = result as! NSDictionary
                    let facebookId = responseData["id"] as! String
                    let name = responseData["name"] as! String
                    var email = ""
                    if responseData["email"] != nil {
                        email = responseData["email"] as! String
                    }
                    loginManager.logOut()
                    let urlString = API_URL + "register"
                    let params: NSDictionary = ["name":name,
                                                "email":email,
                                                "mobile":"",
                                                "token":UserDefaultManager.getStringFromUserDefaults(key: UD_fcmToken),
                                                "login_type":"facebook",
                                                "facebook_id":facebookId]
                    self.Webservice_Register(url: urlString, params: params,email:email,name:name,google_Id:"",facebook_id:facebookId,logintype:"facebook",mobile: "")
                }
                break
            case .cancelled:
                showAlertMessage(titleStr: "", messageStr: "Something went wrong. Try again later".localiz())
                break
            case .failed( _):
                showAlertMessage(titleStr: "", messageStr: "Something went wrong. Try again later".localiz())
                break
            }
        }
    }
    
    func GoogleLogin() {
        UserDefaults.standard.set(key_google, forKey: key_Type)
//        GIDSignIn.sharedInstance.delegate = self
//        GIDSignIn.sharedInstance()?.presentingViewController = self
//        GIDSignIn.sharedInstance.signIn(with: <#GIDConfiguration#>, presenting: <#UIViewController#>)
        
        let signInConfig = GIDConfiguration.init(clientID: GoogleClient_Id)

        GIDSignIn.sharedInstance.signIn(with: signInConfig, presenting: self) { user, error in
            
            if error != nil {
                showAlertMessage(titleStr: "", messageStr: "Something went wrong. Try again later".localiz())
            }
            else {
                guard let user = user else { return }

             //   if let profiledata = user.profile {
                    
                  //  let userId : String = user.userID ?? ""
                    let googleId = user.userID!
                 //   let givenName : String = profiledata.givenName ?? ""
                   // let familyName : String = profiledata.familyName ?? ""
                    let name = user.profile?.name
                    let email = user.profile?.email
                    GIDSignIn.sharedInstance.disconnect()
                    GIDSignIn.sharedInstance.signOut()
                    let urlString = API_URL + "register"
                    let params: NSDictionary = ["name":name!,
                                                "email":email!,
                                                "mobile":"",
                                                "token":UserDefaultManager.getStringFromUserDefaults(key: UD_fcmToken),
                                                "google_id":googleId,
                                                "login_type":"google"]
                    self.Webservice_Register(url: urlString, params: params,email:email!,name:name!,google_Id:googleId,facebook_id:"",logintype:"google",mobile:"")
                    
//                    if let imgurl = user.profile?.imageURL(withDimension: 100) {
//                        let absoluteurl : String = imgurl.absoluteString
//                        //HERE CALL YOUR SERVER API
//                    }
           //     }
                
                
             
                
            }
            
            guard error == nil else { return }
           
            
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            showAlertMessage(titleStr: "", messageStr: "Something went wrong. Try again later".localiz())
        }
        else {
            let googleId = user.userID!
            let name = user.profile?.name
            let email = user.profile?.email
            GIDSignIn.sharedInstance.disconnect()
            GIDSignIn.sharedInstance.signOut()
            let urlString = API_URL + "register"
            let params: NSDictionary = ["name":name!,
                                        "email":email!,
                                        "mobile":"",
                                        "token":UserDefaultManager.getStringFromUserDefaults(key: UD_fcmToken),
                                        "google_id":googleId,
                                        "login_type":"google"]
            self.Webservice_Register(url: urlString, params: params,email:email!,name:name!,google_Id:googleId,facebook_id:"",logintype:"google",mobile:"")
        }
    }
}
extension LoginVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txt_Email {
            let updatedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
            if updatedText.starts(with: "0") && updatedText.count > 1 {
                textField.text = String(updatedText.dropFirst())
                return false
            }
        }
        return true
    }

}
//MARK: Webservices
extension LoginVC {
    func Webservice_Login(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let userData = jsonResponse!["data"].dictionaryValue
                    let userId = userData["id"]!.stringValue
                    UserDefaultManager.setStringToUserDefaults(value: userId, key: UD_userId)
                    UserDefaultManager.setStringToUserDefaults(value: userData["name"]!.stringValue, key: UD_userName)
                    UserDefaultManager.setStringToUserDefaults(value: userData["email"]!.stringValue, key: UD_userEmail)
                    UserDefaultManager.setStringToUserDefaults(value: userData["mobile"]!.stringValue, key: UD_userMobile)
                    UserDefaultManager.setStringToUserDefaults(value: userData["profile_pic"]!.stringValue, key: UD_userImage)
                    UserDefaultManager.setStringToUserDefaults(value: userData["referral_code"]!.stringValue, key: UD_userReferCode)
                    if self.isFromCart {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let objVC = storyBoard.instantiateViewController(withIdentifier: "MyCartVC") as! MyCartVC
                        objVC.modalPresentationStyle = .fullScreen
                        self.present(objVC, animated: true)
                    } else {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let objVC = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        objVC.modalPresentationStyle = .fullScreen
                        self.present(objVC, animated: true)
                    }
                    

                }
                else if responseCode == "2" {
                    let vc = self.storyboard?.instantiateViewController(identifier: "EmailVerifyVC") as! EmailVerifyVC
                    vc.Email = self.txt_Email.text!
                    vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    
    func Webservice_Register(url:String, params:NSDictionary,email:String,name:String,google_Id:String,facebook_id:String,logintype:String,mobile:String) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let userData = jsonResponse!["data"].dictionaryValue
                    let userId = userData["id"]!.stringValue
                    UserDefaultManager.setStringToUserDefaults(value: userId, key: UD_userId)
                    UserDefaultManager.setStringToUserDefaults(value: userData["name"]!.stringValue, key: UD_userName)
                    UserDefaultManager.setStringToUserDefaults(value: userData["email"]!.stringValue, key: UD_userEmail)
                    UserDefaultManager.setStringToUserDefaults(value: userData["mobile"]!.stringValue, key: UD_userMobile)
                    UserDefaultManager.setStringToUserDefaults(value: userData["profile_pic"]!.stringValue, key: UD_userImage)
                    UserDefaultManager.setStringToUserDefaults(value: userData["referral_code"]!.stringValue, key: UD_userReferCode)
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let objVC = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    objVC.modalPresentationStyle = .fullScreen
                    self.present(objVC, animated: true)

                }
                else if responseCode == "2" {
                    let vc = self.storyboard?.instantiateViewController(identifier: "SignupVC") as! SignupVC
                    vc.email = email
                    vc.name = name
                    vc.google_Id = google_Id
                    vc.facebook_Id = facebook_id
                    vc.isloginType = logintype
                    vc.mobile = mobile
                    vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false)
                }
                else if responseCode == "3" {
                    let vc = self.storyboard?.instantiateViewController(identifier: "EmailVerifyVC") as! EmailVerifyVC
                    vc.Email = email
                    vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
