//
//  ForgotPassword.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 24/04/21.
//

import UIKit
import SwiftyJSON

class ForgotPassword: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var txt_Email: UITextField!
    @IBOutlet weak var btn_signup: UIButton!
    @IBOutlet weak var lbl_donthaveaccount: UILabel!
    @IBOutlet weak var lbl_forgotpassword: UILabel!
    
    @IBOutlet weak var verifyOtpSpace: NSLayoutConstraint!

    @IBOutlet weak var txt_confirm_password: UITextField!
    @IBOutlet weak var txt_enter_otp: UITextField!
    @IBOutlet weak var txt_enter_password: UITextField!
    @IBOutlet weak var btn_continue_outlet: UIButton!
    @IBAction func btn_continue(_ sender: UIButton) {
        if (self.txt_enter_otp.text! == "") {
              showAlertMessage(titleStr: "", messageStr: "Please enter OTP")
          }
        else if(self.txt_enter_password.text! == ""){
            showAlertMessage(titleStr: "", messageStr: "please enter password")
        }
        else if(self.txt_confirm_password.text! == ""){
            showAlertMessage(titleStr: "", messageStr: "Enter enter confirm password")
        }
        else if(self.txt_confirm_password.text! != self.txt_enter_password.text!){
            showAlertMessage(titleStr: "", messageStr: "Password and confirm password should be same")
        }
        else {
              let urlString = API_URL + "forgotpasswordverifyotp"
              let params: NSDictionary = ["country": "966",
                                          "mobile": self.txt_Email.text!,
                                          "otp":  self.txt_enter_otp.text!,
                                          "confirmPassword":  self.txt_confirm_password.text!]
              self.Webservice_ForgotPassword_SetPassword(url: urlString, params: params)
          }
    }
    
    
    @IBOutlet weak var lbl_forgot_password: UILabel!
    @IBOutlet weak var lblForgotSubTitle: UILabel!
    //MARK: Viewcontroller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print("ForgotPassword")
        self.lbl_forgotpassword.text = "Forgot Password".localiz()
        self.lblForgotSubTitle.text = "Enter your registered mobile number below. We will send OTP in your mobile."
        self.txt_Email.placeholder = "Mobile".localiz()
        self.btn_submit.setTitle("Submit".localiz(), for: .normal)
        self.lbl_donthaveaccount.text = "Don't have an account?".localiz()
        self.btn_signup.setTitle("Signup".localiz(), for: .normal)
        cornerRadius(viewName: self.btn_submit, radius: 6.0)
        if UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "en" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "N/A" {
            self.txt_Email.textAlignment = .left
        }
        else {
            self.txt_Email.textAlignment = .right
        }
        
        //self.verifyOtpSpace.constant = 5
        
        self.txt_enter_otp.isHidden = true
        self.txt_enter_password.isHidden = true
        self.txt_confirm_password.isHidden = true
        self.btn_continue_outlet.isHidden = true

    }
}

//MARK: Actions
extension ForgotPassword {
    @IBAction func btnTap_Signup(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "SignupVC") as! SignupVC
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
    @IBAction func btnTap_Submit(_ sender: UIButton) {
       /* if self.txt_Email.text! == "" {
            showAlertMessage(titleStr: "", messageStr: "Please enter all details".localiz())
        }
        else if self.txt_Email.text!.isEmail == false {
            showAlertMessage(titleStr: "", messageStr: "Please enter valid email".localiz())
        }*/
       if ((self.txt_Email.text!.starts(with: "0"))) {
            showAlertMessage(titleStr: "", messageStr: "Mobile number should not start from 0 ".localiz())
        }
        else if self.txt_Email.text!.isNumeric == false {
            showAlertMessage(titleStr: "", messageStr: "Please enter valid Mobile number".localiz())
        }
        else {
            let urlString = API_URL + "forgotpasswordotp"
            let params: NSDictionary = ["country": "966",
                                        "mobile": self.txt_Email.text!]
            self.ForgotPasswordRequestOtp(url: urlString, params: params)
        }
        
        
      
    }
}

//MARK: Webservices
extension ForgotPassword {
    func ForgotPasswordRequestOtp(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    showAlertMessageSuccess(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                  //  self.dismiss(animated: true)
                    
                    
                    self.txt_Email.isHidden = true
                    self.btn_submit.isHidden = true
                    
                    //self.lbl_forgot_password.text! = ""///jsonResponse!["message"].stringValue
                    
                    self.txt_enter_otp.isHidden = false
                    self.txt_enter_password.isHidden = false
                    self.txt_confirm_password.isHidden = false
                    self.btn_continue_outlet.isHidden = false
                }
                else {
                    showAlertMessageSuccess(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    
    
    func Webservice_ForgotPassword_SetPassword(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    showAlertMessageSuccess(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                    
                    
                    self.dismiss(animated: false)
                    
                    self.txt_Email.isHidden = true
                    self.btn_submit.isHidden = true
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
extension ForgotPassword: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txt_Email {
            let updatedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
            if updatedText.starts(with: "0") && updatedText.count > 1 {
                textField.text = String(updatedText.dropFirst())
                return false
            }
        }
        return true
    }

}
