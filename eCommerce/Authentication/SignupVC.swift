//
//  SignupVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 24/04/21.
//

import UIKit
import SwiftyJSON
import CountryPickerView
class SignupVC: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var btn_signup: UIButton!
    @IBOutlet weak var txt_MobileNumber: UITextField!
    @IBOutlet weak var txt_Password: UITextField!
    @IBOutlet weak var txt_Email: UITextField!
    @IBOutlet weak var txt_FirstName: UITextField!
    @IBOutlet weak var btn_showPassword: UIButton!
    @IBOutlet weak var btn_Chekbox: UIButton!
    @IBOutlet weak var txt_ReferCode: UITextField!
    @IBOutlet weak var password_height: NSLayoutConstraint!
    @IBOutlet weak var btn_skip: UIButton!
    @IBOutlet weak var lbl_signup: UILabel!
    @IBOutlet weak var lbl_strsignuptogetstart: UILabel!
    @IBOutlet weak var lbl_strCreate: UILabel!
    @IBOutlet weak var btn_login: UIButton!
    @IBOutlet weak var lbl_alreadyhaveanaccount: UILabel!
    @IBOutlet weak var btn_accept: UIButton!
    @IBOutlet weak var btn_SelectCountry: UIButton!
    
    //MARK: Variables
    var iselectcheck = String()
    var isloginType = String()
    var email = String()
    var name = String()
    var google_Id = String()
    var mobile = String()
    var facebook_Id = String()
    var countryPickerView = CountryPickerView()
    var CountryCode = String()
    //MARK: Viewcontroller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        self.CountryCode = "+966"
        self.iselectcheck = "no"
        self.lbl_strCreate.text = "Create account".localiz()
        self.lbl_strsignuptogetstart.text = "Signup to get started!".localiz()
        self.btn_skip.setTitle("   "+"Become a Vendor?".localiz()+"   ", for: .normal)
        self.lbl_signup.text = "Signup".localiz()
        self.txt_FirstName.placeholder = "Full name".localiz()
        self.txt_Email.placeholder = "Email".localiz()
        self.txt_MobileNumber.placeholder = "Mobile".localiz()
        self.txt_Password.placeholder = "Password".localiz()
        self.btn_accept.setTitle("I accept the terms & conditions".localiz(), for: .normal)
        self.btn_signup.setTitle("Signup".localiz(), for: .normal)
        self.lbl_alreadyhaveanaccount.text = "Already have an account?".localiz()
        self.btn_login.setTitle("Login".localiz(), for: .normal)
        cornerRadius(viewName: self.btn_signup, radius: 6.0)
        cornerRadius(viewName: self.btn_skip, radius: 6.0)
        self.btn_showPassword.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
        self.btn_Chekbox.setImage(UIImage(systemName: "square"), for: .normal)
        if UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "en" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "N/A" {
            self.txt_FirstName.textAlignment = .left
            self.txt_Email.textAlignment = .left
            self.txt_Password.textAlignment = .left
            self.txt_MobileNumber.textAlignment = .left
            self.txt_ReferCode.textAlignment = .left
        }
        else {
            self.txt_FirstName.textAlignment = .right
            self.txt_Email.textAlignment = .right
            self.txt_Password.textAlignment = .right
            self.txt_MobileNumber.textAlignment = .right
            self.txt_ReferCode.textAlignment = .right
        }
        if self.isloginType == "google" || self.isloginType == "facebook" {
            if self.email != "" {
                self.txt_Email.isEnabled = false
            }
            else {
                self.txt_Email.isEnabled = true
            }
            self.txt_Email.text = self.email
            self.txt_FirstName.text = self.name
            self.password_height.constant = 0.0
            self.txt_MobileNumber.text = self.mobile
        }
        else {
            self.password_height.constant = 50.0
        }
    }
    
}
extension SignupVC : CountryPickerViewDataSource,CountryPickerViewDelegate
{
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print(country.phoneCode)
        self.CountryCode = country.phoneCode
        self.btn_SelectCountry.setTitle("\(CountryCode) ▼", for: .normal)
    }
}
//MARK: Actions
extension SignupVC {
    @IBAction func btnTap_SelectCountyCode(_ sender: UIButton) {
        countryPickerView.showCountriesList(from: self)
    }
    @IBAction func btnTap_ShowPassword(_ sender: UIButton) {
        
        if self.btn_showPassword.image(for: .normal) == UIImage(systemName: "eye.slash.fill") {
            self.btn_showPassword.setImage(UIImage(systemName: "eye.fill"), for: .normal)
            self.txt_Password.isSecureTextEntry = false
        }
        else {
            self.txt_Password.isSecureTextEntry = true
            self.btn_showPassword.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
        }
    }
    
    @IBAction func btnTap_signup(_ sender: UIButton) {
        
        if self.isloginType == "google" || self.isloginType == "facebook" {
            if self.txt_FirstName.text! == "" || self.txt_Email.text! == "" || self.txt_MobileNumber.text! == "" {
                showAlertMessage(titleStr: "", messageStr: "Please enter all details".localiz())
            }
            else if ((self.txt_MobileNumber.text!.starts(with: "0"))) {
                showAlertMessage(titleStr: "", messageStr: "Mobile number should not start from 0 ".localiz())
            }
            else if self.txt_MobileNumber.text!.isNumeric == false {
                showAlertMessage(titleStr: "", messageStr: "Please enter valid Mobile number".localiz())
            }
           else if self.txt_Email.text!.isEmail == false {
                showAlertMessage(titleStr: "", messageStr: "Please enter valid email".localiz())
            } 
            else if self.iselectcheck == "no" {
                showAlertMessage(titleStr: "", messageStr: "Please select terms & conditions".localiz())
            }
            else {
                let urlString = API_URL + "register"
                let params: NSDictionary = ["name":self.txt_FirstName.text!,
                                            "email":self.txt_Email.text!,
                                            "mobile":CountryCode + self.txt_MobileNumber.text!,
                                            "token":UserDefaultManager.getStringFromUserDefaults(key: UD_fcmToken),
                                            "google_id":self.google_Id,
                                            "login_type":self.isloginType,
                                            "register_type":"email",
                                            "facebook_id":self.facebook_Id,
                                            "referral_code":self.txt_ReferCode.text!]
                self.Webservice_Register(url: urlString, params: params)
            }
        }
        else {
            if self.txt_FirstName.text! == "" || self.txt_Email.text! == "" || self.txt_MobileNumber.text! == "" || self.txt_Password.text! == "" {
                showAlertMessage(titleStr: "", messageStr: "Please enter all details".localiz())
            }
            else if ((self.txt_MobileNumber.text!.starts(with: "0"))) {
                showAlertMessage(titleStr: "", messageStr: "Mobile number should not start from 0 ".localiz())
            }
            else if self.txt_MobileNumber.text!.isNumeric == false {
                showAlertMessage(titleStr: "", messageStr: "Please enter valid Mobile number".localiz())
            }
            else if self.txt_Email.text!.isEmail == false {
                showAlertMessage(titleStr: "", messageStr: "Please enter valid email".localiz())
            }
            else if self.iselectcheck == "no" {
                showAlertMessage(titleStr: "", messageStr: "Please select terms & conditions".localiz())
            }
            else {
                let urlString = API_URL + "register"
                let params: NSDictionary = ["name":self.txt_FirstName.text!,
                                            "email":self.txt_Email.text!,
                                            "mobile":self.CountryCode + self.txt_MobileNumber.text!,
                                            "password":self.txt_Password.text!,
                                            "token":UserDefaultManager.getStringFromUserDefaults(key: UD_fcmToken),
                                            "login_type":"email",
                                            "register_type":"email",
                                            "referral_code":self.txt_ReferCode.text!]
                self.Webservice_Register(url: urlString, params: params)
            }
        }
    }
    
    @IBAction func btnTap_Login(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "LoginVC") as! LoginVC
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    
    @IBAction func btnTap_Checkbox(_ sender: UIButton) {
        if self.btn_Chekbox.image(for: .normal) == UIImage(systemName: "square") {
            self.iselectcheck = "yes"
            self.btn_Chekbox.setImage(UIImage(systemName: "checkmark.square.fill"), for: .normal)
        }
        else {
            self.iselectcheck = "no"
            self.btn_Chekbox.setImage(UIImage(systemName: "square"), for: .normal)
        }
    }
    
    @IBAction func btnTap_Termandcondition(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "CMSVC") as! CMSVC
        vc.isType = "terms"
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    
    @IBAction func btnTap_skip(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "VendorVC") as! VendorVC
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
}

extension SignupVC {
    func Webservice_Register(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let vc = self.storyboard?.instantiateViewController(identifier: "EmailVerifyVC") as! EmailVerifyVC
                    vc.Email = self.txt_Email.text!
                    vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
extension SignupVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txt_Email {
            let updatedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
            if updatedText.starts(with: "0") && updatedText.count > 1 {
                textField.text = String(updatedText.dropFirst())
                return false
            }
        }
        return true
    }

}
