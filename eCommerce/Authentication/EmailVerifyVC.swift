//
//  EmailVerifyVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 24/04/21.
//

import UIKit
import SwiftyJSON
class EmailVerifyVC: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var btn_Verify: UIButton!
    @IBOutlet weak var txt_OTP: UITextField!
    @IBOutlet weak var lbl_strOTPverification: UILabel!
    @IBOutlet weak var btn_ResendOTP: UIButton!
    @IBOutlet weak var lbl_dontrceiveotp: UILabel!
    @IBOutlet weak var lbl_otpverification: UILabel!
    
    @IBOutlet weak var lbl_countDown: UILabel!
    
    //MARK: Variables
    var Email = String()
    var count = 119
    var timer = Timer()
    var mobileno = String()
    var verificationId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_countDown.text = "02:00"
        self.lbl_dontrceiveotp.isHidden = true
        self.btn_ResendOTP.isHidden = true
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        
        self.lbl_strOTPverification.text = "Check sms for OTP. Enter OTP below and proceed further.".localiz()
        self.lbl_otpverification.text = "OTP Verification".localiz()
        self.txt_OTP.placeholder = "Enter OTP here".localiz()
        self.lbl_dontrceiveotp.text = "Don't receive the OTP?".localiz()
        self.btn_ResendOTP.setTitle("Resend OTP", for: .normal)
        self.btn_Verify.setTitle("Verify & Proceed", for: .normal)
        cornerRadius(viewName: self.btn_Verify, radius: 6.0)
        if UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "en" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "N/A" {
            self.txt_OTP.textAlignment = .left
        }
        else {
            self.txt_OTP.textAlignment = .right
        }
    }
    @objc func update() {

        if(count > 0){
            let minutes = String(count / 60)
            let seconds = String(count % 60)
            if Int(seconds)! < 10
            {
                lbl_countDown.text = "0\(minutes)" + ":" + "0\(seconds)"
            }
            else{
                lbl_countDown.text = "0\(minutes)" + ":" + seconds
                
            }
            count -= 1
            
        }
        else
        {
            self.timer.invalidate()
            self.lbl_countDown.isHidden = true
            self.lbl_dontrceiveotp.isHidden = false
            self.btn_ResendOTP.isHidden = false
        }

    }
}

//MARK: Actions
extension EmailVerifyVC {
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
    @IBAction func btnTap_Verify(_ sender: UIButton) {
        
        if self.txt_OTP.text! == "" {
            showAlertMessage(titleStr: "", messageStr: "Please enter all details".localiz())
        }
        else {
            let urlString = API_URL + "emailverify"
            let params: NSDictionary = [
                "email":self.Email,
                "otp":self.txt_OTP.text!,
                "token":UserDefaultManager.getStringFromUserDefaults(key: UD_fcmToken)]
            self.Webservice_Emailverify(url: urlString, params: params)
        }
    }
    
    @IBAction func btnTap_ResendOTP(_ sender: UIButton) {
        let urlString = API_URL + "resendemailverification"
        let params: NSDictionary = ["email":self.Email]
        self.Webservice_resendemailverification(url: urlString, params: params)
    }
}

//MARK: Webservices
extension EmailVerifyVC {
    func Webservice_Emailverify(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let userData = jsonResponse!["data"].dictionaryValue
                    let userId = userData["id"]!.stringValue
                    UserDefaultManager.setStringToUserDefaults(value: userId, key: UD_userId)
                    UserDefaultManager.setStringToUserDefaults(value: userData["name"]!.stringValue, key: UD_userName)
                    UserDefaultManager.setStringToUserDefaults(value: userData["profile_pic"]!.stringValue, key: UD_userImage)
                    UserDefaultManager.setStringToUserDefaults(value: userData["email"]!.stringValue, key: UD_userEmail)
                    UserDefaultManager.setStringToUserDefaults(value: userData["mobile"]!.stringValue, key: UD_userMobile)
                    UserDefaultManager.setStringToUserDefaults(value: userData["referral_code"]!.stringValue, key: UD_userReferCode)
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let objVC = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    objVC.modalPresentationStyle = .fullScreen
                    self.present(objVC, animated: true)

                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    
    func Webservice_resendemailverification(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
