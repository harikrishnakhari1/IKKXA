//
//  MyCartVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 15/05/21.
//

import UIKit
import SwiftyJSON
import SDWebImage

//MARK: Cart Table Cell
class CartCell: UITableViewCell {
    
    @IBOutlet weak var lbl_ProductName: UILabel!
    @IBOutlet weak var img_Product: UIImageView!
    @IBOutlet weak var lbl_Weight: UILabel!
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet weak var btn_Minse: UIButton!
    @IBOutlet weak var btn_Pluse: UIButton!
    @IBOutlet weak var lbl_qty: UILabel!
    
    @IBOutlet weak var lbl_Cancel: UILabel!
    
    @IBOutlet weak var lbl_Tax: UILabel!
    @IBOutlet weak var lbl_ShipingCost: UILabel!
}

class MyCartVC: UIViewController {
    
    @IBOutlet weak var btn_CheckOut: UIButton!
    @IBOutlet weak var TableView_MyCartList: UITableView!
    @IBOutlet weak var lbl_title: UILabel!
    var CartArray = [JSON]()
    var cart_id = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "My Cart".localiz()
        self.btn_CheckOut.setTitle("Checkout".localiz(), for: .normal)
        self.btn_CheckOut.isHidden = true
        self.TableView_MyCartList.tableFooterView = UIView()
        self.TableView_MyCartList.delegate = self
        self.TableView_MyCartList.dataSource = self
        self.TableView_MyCartList.reloadData()
    }
    @IBAction func btnTap_Next(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "CheckoutVC") as! CheckoutVC
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
            appNavigation.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.windows[0].rootViewController = appNavigation
        }
        else {
            let urlString = API_URL + "getcart"
            let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
            self.Webservice_GetCart(url: urlString, params: params)
        }
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case is TabbarController:
            let tabbarController = segue.destination as? TabbarController
            tabbarController?.delegate = self
            
        default:
            break
        }
    }
}
//MARK: Tableview Methods
extension MyCartVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)

        imagedata.contentMode = .scaleAspectFit
        self.TableView_MyCartList.backgroundView = imagedata
        if self.CartArray.count == 0 {
            imagedata.image = UIImage(named: "ic_Empty-rafiki")
            self.btn_CheckOut.isHidden = true
        }
        else {
            imagedata.image = UIImage(named: "")
            self.btn_CheckOut.isHidden = false
        }
        return self.CartArray.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TableView_MyCartList.dequeueReusableCell(withIdentifier: "CartCell") as! CartCell
        
        cornerRadius(viewName: cell.img_Product, radius: 4.0)
        let data = self.CartArray[indexPath.row]
        let ItemPrice = data["qty"].doubleValue * data["price"].doubleValue
        let Qty_Price = formatter.string(for: ItemPrice)!
        if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
        {
            cell.lbl_Price.text = "\(Qty_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
        }
        else {
            
            cell.lbl_Price.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Qty_Price)"
        }
        
        cell.lbl_count.text = data["qty"].stringValue
        cell.lbl_ProductName.text = data["product_name"].stringValue
        
        if data["variation"].stringValue == ""
        {
            cell.lbl_Weight.text = "-"
        }
        else{
            cell.lbl_Weight.text = "\(data["attribute"].stringValue) : \(data["variation"].stringValue)"
        }
        
        
        cell.img_Product.sd_imageTransition = .fade
        let items = indexPath.item % 6
        cell.img_Product.sd_setImage(with: URL(string: data["image_url"].stringValue), placeholderImage: UIImage(named: "b_0\(items)"))
        
        
        if Int(data["qty"].stringValue)! <= 1 {
            cell.btn_Minse.isEnabled = false
        }
        else {
            cell.btn_Minse.isEnabled = true
        }
        
        cell.btn_Minse.tag = indexPath.row
        cell.btn_Minse.addTarget(self, action: #selector(btnTapMines), for: .touchUpInside)
        cell.btn_Pluse.tag = indexPath.row
        cell.btn_Pluse.addTarget(self, action: #selector(btnTapPluse), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            
            let data = self.CartArray[indexPath.row]
            
            self.cart_id = data["id"].stringValue
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConformationAlertVC") as! ConformationAlertVC
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.deleget = self
            vc.isSelectedTypes = "1"
            self.present(vc,animated: true,completion: nil)
            
        }
    }
    
    @objc func btnTapMines(sender:UIButton) {
        let data = CartArray[sender.tag]
        var qty = Int(data["qty"].stringValue)
        qty = qty! - 1
        
        let urlString = API_URL + "qtyupdate"
        let params: NSDictionary = ["qty":"\(qty!)","cart_id":data["id"].stringValue]
        self.Webservice_QtyUpdatetoCart(url: urlString, params:params)
    }
    
    @objc func btnTapPluse(sender:UIButton) {
        let data = CartArray[sender.tag]
        var qty = Int(data["qty"].stringValue)
        qty = qty! + 1
        let urlString = API_URL + "qtyupdate"
        let params: NSDictionary = ["qty":"\(qty!)","cart_id":data["id"].stringValue]
        self.Webservice_QtyUpdatetoCart(url: urlString, params:params)
        
//        let limitsorderCount = UserDefaultManager.getStringFromUserDefaults(key: UD_MaxOrderQty)
//        if qty! <= Int(limitsorderCount)! {
//            let urlString = API_URL + "qtyupdate"
//            let params: NSDictionary = ["qty":"\(qty!)","user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
//            self.Webservice_QtyUpdatetoCart(url: urlString, params:params)
//        }
//        else {
//            showAlertMessage(titleStr: "", messageStr: "You've reached the maximum units allowed for the purchase of this item".localiz())
//        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = CartArray[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
        vc.item_id = data["product_id"].stringValue
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
}

//MARK: Webservices
extension MyCartVC
{
    func Webservice_GetCart(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let response = jsonResponse!["data"].arrayValue
                    self.CartArray = response
                    self.TableView_MyCartList.delegate = self
                    self.TableView_MyCartList.dataSource = self
                    self.TableView_MyCartList.reloadData()
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_QtyUpdatetoCart(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let urlString = API_URL + "getcart"
                    let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                    self.Webservice_GetCart(url: urlString, params: params)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_DeleteItemtoCart(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
//                    self.CartArray.remove(at: sender)
//                    self.TableView_MyCartList.reloadData()
                    let urlString = API_URL + "getcart"
                    let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                    self.Webservice_GetCart(url: urlString, params: params)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
extension MyCartVC : DismissAlertDeleget
{
    func DimissSucess() {
        
        let urlString = API_URL + "deleteproduct"
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"cart_id":self.cart_id]
        self.Webservice_DeleteItemtoCart(url: urlString, params: params)
    }
    
    
}
extension MyCartVC: TabbarControllerDelegate
{
    func tabbarController(_ viewController: TabbarController, selected option: TabbarOption) {
        switch option {
        case .home:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.homeViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .category:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.categoryViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .cart:
            break
        case .wishlist:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.wishlistViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .profile:
            if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A" {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.logInViewController)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false)
            } else {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.profileViewController)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false)
            }
        }
    }
    
}
