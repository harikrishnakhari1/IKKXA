//
//  SetAddressVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 31/07/21.
//

import UIKit
import SwiftyJSON
import SDWebImage

class CheckoutVC: UIViewController {
    
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var btn_Edit: UIButton!
    @IBOutlet weak var textView_Note: UITextView!
    @IBOutlet weak var btn_SelectAddress: UIButton!
    @IBOutlet weak var btn_Apply: UIButton!
    @IBOutlet weak var Height_CouponView: NSLayoutConstraint!
    @IBOutlet weak var View_coupon: UIView!
    
    @IBOutlet weak var lbl_DiscountString: UILabel!
    @IBOutlet weak var txt_CouponCode: UITextField!
    @IBOutlet weak var lbl_Total: UILabel!
    @IBOutlet weak var lbl_Discount: UILabel!
    @IBOutlet weak var lbl_ShippingCharge: UILabel!
    @IBOutlet weak var lbl_tax: UILabel!
    @IBOutlet weak var lbl_Subtotal: UILabel!
    
    @IBOutlet weak var Height_TableView: NSLayoutConstraint!
    @IBOutlet weak var Tableview_ItemListting: UITableView!
    @IBOutlet weak var btn_Havecoupon: UIButton!
    var Address_data = JSON()
    @IBOutlet weak var lblstr_promoapply: UILabel!
    @IBOutlet weak var lblstr_billingaddress: UILabel!
    @IBOutlet weak var lblstr_Orderinfo: UILabel!
    @IBOutlet weak var lblstr_Subtotal: UILabel!
    @IBOutlet weak var lblstr_Tax: UILabel!
    @IBOutlet weak var lblstr_Shipping: UILabel!
    @IBOutlet weak var lblstr_Total: UILabel!
    @IBOutlet weak var lblstr_note: UILabel!
    
    @IBOutlet weak var btn_ProccedtoPayment: UIButton!
    var CartArray = [JSON]()
    var Order_Total = String()
    var orderinfo = [String : JSON]()
    var Promoinfo = [String : JSON]()
    var ispromoType = String()
    var Coupon_name = String()
    var Discount_Amount = String()
    var ispromoApply = String()
    //    var Promocodeinfo = [String : JSON]()
    var myArray = [Double]()
    var highPrice = Double()
    var highPriceCost : Double = 0.0
    var highPriceCostFlag : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_Title.text = "Checkout".localiz()
        self.btn_Havecoupon.setTitle("Have a coupon? Click here to enter your code".localiz(), for: .normal)
        self.lblstr_promoapply.text = "If you have a coupon code, please apply it below.".localiz()
        self.btn_Apply.setTitle("Apply".localiz(), for: .normal)
        self.lblstr_billingaddress.text = "Billing / Shiping address".localiz()
        self.btn_Edit.setTitle("EDIT".localiz(), for: .normal)
        self.btn_SelectAddress.setTitle("Select Address".localiz(), for: .normal)
        self.btn_ProccedtoPayment.setTitle("Proceed to Payment".localiz(), for: .normal)
        self.lblstr_Orderinfo.text = "Order Info".localiz()
        self.lblstr_Subtotal.text = "Subtotal".localiz()
        self.lblstr_Tax.text = "Tax".localiz()
        self.lblstr_Shipping.text = "Shipping".localiz()
        self.lblstr_Total.text = "Total".localiz()
        self.lblstr_note.text = "Note".localiz()
        
        
        
        self.btn_Edit.isHidden = true
        self.btn_SelectAddress.isHidden = false
        self.Height_CouponView.constant = 0.0
        self.View_coupon.isHidden = true
        
        self.textView_Note.delegate = self
        self.textView_Note.text = "Order note".localiz()
        self.textView_Note.textColor = UIColor.lightGray
        
        cornerRadius(viewName: self.textView_Note, radius: 6.0)
        cornerRadius(viewName: self.btn_Apply, radius: 4.0)
        cornerRadius(viewName: self.btn_SelectAddress, radius: 4.0)
        self.Height_TableView.constant = 0.0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let urlString = API_URL + "checkout"
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"coupon_name":self.txt_CouponCode.text!]
        self.Webservice_CheckOut(url: urlString, params: params)
    }
}
extension CheckoutVC
{
    @IBAction func btnTap_HaveCouponCode(_ sender: UIButton)
    {
        self.Height_CouponView.constant = 100.0
        self.View_coupon.isHidden = false
    }
    @IBAction func btnTap_Back(_ sender: UIButton)
    {
        self.dismiss(animated: false)
    }
    @IBAction func btnTap_Procedtopayment(_ sender: UIButton) {
        
        if self.lbl_Address.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter Billing / Shiping address".localiz())
        }
        else{
            if self.textView_Note.text == "Order note".localiz()
            {
                self.textView_Note.text = ""
            }
//            var discount_price = [String]()
//            for data in CartArray
//            {
//                discount_price.append(formatter.string(for: data["discount_amount"].stringValue.toDouble)!)
//            }
            let vc = self.storyboard?.instantiateViewController(identifier: "PaymentVC") as! PaymentVC
            vc.Address_data = self.Address_data
            vc.Order_Total = self.Order_Total.replacingOccurrences(of: ",", with: "")
            vc.Order_note = self.textView_Note.text!
            vc.coupon_name = self.Coupon_name
            vc.discount_amount = String("0") //DISABLED FEATURE   //self.orderinfo["discount_amount"]!.stringValue
            vc.vendor_id = self.orderinfo["vendor_id"]!.stringValue
            vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
        }
        
    }
    
    @IBAction func btnTap_Apply(_ sender: UIButton)
    {
        if self.btn_Apply.titleLabel?.text == "Remove".localiz()
        {
            let tax_Price = formatter.string(for: self.orderinfo["tax"]!.stringValue.toDouble)!
            let subtotal_Price = formatter.string(for: self.orderinfo["subtotal"]!.stringValue.toDouble)!
            let shipping_cost_Price = formatter.string(for: self.orderinfo["shipping_cost"]!.stringValue.toDouble)!
            
            if self.ispromoType == "0"
            {
                
                if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                {
                    
                    self.lbl_tax.text = "\(tax_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    self.lbl_Subtotal.text = "\(subtotal_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    self.lbl_ShippingCharge.text = "\(shipping_cost_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    
                    self.lbl_Discount.text = "-0.00\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    
//                    let GrandTotal =  self.orderinfo["subtotal"]!.doubleValue + self.orderinfo["tax"]!.doubleValue + self.orderinfo["shipping_cost"]!.doubleValue
//                    let Total_Price = formatter.string(for: GrandTotal)!
                    let Total_Price = formatter.string(for: self.orderinfo["grand_total"]!.stringValue.toDouble)!
                    self.lbl_Total.text = "\(Total_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    self.Order_Total = Total_Price
                }
                else {
                    
                    self.lbl_tax.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(tax_Price)"
                    self.lbl_Subtotal.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(subtotal_Price)"
                    self.lbl_ShippingCharge.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(shipping_cost_Price)"
                    
                    self.lbl_Discount.text = "-\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))0.00"
                    
                    
//                    let GrandTotal =  self.orderinfo["subtotal"]!.doubleValue + self.orderinfo["tax"]!.doubleValue + self.orderinfo["shipping_cost"]!.doubleValue
//                    let Total_Price = formatter.string(for: GrandTotal)!
                    let Total_Price = formatter.string(for: self.orderinfo["grand_total"]!.stringValue.toDouble)!
                    self.lbl_Total.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Total_Price)"
                    self.Order_Total = Total_Price
                }
            }
            else if self.ispromoType == "1"{
                
                if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                {
                    
                    self.lbl_tax.text = "\(tax_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    self.lbl_Subtotal.text = "\(subtotal_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    self.lbl_ShippingCharge.text = "\(shipping_cost_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    
                    self.lbl_Discount.text = "-0.00\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    
//                    let GrandTotal =  self.orderinfo["subtotal"]!.doubleValue + self.orderinfo["tax"]!.doubleValue + self.orderinfo["shipping_cost"]!.doubleValue
//                    let Total_Price = formatter.string(for: GrandTotal)!
                    let Total_Price = formatter.string(for: self.orderinfo["grand_total"]!.stringValue.toDouble)!
                    self.lbl_Total.text = "\(Total_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    self.Order_Total = Total_Price
                }
                else {
                    
                    self.lbl_tax.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(tax_Price)"
                    self.lbl_Subtotal.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(subtotal_Price)"
                    self.lbl_ShippingCharge.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(shipping_cost_Price)"
                    
                    self.lbl_Discount.text = "-\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))0.00"
                    
//                    let GrandTotal =  self.orderinfo["subtotal"]!.doubleValue + self.orderinfo["tax"]!.doubleValue + self.orderinfo["shipping_cost"]!.doubleValue
//                    let Total_Price = formatter.string(for: GrandTotal)!
                    let Total_Price = formatter.string(for: self.orderinfo["grand_total"]!.stringValue.toDouble)!
                    self.lbl_Total.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Total_Price)"
                    self.Order_Total = Total_Price
                }
                
                
            }
            self.txt_CouponCode.text = ""
            self.btn_Apply.setTitle("Apply", for: .normal)
            self.btn_Apply.setTitleColor(UIColor.black, for: .normal)
            self.txt_CouponCode.resignFirstResponder()
            self.txt_CouponCode.isEnabled = true
            self.lbl_DiscountString.text = "Discount"
            
            self.ispromoApply = "0"
            let urlString = API_URL + "checkout"
            let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"coupon_name":self.txt_CouponCode.text!]
            self.Webservice_CheckOut(url: urlString, params: params)
            
        }
        else if self.btn_Apply.titleLabel?.text == "Apply".localiz()
        {
            if self.txt_CouponCode.text! == ""
            {
                showAlertMessage(titleStr: "", messageStr: "Please enter coupon code".localiz())
            }
            else{
                self.ispromoApply = "1"
                let urlString = API_URL + "checkout"
                let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"coupon_name":self.txt_CouponCode.text!]
                self.Webservice_CheckOut(url: urlString, params: params)
            }
        }
        
    }
    
    @IBAction func btnTap_SelectAddress(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(identifier: "ManageAddressVC") as! ManageAddressVC
        vc.isCheckoutPage = "1"
        vc.delegate = self
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    
    @IBAction func btnTap_Edit(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(identifier: "ManageAddressVC") as! ManageAddressVC
        vc.isCheckoutPage = "1"
        vc.delegate = self
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
}

extension CheckoutVC : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Order note".localiz()
            textView.textColor = UIColor.lightGray
        }
    }
}
extension CheckoutVC : getAddressDelegate
{
    func getAddressData(Address: JSON) {
        print(Address)
        if Address.count != 0
        {
            self.lbl_Address.text! = "\(Address["first_name"].stringValue) \(Address["last_name"].stringValue)\n\(Address["street_address"].stringValue) \(Address["landmark"].stringValue)- \(Address["pincode"].stringValue)\n\(Address["mobile"].stringValue)\n\(Address["email"].stringValue)"
            self.btn_SelectAddress.isHidden = true
            self.btn_Edit.isHidden = false
            self.Address_data = Address
        }
        else
        {
            self.btn_SelectAddress.isHidden = false
            self.btn_Edit.isHidden = true
        }
        
    }
}


//MARK: Tableview Methods
extension CheckoutVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rect = CGRect(origin: CGPoint(x: 0,y: 0), size: CGSize(width: self.Tableview_ItemListting.bounds.size.width, height: self.Tableview_ItemListting.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.textColor = UIColor.lightGray
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "POPPINS-REGULAR", size: 15)!
        messageLabel.sizeToFit()
        self.Tableview_ItemListting.backgroundView = messageLabel
        if self.CartArray.count == 0 {
            messageLabel.text = "No data found".localiz()
        }
        else {
            messageLabel.text = ""
        }
        return self.CartArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_ItemListting.dequeueReusableCell(withIdentifier: "CartCell") as! CartCell
        cornerRadius(viewName: cell.img_Product, radius: 4.0)
        let data = self.CartArray[indexPath.row]
        let ItemPrice = data["qty"].doubleValue * data["price"].doubleValue
        let Qty_Price = formatter.string(for: ItemPrice)!
        let Tax_Price = formatter.string(for: data["tax"].stringValue.toDouble)!
        let Shipping_cost = formatter.string(for: data["shipping_cost"].stringValue.toDouble)!
       // let discount_amount_Price = formatter.string(for: data["discount_amount"].stringValue.toDouble)!
        
        
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
        {
            cell.lbl_Price.text = "\(Qty_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
            cell.lbl_Tax.text =  "\(Tax_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
            cell.lbl_ShipingCost.text = "\(Shipping_cost)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
           // cell.lbl_Discount.text = "\(discount_amount_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
        }
        else {
            cell.lbl_Price.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Qty_Price)"
            cell.lbl_Tax.text =  "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Tax_Price)"
            if(highPriceCost == Shipping_cost.toDouble){
                if(!highPriceCostFlag){
                    highPriceCostFlag = true
                    cell.lbl_ShipingCost.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(self.highPriceCost)"
                }else{
                    cell.lbl_ShipingCost.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\("0")"
                }
            }else{
                cell.lbl_ShipingCost.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\("0")"
            }
          //  cell.lbl_Discount.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(discount_amount_Price)"
        }
        
        cell.lbl_ProductName.text = data["product_name"].stringValue
        
        if data["variation"].stringValue == ""
        {
            cell.lbl_Weight.text = "-"
        }
        else{
            cell.lbl_Weight.text = "\(data["attribute"].stringValue) : \(data["variation"].stringValue)"
        }
        cell.lbl_qty.text = "Qty : \(data["qty"].stringValue)"
        
        
        cell.img_Product.sd_imageTransition = .fade
        let items = indexPath.item % 6
        cell.img_Product.sd_setImage(with: URL(string: data["image_url"].stringValue), placeholderImage: UIImage(named: "b_0\(items)"))
        
        
        //        cell.lbl_Tax.text = data[""].stringValue
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = CartArray[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
        vc.item_id = data["product_id"].stringValue
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
}
//MARK: Webservices
extension CheckoutVC
{
    func Webservice_CheckOut(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let response = jsonResponse!["data"].dictionaryValue
                    self.orderinfo = response
                    self.CartArray = jsonResponse!["cartdata"].arrayValue
                    self.Height_TableView.constant = CGFloat(self.CartArray.count * 160)
                    self.Tableview_ItemListting.delegate = self
                    self.Tableview_ItemListting.dataSource = self
                    self.Tableview_ItemListting.reloadData()
                    let tax_Price = formatter.string(for: response["tax"]!.stringValue.toDouble)!
                    let subtotal_Price = formatter.string(for: response["subtotal"]!.stringValue.toDouble)!
                    var shipping_cost_Price = formatter.string(for: response["shipping_cost"]!.stringValue.toDouble)!
                    self.Coupon_name = jsonResponse!["coupon_name"].stringValue
                    
//                    var dicount_amount = Double()
//                    for data in self.CartArray
//                    {
//                        dicount_amount = dicount_amount + self.orderinfo["discount_amount"]!.doubleValue
//
//                    }
                    
                    
                   /* checkOutDataList?.forEach {
                           if(it.shippingCost?.toDouble()!! > highShippingCharge){
                                                   highShippingCharge = it.shippingCost.toDouble();
                                               }
                      } */
                    
                    //let cities = ["Amsterdam", "New York", "San Francisco"]

                    for response in self.CartArray {
                        print("Hello \(response["shipping_cost"])")
                        if(response["shipping_cost"].stringValue.toDouble >  self.highPriceCost){
                            self.highPriceCost = response["shipping_cost"].stringValue.toDouble
                        }
                    }
                    
                    
                    // 25/08/22 07:58 PM By  Shoaib Ahmed
                    // NOTE :- HIDE Because Disount amount coming null from server
                    // let dicount_price = formatter.string(from:NSNumber(value:self.orderinfo["discount_amount"]!.doubleValue))!
                    
                    let dicount_price : Double = 0.0 // DISCOUNT FEATURE DISABLED
                    
                    if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                    {
                        self.lbl_tax.text = "\(tax_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                        self.lbl_Subtotal.text = "\(subtotal_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                        self.lbl_ShippingCharge.text = "\(shipping_cost_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                        let GrandTotal =  response["subtotal"]!.doubleValue + response["tax"]!.doubleValue + response["shipping_cost"]!.doubleValue - dicount_price
                        let Total_Price = formatter.string(for: GrandTotal)!

                        // 25/08/22 07:58 PM By  Shoaib Ahmed
                       // NOTE :- HIDE Because Disount amount coming null from server hard coded 0.00 value
                      //  let Total_Price = formatter.string(for: response["grand_total"]!.stringValue.toDouble)!
                        
                        self.lbl_Total.text = "\(Total_Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                        self.Order_Total = Total_Price
                        self.lbl_Discount.text = "-\(dicount_price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    }
                    else {
                        self.lbl_tax.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(tax_Price)"
                        self.lbl_Subtotal.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(subtotal_Price)"
                        self.lbl_ShippingCharge.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(self.highPriceCost)"
                        //let GrandTotal =  response["subtotal"]!.doubleValue + response["tax"]!.doubleValue + response["shipping_cost"]!.doubleValue - dicount_price
                        
                        let GrandTotal =  response["subtotal"]!.doubleValue + response["tax"]!.doubleValue + self.highPriceCost - dicount_price

                        let Total_Price = formatter.string(for: GrandTotal)!
                        
                         // 25/08/22 07:58 PM By  Shoaib Ahmed
                        // NOTE :- HIDE Because Discount amount coming null from server
                       // let Total_Price =  formatter.string(for: response["grand_total"]!.stringValue.toDouble)!
                        
                        self.lbl_Total.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Total_Price)"
                        self.Order_Total = Total_Price
                        self.lbl_Discount.text = "-\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(dicount_price)"
                    }
                    if self.Coupon_name == ""
                    {
                        self.lbl_DiscountString.text = "Discount"
                    }
                    else{
                        self.lbl_DiscountString.text = "Discount (\(self.Coupon_name))"
                    }
                    
                    if self.ispromoApply == "1"
                    {
                        self.btn_Apply.setTitle("Remove", for: .normal)
                        self.btn_Apply.setTitleColor(UIColor.red, for: .normal)
                        self.txt_CouponCode.resignFirstResponder()
                        self.txt_CouponCode.isEnabled = false
                    }
                    else if self.ispromoApply == "0"
                    {
                        self.txt_CouponCode.text = ""
                        self.btn_Apply.setTitle("Apply", for: .normal)
                        self.btn_Apply.setTitleColor(UIColor.black, for: .normal)
                        self.txt_CouponCode.resignFirstResponder()
                        self.txt_CouponCode.isEnabled = true
                        self.lbl_DiscountString.text = "Discount"
                    }
                    
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
