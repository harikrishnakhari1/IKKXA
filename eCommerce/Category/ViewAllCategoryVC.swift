//
//  ViewAllCategoryVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 12/06/21.
//

import UIKit
import SwiftyJSON
import SDWebImage

class ViewAllCategoriesCell: UITableViewCell {
    
    @IBOutlet weak var cell_view: CornerView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var img_Categories: UIImageView!
}
class ViewAllCategoryVC: UIViewController {
    
    @IBOutlet weak var Tableview_AllCategories: UITableView!
    @IBOutlet weak var lbl_title: UILabel!
    var AllCategoryArray = [JSON]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "All Categories".localiz()
        self.Tableview_AllCategories.tableFooterView = UIView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // MARK:- Category Api
        let urlString = API_URL + "category"
        self.Webservice_GetCategory(url: urlString,params: [:])
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case is TabbarController:
            let tabbarController = segue.destination as? TabbarController
            tabbarController?.delegate = self
            
        default:
            break
        }
    }
}
extension ViewAllCategoryVC: TabbarControllerDelegate
{
    func tabbarController(_ viewController: TabbarController, selected option: TabbarOption) {
        switch option {
        case .home:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.homeViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .category:
            break
        case .cart:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.cartViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .wishlist:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.wishlistViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .profile:
            if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A" {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.logInViewController)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false)
            } else {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.profileViewController)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false)
            }
        }
    }
    
}

//MARK: Tableview Methods
extension ViewAllCategoryVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)

        imagedata.contentMode = .scaleAspectFit
        self.Tableview_AllCategories.backgroundView = imagedata
        if self.AllCategoryArray.count == 0 {
            imagedata.image = UIImage(named: "ic_Nodata")
        }
        else {
            imagedata.image = UIImage(named: "")
        }
        return self.AllCategoryArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.Tableview_AllCategories.dequeueReusableCell(withIdentifier: "ViewAllCategoriesCell", for: indexPath) as! ViewAllCategoriesCell
        cornerRadius(viewName: cell.img_Categories, radius: 8.0)
        let data = self.AllCategoryArray[indexPath.item]
        cell.lbl_name.text! = data["category_name"].stringValue.localiz()
        cell.img_Categories.sd_imageTransition = .fade
        let items = indexPath.item % 6
        cell.img_Categories.sd_setImage(with: URL(string: data["image_url"].stringValue), placeholderImage: UIImage(named: "b_0\(items)"))
        setBorder(viewName: cell.cell_view, borderwidth: 0.4, borderColor: UIColor.lightGray.cgColor, cornerRadius: 8.0)
        if items == 0 {
            cell.cell_view.backgroundColor = APP_COLOR1
        }
        else if items == 1 {
            cell.cell_view.backgroundColor = APP_COLOR2
        }
        else if items == 2 {
            cell.cell_view.backgroundColor = APP_COLOR3
        }
        else if items == 3 {
            cell.cell_view.backgroundColor = APP_COLOR4
        }
        else if items == 4 {
            cell.cell_view.backgroundColor = APP_COLOR5
        }
        else if items == 5 {
            cell.cell_view.backgroundColor = APP_COLOR6
            
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.AllCategoryArray[indexPath.item]
        let vc = self.storyboard?.instantiateViewController(identifier: "SubCategoryVC") as! SubCategoryVC
        vc.cat_id = data["id"].stringValue
        vc.category_name = data["category_name"].stringValue
        
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
}
//MARK: Webservices
extension ViewAllCategoryVC
{
    func Webservice_GetCategory(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "GET", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    self.AllCategoryArray = jsonResponse!["data"].arrayValue
                    self.Tableview_AllCategories.delegate = self
                    self.Tableview_AllCategories.dataSource = self
                    self.Tableview_AllCategories.reloadData()
                }
                else if responseCode == "2" {
                    
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
