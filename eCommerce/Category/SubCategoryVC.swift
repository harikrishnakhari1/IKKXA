//
//  SubCategoryVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 12/06/21.
//

import UIKit
import SwiftyJSON
import SDWebImage

class AllCategoryCell: UITableViewCell {
    @IBOutlet weak var lbl_categories: UILabel!
    
}
class SubCategoryVC: UIViewController {

    @IBOutlet weak var Tableview_Subcategory: UITableView!
    @IBOutlet weak var lbl_CategoryName: UILabel!
  
    var cat_id = String()
    var category_name = String()
    var sections = [Section]()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Tableview_Subcategory.tableFooterView = UIView()
        self.lbl_CategoryName.text = self.category_name
        let urlString = API_URL + "subcategory"
        let params: NSDictionary = ["cat_id":self.cat_id]
        self.Webservice_GetSubCategory(url: urlString, params: params)
        
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    

}
//MARK: Tableview Methods
extension SubCategoryVC: UITableViewDelegate,UITableViewDataSource,ExpabdableHeaderViewDelegate {
    func toggleSection(header: ExpabdableHeaderView, section: Int) {
        self.sections[section].expanded = !self.sections[section].expanded
        
        self.Tableview_Subcategory.beginUpdates()
        
        for i in 0 ..< self.sections[section].subcategory.count{
            self.Tableview_Subcategory.reloadRows(at: [IndexPath(row:i,section:section)], with: .automatic)
        }
        
        self.Tableview_Subcategory.endUpdates()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)

        imagedata.contentMode = .scaleAspectFit
        self.Tableview_Subcategory.backgroundView = imagedata
        if self.sections.count == 0 {
            imagedata.image = UIImage(named: "ic_Nodata")
        }
        else {
            imagedata.image = UIImage(named: "")
        }
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return (self.sections[section].subcategory.count)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.sections[indexPath.section].expanded!{
            return 50.0
        }
        else{
            return 0.0
        }
    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 1.0
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = ExpabdableHeaderView()
        header.customImit(title: self.sections[section].category, section: section, delegate: self)
        return header
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_Subcategory.dequeueReusableCell(withIdentifier: "AllCategoryCell") as! AllCategoryCell
        cell.lbl_categories.text = self.sections[indexPath.section].subcategory[indexPath.row]["innersubcategory_name"]!
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewallitemVC") as! ViewallitemVC
        vc.innersubcategory_id = self.sections[indexPath.section].subcategory[indexPath.row]["id"]!
        vc.istype = self.sections[indexPath.section].subcategory[indexPath.row]["innersubcategory_name"]!
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
}
//MARK: Webservices
extension SubCategoryVC
{
    func Webservice_GetSubCategory(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let AllSubcategoryArray = jsonResponse!["data"]["subcategory"].arrayValue
                    
                    for data in AllSubcategoryArray
                    {
                        var innersubcategory = [[String:String]]()
                        for innercate in data["innersubcategory"].arrayValue
                        {
                            let obj = ["innersubcategory_name":innercate["innersubcategory_name"].stringValue, "innersubcategoryImage":innercate["innersubcategory_image"].stringValue,"id":innercate["id"].stringValue]
                            innersubcategory.append(obj)
                        }
                        
                        self.sections.append(contentsOf: [Section(category: data["subcategory_name"].stringValue, categoryImage: data["subcategory_image"].stringValue, subcategory: innersubcategory, expanded: false)])
                    }
                    self.Tableview_Subcategory.delegate = self
                    self.Tableview_Subcategory.dataSource = self
                    self.Tableview_Subcategory.reloadData()
                }
                else if responseCode == "2" {
                  
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    
}
