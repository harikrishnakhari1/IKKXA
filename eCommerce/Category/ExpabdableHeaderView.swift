//
//  ExpabdableHeaderView.swift
//  TableView_ExpandableHeader_demo
//
//  Created by Galaxy on 22/11/17.
//  Copyright © 2017 Galaxy. All rights reserved.
//

import UIKit

protocol ExpabdableHeaderViewDelegate {
    func toggleSection(header:ExpabdableHeaderView,section:Int)
}

class ExpabdableHeaderView: UITableViewHeaderFooterView {

    var delegate:ExpabdableHeaderViewDelegate?
    var section:Int?
    
    override init(reuseIdentifier:String?){
        super.init(reuseIdentifier: reuseIdentifier)
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectHeaderAction)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func selectHeaderAction(gestureRecognizer:UITapGestureRecognizer){
        let cell = gestureRecognizer.view as! ExpabdableHeaderView
        delegate?.toggleSection(header: self, section: cell.section!)
    }
    
    func customImit(title:String,section:Int,delegate:ExpabdableHeaderViewDelegate){
        self.textLabel?.text = title
        
        self.section = section
        self.delegate = delegate
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.textLabel?.textColor = UIColor.init(named: "Black")
        self.contentView.backgroundColor = UIColor.init(named: "LightGray_Color")
    }
}
