import Foundation
import UIKit
import SystemConfiguration
import CoreLocation
import NotificationBannerSwift

var formatter = NumberFormatter()
var formatterRatting = NumberFormatter()
let APP_COLOR = Color_RGBA(30, 102, 234, 1)
let BLCK_COLOR = Color_RGBA(38, 50, 56, 1)
let GRAY_COLOR = Color_RGBA(154, 154, 154, 1)


//let APP_COLOR1 = Color_RGBA(208, 238, 249, 1)
//let APP_COLOR2 = Color_RGBA(254, 235, 209, 1)
//let APP_COLOR3 = Color_RGBA(240, 215, 249, 1)
//let APP_COLOR4 = Color_RGBA(254, 227, 220, 1)
//let APP_COLOR5 = Color_RGBA(234, 248, 237, 1)

let APP_COLOR1 = hexStringToUIColor(hex:"FDF7FF")
let APP_COLOR2 = hexStringToUIColor(hex:"FDF3F0")
let APP_COLOR3 = hexStringToUIColor(hex:"EDF7FD")
let APP_COLOR4 = hexStringToUIColor(hex:"FFFAEA")
let APP_COLOR5 = hexStringToUIColor(hex:"F1FFF6")
let APP_COLOR6 = hexStringToUIColor(hex:"FFF5EC")
let APP_COLOR7 = hexStringToUIColor(hex:"FFFFFF")



var Notification_Banner = NotificationBanner(title: "", subtitle: "", style: .danger)

let UD_userId = "UD_userId"
let UD_userName = "UD_userName"
let UD_userImage = "UD_userImage"
let UD_userMobile = "UD_userMobile"
let UD_userReferCode = "UD_userReferCode"
let UD_userEmail = "UD_userEmail"
let UD_isSelectLng = "UD_isSelectLng"
let UD_currency = "UD_currency"
let UD_currency_position = "UD_currency_position"
let UD_isTutorial = "UD_isTutorial"
let UD_fcmToken = "UD_fcmToken"
let UD_SelectedLat = "UD_SelectedLat"
let UD_SelectedLng = "UD_SelectedLng"
let UD_MaxOrderAmount = "UD_MaxOrderAmount"
let UD_MinOrderAmount = "UD_MinOrderAmount"
let UD_MaxOrderQty = "UD_MaxOrderQty"
let UD_referral_amount =  "UD_referral_amount"
let UD_STRIPE_PUBLIC_KEY =  "UD_STRIPE_PUBLIC_KEY"
let UD_RAZORPAY_KEY = "UD_RAZORPAY_KEY"

let UD_DismisHomeBanner = "UD_DismisHomeBanner"


let key_Type = "Type"
let key_facebook = "key_facebook"
let key_google = "key_google"

let kVersion        = "CFBundleShortVersionString"
let kBuildNumber    = "CFBundleVersion"

func setDecimalNumber() {
    formatter.numberStyle = .decimal
    formatter.maximumFractionDigits = 2
    formatter.minimumFractionDigits = 2
    formatter.locale = Locale(identifier: "en_US")
}
func setRatingsNumber() {
    formatterRatting.numberStyle = .decimal
    formatterRatting.maximumFractionDigits = 1
    formatterRatting.minimumFractionDigits = 1
    formatterRatting.locale = Locale(identifier: "en_US")
}

enum HttpResponseStatusCode: Int {
    case ok = 200
    case badRequest = 400
    case noAuthorization = 401
}

extension Bundle {
    var displayName: String? {
        return infoDictionary?["CFBundleDisplayName"] as? String
    }
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}

public func Color_RGBA(_ R: Int,_ G: Int,_ B: Int,_ A: Int) -> UIColor {
    return UIColor(red: CGFloat(R)/255.0, green: CGFloat(G)/255.0, blue: CGFloat(B)/255.0, alpha :CGFloat(A))
}

public func SetCornerToView(_ view : UIView,BorderColor : UIColor,BorderWidth : CGFloat)    {
    view.layer.borderColor = BorderColor.cgColor;
    view.layer.borderWidth = BorderWidth;
}

public func setBorder(viewName: UIView , borderwidth : Float , borderColor: CGColor , cornerRadius: CGFloat) {
    //    viewName.backgroundColor = UIColor.white
    viewName.layer.borderWidth = CGFloat(borderwidth)
    viewName.layer.borderColor = borderColor
    viewName.layer.cornerRadius = cornerRadius
}

func showAlertMsg(Message: String, AutoHide:Bool) -> Void {
    DispatchQueue.main.async {
        let alert = UIAlertController(title: "", message: Message, preferredStyle: UIAlertController.Style.alert)
        if AutoHide == true {
            let deadlineTime = DispatchTime.now() + .seconds(2)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                print("Alert Dismiss")
                alert.dismiss(animated: true, completion:nil)
            }
        }
        else {
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        }
        UIApplication.shared.windows[0].rootViewController?.present(alert, animated: true, completion: nil)
    }
}

func showAlertMessage(titleStr:String, messageStr:String) -> Void {
    DispatchQueue.main.async {
        Notification_Banner.dismiss()
        Notification_Banner = NotificationBanner(title: titleStr, subtitle: messageStr, style: .danger)
        Notification_Banner.duration = 2
        Notification_Banner.show()
    }
   
    //    DispatchQueue.main.async {
    //        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertController.Style.alert);
    //        alert.addAction(UIAlertAction(title: "Okay".localiz(), style: UIAlertAction.Style.default, handler: nil))
    //        UIApplication.shared.windows[0].rootViewController!.present(alert, animated: true, completion: nil)
    //    }
}

func showAlertMessageSuccess(titleStr:String, messageStr:String) -> Void {
    DispatchQueue.main.async {
        Notification_Banner.dismiss()
        Notification_Banner = NotificationBanner(title: titleStr, subtitle: messageStr, style: .success)
        Notification_Banner.duration = 2
        Notification_Banner.show()
    }
}

public func cornerRadius(viewName:UIView, radius: CGFloat) {
    viewName.layer.cornerRadius = radius
    viewName.layer.masksToBounds = true
}

public func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func getDocumentDirectoryPath() -> URL? {
    return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
}

func nullToNil(value : AnyObject?) -> AnyObject? {
    if value is NSNull {
        return nil
    }
    else {
        return value
    }
}

func nullToNill(value : Any?) -> String? {
    if value is NSNull {
        return nil
    } else {
        return value as? String
    }
}

func randomString(length: Int) -> String {
    let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    let len = UInt32(letters.length)
    var randomString = ""
    for _ in 0 ..< length {
        let rand = arc4random_uniform(len)
        var nextChar = letters.character(at: Int(rand))
        randomString += NSString(characters: &nextChar, length: 1) as String
    }
    return randomString
}

func isConnectedToNetwork() -> Bool {
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    })
    else {
        return false
    }
    var flags : SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
        return false
    }
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    let available =  (isReachable && !needsConnection)
    if(available) {
        return true
    }
    else {
        showAlertMsg(Message: "INTERNET_LOSS", AutoHide: true)
        return false
    }
}

func isValidateEmail(email:String) -> Bool {
    let emailRegx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegx)
    let result = emailTest.evaluate(with: email)
    return result
}

func isValidPassword(testStr:String) -> Bool {
    let passwordRegex = "^(?=^.{6,}$)(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s)[0-9a-zA-Z!@#$%^&*()]*$"
    return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: testStr)
}

func viewSlideInFromTop(toBottom views: UIView) {
    let transition = CATransition()
    transition.duration = 0.5
    transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    transition.type = CATransitionType.push
    transition.subtype = CATransitionSubtype.fromBottom
    views.layer.add(transition, forKey: nil)
}

func animateview(vw1 : UIView,vw2:UIView) {
    UIView.animate(withDuration: 0.1,
                   delay: 0.1,
                   options: UIView.AnimationOptions.curveEaseIn,
                   animations: { () -> Void in
                    vw1.alpha = 0;
                    vw2.alpha = 1;
                   }, completion: { (finished) -> Void in
                    vw1.isHidden = true;
                   })
}

func getVersion() -> String {
    let dictionary  = Bundle.main.infoDictionary!
    let version     = dictionary[kVersion] as! String
    let build       = dictionary[kBuildNumber] as! String
    
    return "\(version) (\(build))"
}

extension String {
    var toDouble: Double {
        return Double(self) ?? 0.00
    }
}

extension Double {
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

enum mathFunction {
    case Add
    case Subtract
}



class AFWrapperClass : NSObject {
    class func compareStringValue(currentValue:String, limit:Int, toDo : mathFunction) -> String {
        var current : Int = Int(currentValue)!
        if (current <= limit) && (current >= 0) {
            if toDo == .Add {
                if current == limit {
                    return String(current)
                }
                else{
                    current += 1
                    return String(current)
                }
            }
            else {
                if current == 1 {
                    return String(current)
                }
                else {
                    current -= 1
                    return String(current)
                }
            }
        }
        else {
            return String(current)
        }
    }
}
