import Foundation
import UIKit

//DOMAIN URL

let DOMAIN_URL = "https://ikkxa.com/"

//------------DON'T CHANGE ANYTHING BELOW-------------
//API URL
let API_URL = DOMAIN_URL + "api/"

//PRIVACY URL
let PRIVACY_URL = DOMAIN_URL + "privacypolicy"

//ABOUT US URL
let ABOUTUS_URL = DOMAIN_URL + "aboutus"

//TERMSCONDTIONS URL
let TERMSCONDTIONS_URL = DOMAIN_URL + "termscondition"

//WEPAYMENT URL
let WEPAYMENT_URL = DOMAIN_URL + "wepayment.php?grand_total="

//----------------------------------------------

//GOOGLE CLIENT ID
let GoogleClient_Id = "478006428135-gkil3q9cakmevpuqktp3a82cgbe6gh27.apps.googleusercontent.com"
