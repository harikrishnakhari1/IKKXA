//
//  Constants.swift
//  eCommerce
//
//  Created by mac on 16/03/23.
//

import Foundation


enum TabbarOption: Int {
    case home = 0, category, cart, wishlist, profile
}

struct Identifiers {
    static let tabBarViewController = "TabbarController"
    static let homeViewController = "HomeVC"
    static let categoryViewController = "ViewAllCategoryVC"
    static let cartViewController = "MyCartVC"
    static let wishlistViewController = "MyWishListVC"
    static let profileViewController = "MyAccountVC"
    static let logInViewController = "LoginVC"
}
