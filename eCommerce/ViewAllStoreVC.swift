//
//  ViewAllStoreVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 20/08/21.
//

import UIKit
import SwiftyJSON
import SDWebImage
class ViewAllStore: UITableViewCell {
    
    @IBOutlet weak var lbl_rating: UILabel!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var img_Store: UIImageView!
}
class ViewAllStoreVC: UIViewController {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var Tableview_AllStoreList: UITableView!
    var StoreArray = [[String:String]]()
    var pageIndex = 1
    var lastIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "All Store".localiz()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // MARK:- Category Api
        self.pageIndex = 1
        self.lastIndex = 0
        let urlString = API_URL + "vendors"
        self.Webservice_GetStoreList(url: urlString,params: [:])
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
}
//MARK: Tableview Methods
extension ViewAllStoreVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)

        imagedata.contentMode = .scaleAspectFit
        self.Tableview_AllStoreList.backgroundView = imagedata
        if self.StoreArray.count == 0 {
            imagedata.image = UIImage(named: "ic_Nodata")
            
        }
        else {
            imagedata.image = UIImage(named: "")
        }
        return self.StoreArray.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.Tableview_AllStoreList.dequeueReusableCell(withIdentifier: "ViewAllStore", for: indexPath) as! ViewAllStore
        cornerRadius(viewName: cell.img_Store, radius: 8)
        let data = self.StoreArray[indexPath.row]
        cell.lbl_Name.text! = data["name"]!
        cell.lbl_rating.text = data["rattings"]!
        
        cell.img_Store.sd_imageTransition = .fade
        let items = indexPath.row % 6
        cell.img_Store.sd_setImage(with: URL(string: data["image_url"]!), placeholderImage: UIImage(named: "b_0\(items)"))
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.StoreArray[indexPath.item]
        let vc = self.storyboard?.instantiateViewController(identifier: "StoreitemListVC") as! StoreitemListVC
        vc.vendor_id = data["id"]!
        vc.StoreName = data["name"]!
        vc.imageURL = data["image_url"]!
        vc.store_rate = data["rattings"]!
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.StoreArray.count - 1 {
            if self.pageIndex != self.lastIndex {
                self.pageIndex = self.pageIndex + 1
                if self.StoreArray.count != 0 {
                    let urlString = API_URL + "vendors?page=\(self.pageIndex)"
                    self.Webservice_GetStoreList(url: urlString,params: [:])
                }
            }
        }
    }
    
}
//MARK: Webservices
extension ViewAllStoreVC
{
    func Webservice_GetStoreList(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "GET", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let ResponseData = jsonResponse!["vendors"].dictionaryValue
                    if self.pageIndex == 1 {
                        self.lastIndex = Int(ResponseData["last_page"]!.stringValue)!
                        self.StoreArray.removeAll()
                    }
                    let ListData = ResponseData["data"]!.arrayValue
                    for data in ListData
                    {
                        let rattings = data["rattings"].dictionaryValue
                        var avg_ratting = String()
                        if rattings.count != 0
                        {
                            let ProductRatting = formatterRatting.string(for: rattings["avg_ratting"]!.stringValue.toDouble)
                            avg_ratting = ProductRatting!
                        }
                        else{
                            avg_ratting = "0.0"
                        }
                        
                        
                        let Obj = ["id":data["id"].stringValue,"name":data["name"].stringValue,"image_url":data["image_url"].stringValue,"rattings":avg_ratting]
                        
                        self.StoreArray.append(Obj)
                    }
                    self.Tableview_AllStoreList.delegate = self
                    self.Tableview_AllStoreList.dataSource = self
                    self.Tableview_AllStoreList.reloadData()
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
