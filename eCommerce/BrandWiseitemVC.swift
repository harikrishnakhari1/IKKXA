//
//  BrandWiseitemVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 11/09/21.
//

import UIKit
import SwiftyJSON
import SDWebImage

class BrandWiseitemVC: UIViewController {
    @IBOutlet weak var Tableview_BranditemList: UITableView!
    @IBOutlet weak var lbl_titleName: UILabel!
    var BranditemListArray = [[String:String]]()
    var Brand_id = String()
    var pageIndex = 1
    var lastIndex = 0
    var vendordetails = [String:JSON]()
    var StoreName = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lbl_titleName.text = self.StoreName
        self.Tableview_BranditemList.tableFooterView = UIView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.pageIndex = 1
        self.lastIndex = 0
        let urlString = API_URL + "brandsproducts?page=\(self.pageIndex)"
        let params: NSDictionary = ["brand_id":Brand_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
        self.Webservice_BranditemList(url: urlString,params: params)
    }

    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
}
//MARK: Tableview Methods
extension BrandWiseitemVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)

        imagedata.contentMode = .scaleAspectFit
        self.Tableview_BranditemList.backgroundView = imagedata
        if self.BranditemListArray.count == 0 {
            imagedata.image = UIImage(named: "ic_Nodata")
            
        }
        else {
            imagedata.image = UIImage(named: "")
        }
        return self.BranditemListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_BranditemList.dequeueReusableCell(withIdentifier: "StoreitemListCell") as! StoreitemListCell
        cornerRadius(viewName: cell.img_Item, radius: 4.0)
        let data = self.BranditemListArray[indexPath.item]
        let is_variation = data["is_variation"]!
        let is_wishlist = data["is_wishlist"]!
        if is_wishlist == "1"
        {
            cell.img_Like.setImage(UIImage(named: "ic_heartfill"), for: .normal)
            cell.img_Like.tintColor = UIColor.red
            
        }
        else{
            cell.img_Like.setImage(UIImage(named: "ic_heart"), for: .normal)
            cell.img_Like.tintColor = UIColor.lightGray
        }
        if is_variation == "1"
        {
            let ProductPrice = formatter.string(for: data["variation_price"]!.toDouble)
            let ProductDiscountPrice = formatter.string(for: data["discounted_variation_price"]!.toDouble)
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                cell.lbl_MainPrice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                cell.lbl_discountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
            }
            else {
                cell.lbl_MainPrice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                cell.lbl_discountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
            }
            
        }
        else{
            let ProductPrice = formatter.string(for: data["product_price"]!.toDouble)
            let ProductDiscountPrice = formatter.string(for: data["discounted_price"]!.toDouble)
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                cell.lbl_MainPrice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                cell.lbl_discountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
            }
            else{
                cell.lbl_MainPrice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                cell.lbl_discountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
            }
        }
        cell.lbl_itemName.text = data["product_name"]!
        
        cell.img_Item.sd_imageTransition = .fade
        let items = indexPath.item % 6
        cell.img_Item.sd_setImage(with: URL(string: data["image_url"]!), placeholderImage: UIImage(named: "b_0\(items)"))
        
        cell.img_Like.tag = indexPath.row
        cell.img_Like.addTarget(self, action:#selector(btnTap_Favorites), for: .touchUpInside)
        cell.lbl_Rating.text = data["arg_ratting"]!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.BranditemListArray.count - 1 {
            if self.pageIndex != self.lastIndex {
                self.pageIndex = self.pageIndex + 1
                if self.BranditemListArray.count != 0 {
                    let urlString = API_URL + "brandsproducts?page=\(self.pageIndex)"
                    let params: NSDictionary = ["brand_id":Brand_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                    self.Webservice_BranditemList(url: urlString,params: params)
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.BranditemListArray[indexPath.item]
        let vc = storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
        vc.item_id = data["id"]!
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
        
    }
    @objc func btnTap_Favorites(sender:UIButton!) {
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A"
        {
            UserDefaults.standard.set("", forKey:UD_userId)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            UIApplication.shared.windows[0].rootViewController = nav
        }
        else
        {
            let data = self.BranditemListArray[sender.tag]
            let is_wishlist = data["is_wishlist"]!
            if is_wishlist == "1"
            {
                let urlString = API_URL + "removefromwishlist"
                let params: NSDictionary = ["product_id":data["id"]!,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_RemoveWishList(url: urlString,params: params, sender: sender.tag)
            }
            else{
                let urlString = API_URL + "addtowishlist"
                let params: NSDictionary = ["product_id":data["id"]!,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_AddtoWishList(url: urlString,params: params, sender: sender.tag)
            }
        }
    }
}
extension BrandWiseitemVC
{
    func Webservice_BranditemList(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let StoreData = jsonResponse!["data"].dictionaryValue
                    if self.pageIndex == 1 {
                        self.lastIndex = Int(StoreData["last_page"]!.stringValue)!
                        self.BranditemListArray.removeAll()
                    }
                    let StoreListData = StoreData["data"]!.arrayValue
                    for data in StoreListData
                    {
                        let imageUrl = data["productimage"].dictionaryValue
                        let variation = data["variation"].dictionaryValue
                        let rattings = data["rattings"].arrayValue
                        var arg_rattings = String()
                        
                        if rattings.count == 0
                        {
                            arg_rattings = "0.0"
                        }
                        else{
                            let ProductRatting = formatterRatting.string(for: rattings[0]["avg_ratting"].stringValue.toDouble)
                            arg_rattings = ProductRatting!
                        }
                        var StoreListObj = [String:String]()
                        if variation.count != 0
                        {
                            StoreListObj = ["id":data["id"].stringValue,"product_name":data["product_name"].stringValue,"product_price":data["product_price"].stringValue,"discounted_price":data["discounted_price"].stringValue,"is_variation":data["is_variation"].stringValue,"is_wishlist":data["is_wishlist"].stringValue,"image_url":imageUrl["image_url"]!.stringValue,"variation_id":variation["id"]!.stringValue,"variation_price":variation["price"]!.stringValue,"discounted_variation_price":variation["discounted_variation_price"]!.stringValue,"variation":variation["variation"]!.stringValue,"variation_qty":variation["qty"]!.stringValue,"arg_ratting":arg_rattings]
                        }
                        else{
                            StoreListObj = ["id":data["id"].stringValue,"product_name":data["product_name"].stringValue,"product_price":data["product_price"].stringValue,"discounted_price":data["discounted_price"].stringValue,"is_variation":data["is_variation"].stringValue,"is_wishlist":data["is_wishlist"].stringValue,"image_url":imageUrl["image_url"]!.stringValue,"variation_id":"","variation_price":"","discounted_variation_price":"","variation":"","variation_qty":"","arg_ratting":"0.0"]
                            
                        }
                        self.BranditemListArray.append(StoreListObj)
                    }
                    self.vendordetails = jsonResponse!["vendordetails"].dictionaryValue
                    
                    self.Tableview_BranditemList.delegate = self
                    self.Tableview_BranditemList.dataSource = self
                    self.Tableview_BranditemList.reloadData()
                }
                else if responseCode == "0" {
                    self.Tableview_BranditemList.delegate = self
                    self.Tableview_BranditemList.dataSource = self
                    self.Tableview_BranditemList.reloadData()
                }
                else
                {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_AddtoWishList(url:String, params:NSDictionary,sender:Int) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    var data = self.BranditemListArray[sender]
                    data["is_wishlist"]! = "1"
                    self.BranditemListArray.remove(at: sender)
                    self.BranditemListArray.insert(data, at: sender)
                    self.Tableview_BranditemList.reloadData()
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_RemoveWishList(url:String, params:NSDictionary,sender:Int) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    
                    var data = self.BranditemListArray[sender]
                    data["is_wishlist"]! = "0"
                    self.BranditemListArray.remove(at: sender)
                    self.BranditemListArray.insert(data, at: sender)
                    self.Tableview_BranditemList.reloadData()
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
