//
//  SucessOrderVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 04/09/21.
//

import UIKit

class SucessOrderVC: UIViewController {

    @IBOutlet weak var btn_continue: UIButton!
    @IBOutlet weak var lblstr_title2: UILabel!
    @IBOutlet weak var lbl_title1: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btn_continue.setTitle("Continue Shopping!".localiz(), for: .normal)
        self.lbl_title1.text = "Order Sucess".localiz()
        self.lblstr_title2.text = "Your packet will be sent to your address, Thanks for order".localiz()
    }

    @IBAction func btnTap_ContinueShopping(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        objVC.modalPresentationStyle = .fullScreen
        self.present(objVC, animated: true)
    }
}
