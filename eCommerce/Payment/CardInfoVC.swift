//
//  CardInfoVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 04/09/21.
//

import UIKit
import CCValidator
import MBProgressHUD
import SwiftyJSON

protocol CardDataVCDelegate {
    func refreshCardData(card_number : String,card_exp_month:String,card_exp_year:String,card_cvc:String)
}

class CardInfoVC: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var img_card: UIImageView!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var txt_CardNo: UITextField!
    @IBOutlet weak var txt_CardHolderNameNo: UITextField!
    @IBOutlet weak var txt_CardMonth: UITextField!
    @IBOutlet weak var txt_CardYear: UITextField!
    @IBOutlet weak var txt_CardCVV: UITextField!
    @IBOutlet weak var lbl_cardinfo: UILabel!
    
    //MARK: Variables
    var delegate: CardDataVCDelegate!
    var cards = [AnyObject]()
    var cardIds = String()
    var CustomerId = String()
    var fingerprint = String()
    var Last4 = String()
    var exp_year = String()
    var exp_month = String()
    var brand = String()
    
    //MARK: Viewcontroller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lbl_cardinfo.text = "Card information".localiz()
        self.txt_CardNo.placeholder = "Card number".localiz()
        self.txt_CardHolderNameNo.placeholder = "Card holder name".localiz()
        self.btn_submit.setTitle("Submit".localiz(), for: .normal)
        cornerRadius(viewName: self.btn_submit, radius: 6.0)
        self.txt_CardNo.addTarget(self, action: #selector(didChangeText(textField:)), for: .editingChanged)
        self.txt_CardNo.delegate = self
        self.txt_CardMonth.delegate = self
        self.txt_CardYear.delegate = self
        self.txt_CardCVV.delegate = self
        self.txt_CardNo.becomeFirstResponder()
        if UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "en" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "N/A" {
            self.txt_CardNo.textAlignment = .left
            self.txt_CardCVV.textAlignment = .left
            self.txt_CardYear.textAlignment = .left
            self.txt_CardMonth.textAlignment = .left
            self.txt_CardHolderNameNo.textAlignment = .left
        }
        else {
            self.txt_CardNo.textAlignment = .right
            self.txt_CardCVV.textAlignment = .right
            self.txt_CardYear.textAlignment = .right
            self.txt_CardMonth.textAlignment = .right
            self.txt_CardHolderNameNo.textAlignment = .right
        }
    }
}

//MARK: Actions
extension CardInfoVC {
    @IBAction func btn_Close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnTap_Submit(_ sender: UIButton) {
        if isValidated() {
            self.dismiss(animated: false) {
                self.delegate.refreshCardData(card_number: self.txt_CardNo.text!, card_exp_month: self.txt_CardMonth.text!, card_exp_year: self.txt_CardYear.text!, card_cvc: self.txt_CardCVV.text!)
            }
        }
    }
    
    @objc func didChangeText(textField:UITextField) {
        textField.text = self.modifyCreditCardString(creditCardString: textField.text!)
    }
}

//MARK: Functions
extension CardInfoVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txt_CardNo {
            let cardType = CCValidator.typeCheckingPrefixOnly(creditCardNumber: self.txt_CardNo.text!)
            switch cardType {
            case .AmericanExpress:
                self.img_card.image = #imageLiteral(resourceName: "Amex")
            case .Dankort:
                self.img_card.image = #imageLiteral(resourceName: "Dankort")
            case .DinersClub:
                self.img_card.image = #imageLiteral(resourceName: "Diners Club")
            case .Discover:
                self.img_card.image = #imageLiteral(resourceName: "Discover")
            case .JCB:
                self.img_card.image = #imageLiteral(resourceName: "JCB")
            case .Maestro:
                self.img_card.image = #imageLiteral(resourceName: "Maestro")
            case .MasterCard:
                self.img_card.image = #imageLiteral(resourceName: "Mastercard")
            case .UnionPay:
                self.img_card.image = #imageLiteral(resourceName: "UnionPay")
            case .VisaElectron:
                self.img_card.image = #imageLiteral(resourceName: "VisaElectron")
            case .Visa:
                self.img_card.image = #imageLiteral(resourceName: "Visa")
            case .NotRecognized:
                self.img_card.image = #imageLiteral(resourceName: "demo")
            default:
                self.img_card.image = #imageLiteral(resourceName: "demo")
            }
            if string == "" {
                self.img_card.image = #imageLiteral(resourceName: "demo")
                return true
            }
            if range.length > 0 {
                return true
            }
            if string == "-" {
                return false
            }
            if range.location >= 19 {
                return false
            }
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: "-", with: "")
            let digits = NSCharacterSet.decimalDigits
            for char in replacementText.unicodeScalars {
                if !(digits as NSCharacterSet).longCharacterIsMember(char.value) {
                    return false
                }
            }
            if (originalText?.charactersArray.count)! > 0 {
                if (originalText?.charactersArray.count)! < 5 && (originalText?.charactersArray.count)! % 4 == 0{
                    originalText?.append("-")
                }else if(((originalText?.charactersArray.count)! + 1) % 5 == 0){
                    originalText?.append("-")
                }
            }
            textField.text = originalText
        }
        return true
    }
    
    func modifyCreditCardString(creditCardString : String) -> String {
        let trimmedString = creditCardString.components(separatedBy: .whitespaces).joined()
        let arrOfCharacters = Array(trimmedString)
        var modifiedCreditCardString = ""
        if(arrOfCharacters.count > 0) {
            for i in 0...arrOfCharacters.count-1 {
                modifiedCreditCardString.append(arrOfCharacters[i])
                if((i+1) % 4 == 0 && i+1 != arrOfCharacters.count) {
                    modifiedCreditCardString.append("")
                }
            }
        }
        return modifiedCreditCardString
    }
    
    func isValidated() -> Bool {
        if ((self.txt_CardNo.text == nil || self.txt_CardNo.text?.count == 0) ||
                (self.txt_CardHolderNameNo.text == nil || self.txt_CardHolderNameNo.text?.count == 0) ||
                (self.txt_CardYear.text == nil || self.txt_CardYear.text?.count == 0) ||
                (self.txt_CardMonth.text == nil || self.txt_CardMonth.text?.count == 0) ||
                (self.txt_CardCVV.text == nil || self.txt_CardCVV.text?.count == 0)) {
            let alert = UIAlertController(title: "", message: "Please enter all details".localiz(), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Okay".localiz(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        return true
    }
}
