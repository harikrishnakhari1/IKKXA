//
//  PaymentVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 13/08/21.
//

import UIKit
import Razorpay
import SwiftyJSON
import RaveSDK
class PaymentCell: UITableViewCell {
    
    @IBOutlet weak var img_Checkmark: UIImageView!
    @IBOutlet weak var lbl_PaymentName: UILabel!
    @IBOutlet weak var img_Payment: UIImageView!
    @IBOutlet weak var cell_view: CornerView!
    
}
class PaymentVC: UIViewController {
    @IBOutlet weak var TableView_PaymentList: UITableView!
    var razorpay : RazorpayCheckout!
    var Address_data = JSON()
    var Order_Total = String()
    var Order_note = String()
    var PaymentListArray = [[String:String]]()
    var paymentType = String()
    var Totalamount = Double()
    var coupon_name = String()
    var discount_amount = String()
    var walletAmount = String()
    var walletPrice = String()
    var Flutterwave_public_key = String()
    var Flutterwave_encryption_key = String()
    var Paystack_key = String()
    var environment = String()
    var vendor_id = String()
    
    @IBOutlet weak var lbl_title: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Totalamount)
        self.lbl_title.text = "Payment Method".localiz()
        let urlString = API_URL + "paymentlist"
        let params: NSDictionary = [
            "user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
        self.Webservice_PaymentList(url: urlString, params: params)
        print (urlString)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    @IBAction func btnTap_PayNow(_ sender: UIButton) {
        
       // payment_type = COD : 1, Wallet : 2, RazorPay : 3, Stripe : 4, Flutterwave : 5
        
        if self.paymentType == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please select payment method!".localiz())
        }
        else{
            if self.paymentType == "1" {
                let urlString = API_URL + "order"
                let params: NSDictionary = [
                    "user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                    "order_notes":self.Order_note,
                    "payment_type":self.paymentType,
                    "full_name":"\(Address_data["first_name"].stringValue) \(Address_data["last_name"].stringValue)",
                    "email":Address_data["email"].stringValue,
                    "mobile":Address_data["mobile"].stringValue,
                    "landmark":Address_data["landmark"].stringValue,
                    "street_address":Address_data["street_address"].stringValue,
                    "pincode":Address_data["pincode"].stringValue,
                    "grand_total":self.Order_Total,
                    "coupon_name":self.coupon_name,
                    "discount_amount":self.discount_amount,
                    "vendor_id":self.vendor_id]
                self.Webservice_PlaceOrder(url: urlString, params: params)
            }
           /* else if self.paymentType == "2" {
                
                if Double(walletAmount.replacingOccurrences(of: ",", with: ""))! < Double(self.Order_Total)! {
                    showAlertMessage(titleStr: "", messageStr: "You don't have sufficient wallet amonut. Please select another payment method".localiz())
                }
                else {
                    let urlString = API_URL + "order"
                    let params: NSDictionary = [
                        "user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                        "order_notes":self.Order_note,
                        "payment_type":self.paymentType,
                        "full_name":"\(Address_data["first_name"].stringValue) \(Address_data["last_name"].stringValue)",
                        "email":Address_data["email"].stringValue,
                        "mobile":Address_data["mobile"].stringValue,
                        "landmark":Address_data["landmark"].stringValue,
                        "street_address":Address_data["street_address"].stringValue,
                        "pincode":Address_data["pincode"].stringValue,
                        "grand_total":self.Order_Total,
                        "coupon_name":self.coupon_name,
                        "discount_amount":self.discount_amount,
                        "vendor_id":self.vendor_id]
                    self.Webservice_PlaceOrder(url: urlString, params: params)
                }
            }
            else if self.paymentType == "3" {
                self.showPaymentForm()
            }
            else if self.paymentType == "4" {
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "CardInfoVC") as! CardInfoVC
                VC.modalPresentationStyle = .overFullScreen
                VC.modalTransitionStyle = .crossDissolve
                VC.delegate = self
                self.present(VC,animated: true,completion: nil)
            }
            else if self.paymentType == "5" {
                self.Flutterwave()
            }
            else if self.paymentType == "6" {
                self.Paystack()
            }
            
            */
            else if self.paymentType == "7" {
                let urlString = API_URL + "order"
                let params: NSDictionary = [
                    "user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                    "order_notes":self.Order_note,
                    "payment_type":self.paymentType,
                    "full_name":"\(Address_data["first_name"].stringValue) \(Address_data["last_name"].stringValue)",
                    "email":Address_data["email"].stringValue,
                    "mobile":Address_data["mobile"].stringValue,
                    "landmark":Address_data["landmark"].stringValue,
                    "street_address":Address_data["street_address"].stringValue,
                    "pincode":Address_data["pincode"].stringValue,
                    "grand_total":self.Order_Total,
                    "coupon_name":self.coupon_name,
                    "discount_amount":self.discount_amount,
                    "vendor_id":self.vendor_id]
                
                        if let vc = storyboard?.instantiateViewController(withIdentifier: "WebViewVC")as? WebViewVC {
                            vc.iswebview = "wepayment"
                            vc.grandTotal = self.Order_Total
                            vc.callBack = { (name: String) in
                                print(name)
                                if(name == "Paid"){
                                    self.Webservice_PlaceOrder(url: urlString, params: params)
                                }else{
                                    print(name)
                                }
                            }
                            vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: false)
                        }
                    
        }
        
        }
    }
}
//MARK: Functions
extension PaymentVC: CardDataVCDelegate,RazorpayPaymentCompletionProtocol,RazorpayProtocol {
    
    func showPaymentForm(){
        let options: [String:Any] = [
            "amount" : "\(Double(Order_Total)! * 100)", //mandatory in paise like:- 1000 paise ==  10 rs
            "currency": "INR",
            "description": "Order payment",
            "image": "",
            "name": "Ecommerce user",
            "prefill": [
                "contact": UserDefaultManager.getStringFromUserDefaults(key: UD_userMobile),
                "email": UserDefaultManager.getStringFromUserDefaults(key: UD_userEmail)
            ],
            "theme": [
                "color": "#2291FF"
            ]
        ]
        razorpay?.open(options)
    }
    
    func onPaymentError(_ code: Int32, description str: String) {
        let alertController = UIAlertController(title: "", message: str, preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: "Okay".localiz(), style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        let urlString = API_URL + "order"
        let params: NSDictionary = [
            "payment_id":"\(payment_id)",
            "user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
            "order_notes":self.Order_note,
            "payment_type":self.paymentType,
            "full_name":"\(Address_data["first_name"].stringValue) \(Address_data["last_name"].stringValue)",
            "email":Address_data["email"].stringValue,
            "mobile":Address_data["mobile"].stringValue,
            "landmark":Address_data["landmark"].stringValue,
            "street_address":Address_data["street_address"].stringValue,
            "pincode":Address_data["pincode"].stringValue,
            "grand_total":self.Order_Total,
            "coupon_name":self.coupon_name,
            "discount_amount":self.discount_amount,
            "vendor_id":self.vendor_id]
        self.Webservice_PlaceOrder(url: urlString, params: params)
    }
    
    func refreshCardData(card_number: String, card_exp_month: String, card_exp_year: String, card_cvc: String) {
        let urlString = API_URL + "order"
        let params: NSDictionary = [
            "user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
            "order_notes":self.Order_note,
            "payment_type":self.paymentType,
            "full_name":"\(Address_data["first_name"].stringValue) \(Address_data["last_name"].stringValue)",
            "email":Address_data["email"].stringValue,
            "mobile":Address_data["mobile"].stringValue,
            "landmark":Address_data["landmark"].stringValue,
            "street_address":Address_data["street_address"].stringValue,
            "pincode":Address_data["pincode"].stringValue,
            "grand_total":self.Order_Total,
            "coupon_name":self.coupon_name,
            "discount_amount":self.discount_amount,
            "vendor_id":self.vendor_id,
            "card_number":card_number,
            "card_exp_month":card_exp_month,
            "card_exp_year":card_exp_year,
            "card_cvc":card_cvc
        ]
        self.Webservice_PlaceOrder(url: urlString, params: params)
    }
}
extension PaymentVC : RavePayProtocol
{
    func Flutterwave() {
        let config = RaveConfig.sharedConfig()
        config.country = "US" // Country Code
        config.currencyCode = "USD" // Currency
        config.email = UserDefaultManager.getStringFromUserDefaults(key: UD_userEmail)
        if self.paymentType == "5"
        {
            if self.environment == "1"
            {
                config.isStaging = false // Toggle this for staging and live environment
            }
            else{
                config.isStaging = true // Toggle this for staging and live environment
            }
        }
        config.phoneNumber = UserDefaultManager.getStringFromUserDefaults(key: UD_userMobile) //Phone number
        config.transcationRef = "ref_" // transaction ref
        config.firstName = UserDefaultManager.getStringFromUserDefaults(key: UD_userName) // first name
        config.lastName = "" //lastname
        config.meta = [["metaname":"sdk", "metavalue":"ios"]]
        config.publicKey = Flutterwave_public_key //Public key
        config.encryptionKey = self.Flutterwave_encryption_key //Encryption key
        let controller = NewRavePayViewController()
        let nav = UINavigationController(rootViewController: controller)
        controller.amount = self.Order_Total // Amount
        controller.delegate = self
        self.present(nav, animated: true)
    }
    func onDismiss() {
        print("Dismiss")
    }
    func tranasctionSuccessful(flwRef: String?, responseData: [String : Any]?) {
        
        print(responseData?.description ?? "Nothing here")
        let paymentid = responseData!["flwRef"] as! String
        print(paymentid)
        let urlString = API_URL + "order"
        let params: NSDictionary = [
            "payment_id":"\(paymentid)",
            "user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
            "order_notes":self.Order_note,
            "payment_type":self.paymentType,
            "full_name":"\(Address_data["first_name"].stringValue) \(Address_data["last_name"].stringValue)",
            "email":Address_data["email"].stringValue,
            "mobile":Address_data["mobile"].stringValue,
            "landmark":Address_data["landmark"].stringValue,
            "street_address":Address_data["street_address"].stringValue,
            "pincode":Address_data["pincode"].stringValue,
            "grand_total":self.Order_Total,
            "coupon_name":self.coupon_name,
            "discount_amount":self.discount_amount,
            "vendor_id":self.vendor_id]
        self.Webservice_PlaceOrder(url: urlString, params: params)

    }

    func tranasctionFailed(flwRef: String?, responseData: [String : Any]?) {
        print(responseData?.description ?? "Nothing here")
    }
    
    func Paystack() {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "PaystackCardInfoVC") as! PaystackCardInfoVC
        VC.modalPresentationStyle = .overFullScreen
        VC.modalTransitionStyle = .crossDissolve
        VC.delegate = self
        VC.paystackPublicKey = self.Paystack_key
        VC.TotalAmount = Int(Double(self.Order_Total)! * 100)
        self.present(VC,animated: true,completion: nil)
    }
}
//MARK: Tableview Methods
extension PaymentVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return PaymentListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TableView_PaymentList.dequeueReusableCell(withIdentifier: "PaymentCell") as! PaymentCell
        cornerRadius(viewName: cell.img_Payment, radius: 4.0)
        let data = self.PaymentListArray[indexPath.row]
        let environment = data["environment"]!
        setBorder(viewName: cell.cell_view, borderwidth: 0.4, borderColor: UIColor.lightGray.cgColor, cornerRadius: 8.0)
        if data["payment_name"]! == "Stripe"
        {
            cell.img_Payment.image = UIImage(named:"ic_stripe")
            cell.lbl_PaymentName.text = "\(data["payment_name"]!) Payment"
//            if environment == "1" {
//                STPPaymentConfiguration.shared.publishableKey = data["test_public_key"]!
//            }
//            else {
//                STPPaymentConfiguration.shared.publishableKey = data["live_public_key"]!
//            }
        }
        else if data["payment_name"]! == "RazorPay"
        {
            cell.img_Payment.image = UIImage(named:"ic_rezorpay")
            cell.lbl_PaymentName.text = "\(data["payment_name"]!) Payment"
            if environment == "1" {
                self.razorpay = RazorpayCheckout.initWithKey(data["test_public_key"]!, andDelegate: self)
            }
            else {
                self.razorpay = RazorpayCheckout.initWithKey(data["live_public_key"]!, andDelegate: self)
            }
        }
        
        // NOTE By SHOAIB :- COD to Cash on delivery
        else if data["payment_name"]! == "Cash on delivery"
        {
            cell.img_Payment.image = UIImage(named:"ic_cod")
            cell.lbl_PaymentName.text = "\(data["payment_name"]!)"
            
        }
        else if data["payment_name"]! == "Flutterwave"
        {
            cell.img_Payment.image = UIImage(named:"ic_Flutterwave")
            cell.lbl_PaymentName.text = "\(data["payment_name"]!) Payment"
            self.environment = environment
            if environment == "1" {
                self.Flutterwave_public_key = data["test_public_key"]!
                self.Flutterwave_encryption_key = data["encryption_key"]!
            }
            else {
                self.Flutterwave_public_key = data["test_public_key"]!
                self.Flutterwave_encryption_key = data["encryption_key"]!
            }
            
        }
        else if data["payment_name"]! == "Wallet"
        {
            cell.img_Payment.image = UIImage(named:"ic_wallet")
            cell.lbl_PaymentName.text = "\(data["payment_name"]!) Payment (\(self.walletPrice))"
            
        }
        else if data["payment_name"]! == "Paystack"
        {
            cell.img_Payment.image = UIImage(named:"ic_Paystack")
            cell.lbl_PaymentName.text = "\(data["payment_name"]!) Payment"
            if environment == "1" {
                self.Paystack_key = data["test_public_key"]!
            }
            else {
                self.Paystack_key = data["live_public_key"]!
            }
        }
        else if data["payment_name"]! == "Credit and Debit"
        {
            cell.img_Payment.image = UIImage(named:"ic_cardpayment")
            cell.lbl_PaymentName.text = "\(data["payment_name"]!)"
        
        }
        if data["isselect"]! == "1" {
            cell.img_Checkmark.image = UIImage(named: "ic_checkmark")
            cell.img_Checkmark.isHidden = false
        }
        else {
            cell.img_Checkmark.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<self.PaymentListArray.count {
            var payment = self.PaymentListArray[i]
            payment["isselect"] = "0"
            self.PaymentListArray.remove(at: i)
            self.PaymentListArray.insert(payment, at: i)
        }
        
        var data = self.PaymentListArray[indexPath.row]
       // payment_type = COD : 1, Wallet : 2, RazorPay : 3, Stripe : 4, Flutterwave : 5
        // COD to Cash on delivery
        if data["payment_name"]! == "Cash on delivery" {
            self.paymentType = "1"
        }
        if data["payment_name"]! == "Wallet" {
            self.paymentType = "2"
        }
        else if data["payment_name"]! == "RazorPay" {
            self.paymentType = "3"
        }
        else if data["payment_name"]! == "Stripe" {
            self.paymentType = "4"
        }
        else if data["payment_name"]! == "Flutterwave" {
            self.paymentType = "5"
        }
        else if data["payment_name"]! == "Paystack" {
            self.paymentType = "6"
        }
        else if data["payment_name"]! == "Credit and Debit" {
            self.paymentType = "7"
        }
        data["isselect"]! = "1"
        self.PaymentListArray.remove(at: indexPath.row)
        self.PaymentListArray.insert(data, at: indexPath.row)
        
        self.TableView_PaymentList.reloadData()
        
    }
}
extension PaymentVC
{
    func Webservice_PaymentList(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let responseData = jsonResponse!["paymentlist"].arrayValue
                    
                    let Price = formatter.string(for: jsonResponse!["walletamount"].stringValue.toDouble)!
                    self.walletAmount = Price
                    if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                    {
                        self.walletPrice = "\(Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"

                    }
                    else {
                        
                        self.walletPrice = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Price)"
                    }
                    for data in responseData {
                        let productObj = ["payment_name":data["payment_name"].stringValue,"test_public_key":data["test_public_key"].stringValue,"live_public_key":data["live_public_key"].stringValue,"environment":data["environment"].stringValue,"isselect":"0","encryption_key":data["encryption_key"].stringValue]
                        self.PaymentListArray.append(productObj)
                    }
                    
                    self.TableView_PaymentList.delegate = self
                    self.TableView_PaymentList.dataSource = self
                    self.TableView_PaymentList.reloadData()
                    
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    
    func Webservice_PlaceOrder(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let vc = self.storyboard?.instantiateViewController(identifier: "SucessOrderVC") as! SucessOrderVC
                    vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
extension PaymentVC : PaystackCardDataVCDelegate
{
    func refreshPaystackCardData(TokenID: String) {
        let urlString = API_URL + "order"
        let params: NSDictionary = [
            "payment_id":"\(TokenID)",
            "user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
            "order_notes":self.Order_note,
            "payment_type":self.paymentType,
            "full_name":"\(Address_data["first_name"].stringValue) \(Address_data["last_name"].stringValue)",
            "email":Address_data["email"].stringValue,
            "mobile":Address_data["mobile"].stringValue,
            "landmark":Address_data["landmark"].stringValue,
            "street_address":Address_data["street_address"].stringValue,
            "pincode":Address_data["pincode"].stringValue,
            "grand_total":self.Order_Total,
            "coupon_name":self.coupon_name,
            "discount_amount":self.discount_amount,
            "vendor_id":self.vendor_id]
        self.Webservice_PlaceOrder(url: urlString, params: params)
    }
    
}
