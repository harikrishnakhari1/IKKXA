//
//  PaystackCardInfoVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 22/10/21.
//

import UIKit
import Paystack
protocol PaystackCardDataVCDelegate {
    
    func refreshPaystackCardData(TokenID : String)
    
}

class PaystackCardInfoVC: UIViewController,PSTCKPaymentCardTextFieldDelegate {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var cardDetailsForm: PSTCKPaymentCardTextField!
    var paystackPublicKey = String()
    var TotalAmount = Int()
    var delegate: PaystackCardDataVCDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "Card information".localiz()
        self.btn_submit.setTitle("Submit".localiz(), for: .normal)
                btn_submit.isEnabled = false
                cardDetailsForm.delegate = self
                cornerRadius(viewName: self.btn_submit, radius: 4.0)
    }
    func dismissKeyboardIfAny(){
        // Dismiss Keyboard if any
        cardDetailsForm.resignFirstResponder()
        
    }
    
    @IBAction func cardDetailsChanged(_ sender: PSTCKPaymentCardTextField) {
        btn_submit.isEnabled = sender.isValid
    }

    @IBAction func btnTap_Close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnTap_Submit(_ sender: UIButton) {
        dismissKeyboardIfAny()
        
        // Make sure public key has been set
        if (paystackPublicKey == "" || !paystackPublicKey.hasPrefix("pk_")) {
            showAlertMessage(titleStr: "", messageStr: "You can find your public key at https://dashboard.paystack.co/#/settings/developer .")
            // You need to set your Paystack public key.
            return
        }
        
        Paystack.setDefaultPublicKey(paystackPublicKey)
        
        if cardDetailsForm.isValid {

//            if backendURLString != "" {
//                fetchAccessCodeAndChargeCard()
//                return
//            }
//            showOkayableMessage("Backend not configured", message:"To run this sample, please configure your backend.")
            chargeWithSDK(newCode:"");

        }
    }

    func chargeWithSDK(newCode: NSString){
        let transactionParams = PSTCKTransactionParams.init();
//        transactionParams.access_code = newCode as String;
        //transactionParams.additionalAPIParameters = ["enforce_otp": "true"];
        transactionParams.email = UserDefaultManager.getStringFromUserDefaults(key: UD_userEmail)
        print(TotalAmount)
        transactionParams.amount = UInt(TotalAmount)
        transactionParams.currency = "GHS"
//        let dictParams: NSMutableDictionary = [
//            "recurring": true
//        ];
//        let arrParams: NSMutableArray = [
//            "0","go"
//        ];
//        do {
//            try transactionParams.setMetadataValueDict(dictParams, forKey: "custom_filters");
//            try transactionParams.setMetadataValueArray(arrParams, forKey: "custom_array");
//        } catch {
//            print(error)
//        }
        // use library to create charge and get its reference
        PSTCKAPIClient.shared().chargeCard(self.cardDetailsForm.cardParams, forTransaction: transactionParams, on: self, didEndWithError: { (error, reference) in
//            self.outputOnLabel(str: "Charge errored")
            showAlertMessage(titleStr: "", messageStr: "Charge errored")
            // what should I do if an error occured?
            print(error)
            if error._code == PSTCKErrorCode.PSTCKExpiredAccessCodeError.rawValue{
                // access code could not be used
                // we may as well try afresh
            }
            if error._code == PSTCKErrorCode.PSTCKConflictError.rawValue{
                // another transaction is currently being
                // processed by the SDK... please wait
            }
            if let errorDict = (error._userInfo as! NSDictionary?){
                if let errorString = errorDict.value(forKeyPath: "com.paystack.lib:ErrorMessageKey") as! String? {
                    if let reference=reference {
//                        self.showOkayableMessage("An error occured while completing "+reference, message: errorString)
                        showAlertMessage(titleStr: "", messageStr: errorString)
                        
//                        self.outputOnLabel(str: reference + ": " + errorString)
                        showAlertMessage(titleStr: "", messageStr: errorString)
//                        self.verifyTransaction(reference: reference)
                    } else {
//                        self.showOkayableMessage("An error occured", message: errorString)
//                        self.outputOnLabel(str: errorString)
                        showAlertMessage(titleStr: "", messageStr: errorString)
                    }
                }
            }
            self.btn_submit.isEnabled = true;
        }, didRequestValidation: { (reference) in
            showAlertMessage(titleStr: "", messageStr: "requested validation: " + reference)
        }, willPresentDialog: {
            // make sure dialog can show
            // if using a "processing" dialog, please hide it
            showAlertMessage(titleStr: "", messageStr: "will show a dialog")
        }, dismissedDialog: {
            // if using a processing dialog, please make it visible again
            showAlertMessage(titleStr: "", messageStr: "dismissed dialog")
        }) { (reference) in
//            self.outputOnLabel(str: "succeeded: " + reference)
//            showAlertMessage(titleStr: "", messageStr: "succeeded: " + reference)
            self.btn_submit.isEnabled = true;
            
            self.dismiss(animated: false) {
                self.delegate.refreshPaystackCardData(TokenID: reference)
            }
//            self.verifyTransaction(reference: reference)
        }
        return
    }

}
