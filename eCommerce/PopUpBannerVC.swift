//
//  PopUpBannerVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 25/06/21.
//

import UIKit
import SDWebImage

class PopUpBannerVC: UIViewController {
    @IBOutlet weak var img_Popup: UIImageView!
    var imgurl = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(imgurl)
        self.img_Popup.sd_imageTransition = .fade
        self.img_Popup.sd_setImage(with: URL(string: self.imgurl), placeholderImage: UIImage(named: "b_00"))
    }
    
    
   
}
extension PopUpBannerVC
{
    @IBAction func btnTap_Dismis(_ sender: UIButton) {
        UserDefaultManager.setStringToUserDefaults(value: "1", key: UD_DismisHomeBanner)
        self.dismiss(animated: true, completion: nil)
    }
}

