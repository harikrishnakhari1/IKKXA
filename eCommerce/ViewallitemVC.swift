//
//  ViewallitemVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 03/08/21.
//

import UIKit
import SDWebImage
import SwiftyJSON

class ViewallitemVC: UIViewController {
    
    @IBOutlet weak var CollectionView_ViewallList: UICollectionView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var btn_Filter: UIButton!
    var pageIndex = 1
    var lastIndex = 0
    var ViewAllListArray = [[String:String]]()
    var istype = String()
    var innersubcategory_id = String()
    var navtitle = String()
    var isSelectFilter = String()
    var products = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(istype)
        self.products = self.istype
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.pageIndex = 1
        self.lastIndex = 0
        if self.innersubcategory_id != ""
        {
            self.lbl_title.text = istype
            self.btn_Filter.isHidden = true
            let urlString = API_URL + "products?page=\(self.pageIndex)"
            let params: NSDictionary = ["innersubcategory_id":self.innersubcategory_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
            self.Webservice_ViewAllitemList(url: urlString,params: params)
        }
        else{
            self.lbl_title.text = navtitle
            self.btn_Filter.isHidden = false
            let urlString = API_URL + "viewalllisting?page=\(self.pageIndex)"
            let params: NSDictionary = ["type":self.istype,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
            self.Webservice_ViewAllitemList(url: urlString,params: params)
        }
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    @IBAction func btnTap_Filter(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.deleget = self
        self.present(vc,animated: true,completion: nil)
    }
    
}
extension ViewallitemVC : SelectFilterDeleget
{
    func FilterText(Selectedtext: String) {
        print(self.products)
        self.isSelectFilter = "1"
        self.istype = Selectedtext
        self.pageIndex = 1
        self.lastIndex = 0
        let urlString = API_URL + "filter?page=\(self.pageIndex)"
        let params: NSDictionary = ["type":Selectedtext,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"product":self.products]
        self.Webservice_ViewAllitemList(url: urlString,params: params)
    }
    
    
}
extension ViewallitemVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)
        
        imagedata.contentMode = .scaleAspectFit
        self.CollectionView_ViewallList.backgroundView = imagedata
        if self.ViewAllListArray.count == 0 {
            imagedata.image = UIImage(named: "ic_Nodata")
        }
        else {
            imagedata.image = UIImage(named: "")
        }
        return ViewAllListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.CollectionView_ViewallList.dequeueReusableCell(withReuseIdentifier: "MostpopularCell", for: indexPath) as! MostpopularCell
        
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.cornerRadius = 8.0
        cell.layer.borderWidth = 0.4
        let data = self.ViewAllListArray[indexPath.item]
        let is_variation = data["is_variation"]!
        let is_wishlist = data["is_wishlist"]!
        cell.lbl_itemName.text = data["product_name"]!
        cell.img_item.sd_imageTransition = .fade
        let items = indexPath.item % 6
        cell.img_item.sd_setImage(with: URL(string: data["image_url"]!), placeholderImage: UIImage(named: "b_0\(items)"))
        
        cell.lbl_ratingcount.text = data["arg_ratting"]!
        if is_wishlist == "1"
        {
            cell.btn_favorite.setImage(UIImage(named: "ic_heartfill"), for: .normal)
            cell.btn_favorite.tintColor = UIColor.red
            
        }
        else{
            cell.btn_favorite.setImage(UIImage(named: "ic_heart"), for: .normal)
            cell.btn_favorite.tintColor = UIColor.darkGray
        }
        if is_variation == "1"
        {
            let ProductPrice = formatter.string(for: data["variation_price"]!.toDouble)
            let ProductDiscountPrice = formatter.string(for: data["discounted_variation_price"]!.toDouble)
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                cell.lbl_itemprice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                cell.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
            }
            else {
                cell.lbl_itemprice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                cell.lbl_DiscountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
            }
            
        }
        else{
            let ProductPrice = formatter.string(for: data["product_price"]!.toDouble)
            let ProductDiscountPrice = formatter.string(for: data["discounted_price"]!.toDouble)
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                cell.lbl_itemprice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                cell.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
            }
            else{
                cell.lbl_itemprice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                cell.lbl_DiscountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
            }
            
        }
        cell.btn_favorite.tag = indexPath.row
        cell.btn_favorite.addTarget(self, action:#selector(btnTap_Favorites), for: .touchUpInside)
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (UIScreen.main.bounds.width - 30.0) / 2, height: ((UIScreen.main.bounds.width - 30.0) / 2) * 1.90)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = self.ViewAllListArray[indexPath.item]
        let vc = storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
        vc.item_id = data["id"]!
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == self.ViewAllListArray.count - 1 {
            if self.pageIndex != self.lastIndex {
                self.pageIndex = self.pageIndex + 1
                if self.ViewAllListArray.count != 0 {
                    
                    
                    if self.isSelectFilter == "1"
                    {
                        let urlString = API_URL + "filter?page=\(self.pageIndex)"
                        let params: NSDictionary = ["type":self.istype,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"product":self.istype]
                        self.Webservice_ViewAllitemList(url: urlString,params: params)
                    }
                    else if self.innersubcategory_id != ""
                     {
                         self.lbl_title.text = istype
                         self.btn_Filter.isHidden = true
                         let urlString = API_URL + "products?page=\(self.pageIndex)"
                         let params: NSDictionary = ["innersubcategory_id":self.innersubcategory_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                         self.Webservice_ViewAllitemList(url: urlString,params: params)
                         print(urlString)
                     }
                    else
                    {
                        let urlString = API_URL + "viewalllisting?page=\(self.pageIndex)"
                        let params: NSDictionary = ["type":self.istype,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                        self.Webservice_ViewAllitemList(url: urlString,params: params)
                    }
                    
                }
            }
        }
    }
    @objc func btnTap_Favorites(sender:UIButton!) {
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A"
        {
            UserDefaults.standard.set("", forKey:UD_userId)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            UIApplication.shared.windows[0].rootViewController = nav
        }
        else{
            let data = self.ViewAllListArray[sender.tag]
            let is_wishlist = data["is_wishlist"]!
            if is_wishlist == "1"
            {
                let urlString = API_URL + "removefromwishlist"
                let params: NSDictionary = ["product_id":data["id"]!,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_RemoveWishList(url: urlString,params: params, sender: sender.tag)
            }
            else{
                let urlString = API_URL + "addtowishlist"
                let params: NSDictionary = ["product_id":data["id"]!,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_AddtoWishList(url: urlString,params: params, sender: sender.tag)
            }
        }
        
        
    }
    
}
extension ViewallitemVC
{
    func Webservice_ViewAllitemList(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    
                    let ResponseData = jsonResponse!["data"].dictionaryValue
                    
                    if self.pageIndex == 1 {
                        self.lastIndex = Int(ResponseData["last_page"]!.stringValue)!
                        self.ViewAllListArray.removeAll()
                    }
                    let ListData = ResponseData["data"]!.arrayValue
                    for data in ListData
                    {
                        let imageUrl = data["productimage"].dictionaryValue
                        let variation = data["variation"].dictionaryValue
                        let rattings = data["rattings"].arrayValue
                        var arg_rattings = String()
                        
                        if rattings.count == 0
                        {
                            arg_rattings = "0.0"
                        }
                        else{
                            let ProductRatting = formatterRatting.string(for: rattings[0]["avg_ratting"].stringValue.toDouble)
                            arg_rattings = ProductRatting!
                        }
                        var StoreListObj = [String:String]()
                        if variation.count != 0
                        {
                            StoreListObj = ["id":data["id"].stringValue,"product_name":data["product_name"].stringValue,"product_price":data["product_price"].stringValue,"discounted_price":data["discounted_price"].stringValue,"is_variation":data["is_variation"].stringValue,"is_wishlist":data["is_wishlist"].stringValue,"image_url":imageUrl["image_url"]!.stringValue,"variation_id":variation["id"]!.stringValue,"variation_price":variation["price"]!.stringValue,"discounted_variation_price":variation["discounted_variation_price"]!.stringValue,"variation":variation["variation"]!.stringValue,"variation_qty":variation["qty"]!.stringValue,"arg_ratting":arg_rattings]
                        }
                        else{
                            StoreListObj = ["id":data["id"].stringValue,"product_name":data["product_name"].stringValue,"product_price":data["product_price"].stringValue,"discounted_price":data["discounted_price"].stringValue,"is_variation":data["is_variation"].stringValue,"is_wishlist":data["is_wishlist"].stringValue,"image_url":imageUrl["image_url"]!.stringValue,"variation_id":"","variation_price":"","discounted_variation_price":"","variation":"","variation_qty":"","arg_ratting":"0.0"]
                            
                        }
                        self.ViewAllListArray.append(StoreListObj)
                    }
                    //                    self.vendordetails = jsonResponse!["vendordetails"].dictionaryValue
                    
                    self.CollectionView_ViewallList.delegate = self
                    self.CollectionView_ViewallList.dataSource = self
                    self.CollectionView_ViewallList.reloadData()
                }
                else if responseCode == "0" {
                    self.CollectionView_ViewallList.delegate = self
                    self.CollectionView_ViewallList.dataSource = self
                    self.CollectionView_ViewallList.reloadData()
                }
                else
                {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_AddtoWishList(url:String, params:NSDictionary,sender:Int) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    var data = self.ViewAllListArray[sender]
                    data["is_wishlist"]! = "1"
                    self.ViewAllListArray.remove(at: sender)
                    self.ViewAllListArray.insert(data, at: sender)
                    self.CollectionView_ViewallList.reloadData()
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_RemoveWishList(url:String, params:NSDictionary,sender:Int) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    
                    var data = self.ViewAllListArray[sender]
                    data["is_wishlist"]! = "0"
                    self.ViewAllListArray.remove(at: sender)
                    self.ViewAllListArray.insert(data, at: sender)
                    self.CollectionView_ViewallList.reloadData()
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    
}
