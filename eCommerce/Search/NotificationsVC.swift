//
//  NotificationsVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 04/09/21.
//

import UIKit
import SwiftyJSON
import SDWebImage

class Notificationcell: UITableViewCell {
    
    @IBOutlet weak var Cell_view: UIView!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_NotificaionDesc: UILabel!
    @IBOutlet weak var lbl_NotificaionStatus: UILabel!
    @IBOutlet weak var img_Notification: UIImageView!
}
class NotificationsVC: UIViewController {
    
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var TableView_NotificationListing: UITableView!
    var pageIndex = 1
    var lastIndex = 0
    var NotificationListArray = [[String:String]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "Notifications".localiz()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.pageIndex = 1
        self.lastIndex = 0
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
            appNavigation.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.windows[0].rootViewController = appNavigation
        }
        else {
            let urlString = API_URL + "notification"
            let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
            self.Webservice_Notifications(url: urlString, params:params)
        }
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
}
//MARK: Tableview methods
extension NotificationsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)
        
        imagedata.contentMode = .scaleAspectFit
        self.TableView_NotificationListing.backgroundView = imagedata
        if self.NotificationListArray.count == 0 {
            imagedata.image = UIImage(named: "ic_Pushnotifications-rafiki")
        }
        else {
            imagedata.image = UIImage(named: "")
        }
        return NotificationListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TableView_NotificationListing.dequeueReusableCell(withIdentifier: "Notificationcell") as! Notificationcell
        let data = self.NotificationListArray[indexPath.row]
        cornerRadius(viewName: cell.Cell_view, radius: cell.Cell_view.frame.height / 2)
        
        if data["order_status"]! == "1"
        {
            cell.lbl_NotificaionStatus.text = "Order Placed"
            cell.Cell_view.backgroundColor = UIColor.systemYellow
            cell.img_Notification.image = UIImage.init(named: "ic_package")
        }
        else if data["order_status"]! == "2"
        {
            cell.lbl_NotificaionStatus.text = "Order Confirmed"
            cell.Cell_view.backgroundColor = UIColor.systemBlue
            cell.img_Notification.image = UIImage.init(named: "ic_package_Confirm")
        }
        else if data["order_status"]! == "3"
        {
            cell.lbl_NotificaionStatus.text = "Order Shipped"
            cell.Cell_view.backgroundColor = UIColor.systemTeal
            cell.img_Notification.image = UIImage.init(named: "ic_delivery")
        }
        else if data["order_status"]! == "4"
        {
            cell.lbl_NotificaionStatus.text = "Order Delivered"
            cell.Cell_view.backgroundColor = UIColor.systemGreen
            cell.img_Notification.image = UIImage.init(named: "ic_delivered_order")
        }
        else if data["order_status"]! == "5"
        {
            cell.lbl_NotificaionStatus.text = "Order Cancelled"
            cell.Cell_view.backgroundColor = UIColor.systemRed
            cell.img_Notification.image = UIImage.init(named: "ic_package_Cancel")
        }
        else if data["order_status"]! == "6"
        {
            cell.lbl_NotificaionStatus.text = "Order Cancelled"
            cell.Cell_view.backgroundColor = UIColor.systemRed
            cell.img_Notification.image = UIImage.init(named: "ic_package_Cancel")
        }
        else if data["order_status"]! == "7"
        {
            cell.lbl_NotificaionStatus.text = "Return Request Created"
            cell.Cell_view.backgroundColor = UIColor.systemOrange
            cell.img_Notification.image = UIImage.init(named: "ic_returning")
        }
        else if data["order_status"]! == "8"
        {
            cell.lbl_NotificaionStatus.text = "Order Return Accepted"
            cell.Cell_view.backgroundColor = UIColor.systemGreen
            cell.img_Notification.image = UIImage.init(named: "ic_delivered_order")
        }
        else if data["order_status"]! == "9"
        {
            cell.lbl_NotificaionStatus.text = "Order Return Completed"
            cell.Cell_view.backgroundColor = UIColor.systemTeal
            cell.img_Notification.image = UIImage.init(named: "ic_delivery")
        }
        else if data["order_status"]! == "10"
        {
            cell.lbl_NotificaionStatus.text = "Order Return Rejected"
            cell.Cell_view.backgroundColor = UIColor.systemRed
            cell.img_Notification.image = UIImage.init(named: "ic_package_Cancel")
        }
        
        cell.lbl_NotificaionDesc.text = data["message"]!
        let setdate = DateFormater.getBirthDateStringFromDateString(givenDate:data["date"]!)
        cell.lbl_date.text = setdate
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let urlString = API_URL + "notificationread"
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
        self.Webservice_Notifications_Read(url: urlString, params:params, sender:indexPath.row)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.NotificationListArray.count - 1 {
            if self.pageIndex != self.lastIndex {
                self.pageIndex = self.pageIndex + 1
                if self.NotificationListArray.count != 0 {
                    let urlString = API_URL + "notification?page=\(self.pageIndex)"
                    let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                    self.Webservice_Notifications(url: urlString, params:params)
                    
                }
            }
        }
    }
    
}
//MARK: Webservices
extension NotificationsVC {
    func Webservice_Notifications(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let ResponseData = jsonResponse!["data"].dictionaryValue
                    if self.pageIndex == 1 {
                        self.lastIndex = Int(ResponseData["last_page"]!.stringValue)!
                        self.NotificationListArray.removeAll()
                    }
                    let ListData = ResponseData["data"]!.arrayValue
                    for data in ListData
                    {
                        
                        let ListObj = ["order_id":data["order_id"].stringValue,"order_number":data["order_number"].stringValue,"message":data["message"].stringValue,"order_status":data["order_status"].stringValue,"date":data["date"].stringValue,"type":data["type"].stringValue]
                        self.NotificationListArray.append(ListObj)
                    }
                    self.TableView_NotificationListing.delegate = self
                    self.TableView_NotificationListing.dataSource = self
                    self.TableView_NotificationListing.reloadData()
                }
                else {
                    self.TableView_NotificationListing.delegate = self
                    self.TableView_NotificationListing.dataSource = self
                    self.TableView_NotificationListing.reloadData()
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_Notifications_Read(url:String, params:NSDictionary,sender:Int) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let data = self.NotificationListArray[sender]
                    //                     if data["order_status"]! == "1"
                    //                    {
                    //                        let vc = self.storyboard?.instantiateViewController(identifier: "MyOrdersDetailsVC") as! MyOrdersDetailsVC
                    //                        vc.order_number = data["order_number"]!
                    //                        self.present(vc, animated: true)
                    //                    }
                    if data["order_status"]! == "2" || data["order_status"]! == "3" || data["order_status"]! == "4"
                    {
                        let vc = self.storyboard?.instantiateViewController(identifier: "TrackOrderVC") as! TrackOrderVC
                        vc.order_id = data["order_id"]!
                        vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: false)
                    }
                    else if data["order_status"]! == "1" || data["order_status"]! == "6"
                    {
                        let vc = self.storyboard?.instantiateViewController(identifier: "MyOrdersDetailsVC") as! MyOrdersDetailsVC
                        vc.order_number = data["order_number"]!
                        vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: false)
                    }
                    else if data["order_status"]! == "7" || data["order_status"]! == "8" || data["order_status"]! == "9" || data["order_status"]! == "10"
                    {
                        let vc = self.storyboard?.instantiateViewController(identifier: "TrackReturnVC") as! TrackReturnVC
                        vc.order_id = data["order_id"]!
                        vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: false)
                    }
                    
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
