//
//  FilterVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 12/06/21.
//

import UIKit
protocol SelectFilterDeleget {
    func FilterText(Selectedtext:String)
}
class SortbyCell: UITableViewCell {
    @IBOutlet weak var lbl_Name: UILabel!
}
class FilterVC: UIViewController {

    @IBOutlet weak var TableView_FilterList: UITableView!
    @IBOutlet weak var lbl_title: UILabel!
    var SortingArray = [String]()
    var deleget :SelectFilterDeleget!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "Sort By".localiz()
        self.TableView_FilterList.tableFooterView = UIView()
        SortingArray = ["Latest","Price Low to high","Price High to low","Rating Low to high","Rating High to low"]
        self.TableView_FilterList.delegate = self
        self.TableView_FilterList.dataSource = self
        self.TableView_FilterList.reloadData()
    }
    @IBAction func btnTap_close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK: Tableview Methods
extension FilterVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SortingArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TableView_FilterList.dequeueReusableCell(withIdentifier: "SortbyCell") as! SortbyCell
        cell.lbl_Name.text = self.SortingArray[indexPath.row].localiz()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.SortingArray[indexPath.row] == "Latest"
        {
            self.dismiss(animated: false) {
                self.deleget.FilterText(Selectedtext: "new")
            }
        }
        else if self.SortingArray[indexPath.row] == "Price Low to high"
        {
            self.dismiss(animated: false) {
                self.deleget.FilterText(Selectedtext: "price-low-to-high")
            }
        }
        else if self.SortingArray[indexPath.row] == "Price High to low"
        {
            self.dismiss(animated: false) {
                self.deleget.FilterText(Selectedtext: "price-high-to-low")
            }
        }
        else if self.SortingArray[indexPath.row] == "Ratting High to low"
        {
            self.dismiss(animated: false) {
                self.deleget.FilterText(Selectedtext: "ratting-high-to-low")
            }
        }
        else if self.SortingArray[indexPath.row] == "Ratting Low to high"
        {
            self.dismiss(animated: false) {
                self.deleget.FilterText(Selectedtext: "ratting-low-to-high")
            }
        }
        
    }
    
}
