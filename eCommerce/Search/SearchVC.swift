//
//  SearchVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 15/05/21.
//

import UIKit
import SwiftyJSON
import SDWebImage

class SearchCell: UITableViewCell {
    
    @IBOutlet weak var img_Search: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
}
class SearchVC: UIViewController {
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var TableView_SearchList: UITableView!
    @IBOutlet weak var lbl_search: UILabel!
    var pageIndex = 1
    var lastIndex = 0
    var AllItemsArray = [[String:String]]()
    var SearchArray = [[String:String]]()
    var searchTxt = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_search.text = "Search".localiz()
        self.TableView_SearchList.tableFooterView = UIView()
        let urlString = API_URL + "searchproducts"
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
        self.Webservice_getSearch(url: urlString, params:params)
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
    
    @IBAction func textTap_Search(_ sender: UITextField) {
        self.searchTxt = sender.text!
        self.SearchArray.removeAll()
        if searchTxt == "" {
            
            self.TableView_SearchList.reloadData()
        }
        else {
            for data in AllItemsArray {
                if data["product_name"]!.lowercased().contains(self.txt_search.text!.lowercased()) == true || data["tags"]!.lowercased().contains(self.txt_search.text!.lowercased()) == true
                {
                    let productObj = ["id":data["id"]!,"product_name":data["product_name"]!,"product_image":
                                        data["product_image"]!,"tags":data["tags"]!]
                    self.SearchArray.append(productObj)
                }
            }
            self.TableView_SearchList.reloadData()
        }
    }
    
}
//MARK: Tableview methods
extension SearchVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)

        imagedata.contentMode = .scaleAspectFit
        self.TableView_SearchList.backgroundView = imagedata
        if self.SearchArray.count == 0 {
            imagedata.image = UIImage(named: "ic_searchEmpty")
        }
        else {
            imagedata.image = UIImage(named: "")
        }
        return SearchArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TableView_SearchList.dequeueReusableCell(withIdentifier: "SearchCell") as! SearchCell
        cornerRadius(viewName: cell.img_Search, radius: 4.0)
        let data = self.SearchArray[indexPath.row]
        cell.lbl_Name.text = data["product_name"]!
        cell.img_Search.sd_imageTransition = .fade
        let items = indexPath.item % 6
        cell.img_Search.sd_setImage(with: URL(string: data["product_image"]!), placeholderImage: UIImage(named: "b_0\(items)"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.SearchArray[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
        vc.item_id = data["id"]!
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    
}
//MARK: Webservices
extension SearchVC {
    func Webservice_getSearch(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let responcedata = jsonResponse!["data"].arrayValue
                    self.SearchArray.removeAll()
                    self.AllItemsArray.removeAll()
                    for product in responcedata {
                        let productimage = product["productimage"].dictionaryValue
                        let productObj = ["id":product["id"].stringValue,"product_name":product["product_name"].stringValue,"product_image":
                                            productimage["image_url"]!.stringValue,"tags":product["tags"].stringValue]
                        self.AllItemsArray.append(productObj)
                    }
                    self.TableView_SearchList.delegate = self
                    self.TableView_SearchList.dataSource = self
                    self.TableView_SearchList.reloadData()
                }
                else {
                    self.TableView_SearchList.delegate = self
                    self.TableView_SearchList.dataSource = self
                    self.TableView_SearchList.reloadData()
                    // showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
