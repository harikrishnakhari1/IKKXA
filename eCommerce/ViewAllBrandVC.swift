//
//  ViewAllBrandVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 11/09/21.
//

import UIKit
import SwiftyJSON
import SDWebImage

class ViewAllBrandVC: UIViewController {
    
    @IBOutlet weak var CollectionView_BrandsList: UICollectionView!
    @IBOutlet weak var lbl_title: UILabel!
    var BrandsArray = [[String:String]]()
    var pageIndex = 1
    var lastIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lbl_title.text = "All Brands".localiz()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.pageIndex = 1
        self.lastIndex = 0
        // MARK:- Category Api
        let urlString = API_URL + "brands"
        self.Webservice_GetBrands(url: urlString,params: [:])
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
}


extension ViewAllBrandVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
//        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.CollectionView_BrandsList.bounds.size.width, height: self.CollectionView_BrandsList.bounds.size.height))
//        let messageLabel = UILabel(frame: rect)
//        messageLabel.textColor = UIColor.lightGray
//        messageLabel.numberOfLines = 0
//        messageLabel.textAlignment = .center
//        messageLabel.sizeToFit()
//        self.CollectionView_BrandsList.backgroundView = messageLabel;
//        if self.BrandsArray.count == 0 {
//            messageLabel.text = "No data found".localized()
//        }
//        else {
//            messageLabel.text = ""
//        }
        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)

        imagedata.contentMode = .scaleAspectFit
        self.CollectionView_BrandsList.backgroundView = imagedata
        if self.BrandsArray.count == 0 {
            imagedata.image = UIImage(named: "ic_Nodata")
            
        }
        else {
            imagedata.image = UIImage(named: "")
        }
        return BrandsArray.count
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.CollectionView_BrandsList.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
        let data = self.BrandsArray[indexPath.item]
        cell.lbl_name.text! = data["brand_name"]!
        cell.img_Category.sd_imageTransition = .fade
        let items = indexPath.item % 6
        cell.img_Category.sd_setImage(with: URL(string: data["image_url"]!), placeholderImage: UIImage(named: "b_0\(items)"))
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.cornerRadius = 8.0
        cell.layer.borderWidth = 0.4
        if items == 0 {
            cell.cell_view.backgroundColor = APP_COLOR1
            
        }
        else if items == 1 {
            cell.cell_view.backgroundColor = APP_COLOR2
        }
        else if items == 2 {
            cell.cell_view.backgroundColor = APP_COLOR3
        }
        else if items == 3 {
            cell.cell_view.backgroundColor = APP_COLOR4
        }
        else if items == 4 {
            cell.cell_view.backgroundColor = APP_COLOR5
        }
        else if items == 5 {
            cell.cell_view.backgroundColor = APP_COLOR6
        }
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width - 55) / 3, height: 150)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = self.BrandsArray[indexPath.item]
        let vc = self.storyboard?.instantiateViewController(identifier: "BrandWiseitemVC") as! BrandWiseitemVC
        vc.Brand_id = data["id"]!
        vc.StoreName = data["brand_name"]!
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == self.BrandsArray.count - 1 {
            if self.pageIndex != self.lastIndex {
                self.pageIndex = self.pageIndex + 1
                if self.BrandsArray.count != 0 {
                    let urlString = API_URL + "brands?page=\(self.pageIndex)"
                    self.Webservice_GetBrands(url: urlString,params: [:])
                }
            }
        }
    }
    
    
}

//MARK: Webservices
extension ViewAllBrandVC
{
    func Webservice_GetBrands(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "GET", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let ResponseData = jsonResponse!["vendors"].dictionaryValue
                    if self.pageIndex == 1 {
                        self.lastIndex = Int(ResponseData["last_page"]!.stringValue)!
                        self.BrandsArray.removeAll()
                    }
                    let ListData = ResponseData["data"]!.arrayValue
                    for data in ListData
                    {
                        let Obj = ["id":data["id"].stringValue,"brand_name":data["brand_name"].stringValue,"image_url":data["image_url"].stringValue]
                        self.BrandsArray.append(Obj)
                    }
                    self.CollectionView_BrandsList.delegate = self
                    self.CollectionView_BrandsList.dataSource = self
                    self.CollectionView_BrandsList.reloadData()
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
