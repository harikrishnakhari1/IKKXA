//
//  AdderessAddVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 07/08/21.
//

import UIKit
import SwiftyJSON

class AdderessAddVC: UIViewController {

    @IBOutlet weak var txt_FirstName: UITextField!
    @IBOutlet weak var txt_LastName: UITextField!
    @IBOutlet weak var txt_StreetAddress: UITextField!
    @IBOutlet weak var txt_Landmark: UITextField!
    @IBOutlet weak var txt_Postcode: UITextField!
    @IBOutlet weak var txt_Phone: UITextField!
    @IBOutlet weak var txt_Email: UITextField!
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var btn_Save: UIButton!
    var isedit = String()
    var addressid = String()
    var EditData = JSON()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "Add Address".localiz()
        self.btn_Save.setTitle("Save".localiz(), for: .normal)
        
        if self.isedit == "1"
        {
            self.txt_FirstName.text = self.EditData["first_name"].stringValue
            self.txt_LastName.text = self.EditData["last_name"].stringValue
            self.txt_StreetAddress.text = self.EditData["street_address"].stringValue
            self.txt_Landmark.text = self.EditData["landmark"].stringValue
            self.txt_Postcode.text = self.EditData["pincode"].stringValue
            self.txt_Phone.text = self.EditData["mobile"].stringValue
            self.txt_Email.text = self.EditData["email"].stringValue
            
        }
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    @IBAction func btnTap_Save(_ sender: UIButton) {
        if self.txt_FirstName.text == "" || self.txt_LastName.text == "" || self.txt_StreetAddress.text == "" || self.txt_Phone.text == "" || self.txt_Postcode.text == "" || self.txt_Email.text == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter all details".localiz())
        }
        else{
            if self.isedit == "1"
            {
                let urlString = API_URL + "editaddress"
                let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"first_name":self.txt_FirstName.text!,"last_name":self.txt_LastName.text!,"street_address":self.txt_StreetAddress.text!,"landmark":self.txt_Landmark.text!,"pincode":self.txt_Postcode.text!,"mobile":self.txt_Phone.text!,"email":self.txt_Email.text!,"address_id":self.addressid]
                self.Webservice_UpdateAddress(url: urlString, params:params)
            }
            else{
                let urlString = API_URL + "saveaddress"
                let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"first_name":self.txt_FirstName.text!,"last_name":self.txt_LastName.text!,"street_address":self.txt_StreetAddress.text!,"landmark":self.txt_Landmark.text!,"pincode":self.txt_Postcode.text!,"mobile":self.txt_Phone.text!,"email":self.txt_Email.text!]
                self.Webservice_AddAddress(url: urlString, params:params)
            }
            
        }
        
    }
}
//MARK: Webservices
extension AdderessAddVC {
    func Webservice_AddAddress(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    self.dismiss(animated: false)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_UpdateAddress(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    self.dismiss(animated: false)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
