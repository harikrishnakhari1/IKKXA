//
//  ManageAddressVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 14/05/21.
//

import UIKit
import SwiftyJSON
protocol getAddressDelegate {
    func getAddressData(Address:JSON)
}
//MARK: Rating Table cell
class AddressCell: UITableViewCell {
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var lbl_mobile: UILabel!
    @IBOutlet weak var btn_Add: UIButton!
    @IBOutlet weak var view_holder: UIView!
}
class ManageAddressVC: UIViewController {
    
    @IBOutlet weak var Tableview_AddressList: UITableView!
    @IBOutlet weak var btn_Add: UIButton!
    @IBOutlet weak var lbl_titleLabel: UILabel!
    var AddressArray = [JSON]()
    var delegate: getAddressDelegate!
    var isCheckoutPage = String()
    var address_id = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Tableview_AddressList.tableFooterView = UIView()
        self.lbl_titleLabel.text = "My Addresses".localiz()
        cornerRadius(viewName: self.btn_Add, radius: 6.0)
    }
    override func viewWillAppear(_ animated: Bool) {
        let urlString = API_URL + "getaddress"
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
        self.Webservice_GetAddress(url: urlString, params:params)
    }
}
extension ManageAddressVC {
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    @IBAction func btnTap_Add(_ sender: UIButton) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
            appNavigation.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.windows[0].rootViewController = appNavigation
        }
        else {
            let vc = self.storyboard?.instantiateViewController(identifier: "AdderessAddVC") as! AdderessAddVC
            vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
        }
    }
    
}
//MARK: Tableview Methods
extension ManageAddressVC: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
            let imagedata = UIImageView(frame: rect)
            
            imagedata.contentMode = .scaleAspectFit
            self.Tableview_AddressList.backgroundView = imagedata
            if self.AddressArray.count == 0 {
                imagedata.image = UIImage(named: "ic_Nodata")
            }
            else {
                imagedata.image = UIImage(named: "")
            }
            return AddressArray.count
        } else {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_AddressList.dequeueReusableCell(withIdentifier: "AddressCell") as! AddressCell
        if self.AddressArray.count != 0 {
            let data = self.AddressArray[indexPath.row]
            
            cell.lbl_name.text = "\(data["first_name"].stringValue) \(data["last_name"].stringValue)"
            cell.lbl_Address.text = "\(data["street_address"].stringValue) \(data["landmark"].stringValue) - \(data["pincode"].stringValue)"
            cell.lbl_mobile.text = "\(data["mobile"].stringValue)"
            cell.lbl_email.text = "\(data["email"].stringValue)"
        }
        
        cell.btn_Add.isHidden =  indexPath.section == 0
        cell.view_holder.isHidden =  indexPath.section == 1
        return cell
    }
    @objc func btnTap_check(sender:UIButton!) {
        let data = self.AddressArray[sender.tag]
        self.delegate.getAddressData(Address: data)
        self.dismiss(animated: false)
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if indexPath.section == 0 {
            // action one
            let editAction = UITableViewRowAction(style: .default, title: "Edit", handler: { (action, indexPath) in
                let data = self.AddressArray[indexPath.row]
                let vc = self.storyboard?.instantiateViewController(identifier: "AdderessAddVC") as! AdderessAddVC
                vc.isedit = "1"
                vc.addressid = data["id"].stringValue
                vc.EditData = data
                vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
            })
            editAction.backgroundColor = UIColor.systemBlue
            
            // action two
            let deleteAction = UITableViewRowAction(style: .default, title: "Delete", handler: { (action, indexPath) in
                
                
                
                
                let data = self.AddressArray[indexPath.row]
                self.address_id = data["id"].stringValue
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConformationAlertVC") as! ConformationAlertVC
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.deleget = self
                vc.isSelectedTypes = "3"
                self.present(vc,animated: true,completion: nil)
                
            })
            deleteAction.backgroundColor = UIColor.systemRed
            
            return [editAction, deleteAction]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isCheckoutPage == "1" && indexPath.section == 0
        {
            let data = self.AddressArray[indexPath.row]
            self.delegate.getAddressData(Address: data)
            self.dismiss(animated: false)
        }
        
    }
    
    //    func ApproveFunc(indexPath: IndexPath) {
    //        print(indexPath.row)
    //    }
    //    func rejectFunc(indexPath: IndexPath) {
    //        print(indexPath.row)
    //    }
}
//MARK: Webservices
extension ManageAddressVC {
    func Webservice_GetAddress(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let responseData = jsonResponse!["data"].arrayValue
                    self.AddressArray = responseData
                    self.btn_Add.isHidden = self.AddressArray.count != 0
                    self.Tableview_AddressList.isHidden = self.AddressArray.count == 0
                    self.Tableview_AddressList.delegate = self
                    self.Tableview_AddressList.dataSource = self
                    self.Tableview_AddressList.reloadData()
                    
                    let customView = UIView(frame: CGRect(x: 0, y: 0, width: self.Tableview_AddressList.frame.width, height: 15))
                    customView.backgroundColor = UIColor.clear
                    let titleLabel = UILabel(frame: CGRect(x:10,y: 5 ,width:customView.frame.width,height:15))
                    titleLabel.numberOfLines = 0;
                    titleLabel.backgroundColor = UIColor.clear
                    titleLabel.textColor = UIColor.orange
                    titleLabel.textAlignment = .center
                    titleLabel.font = UIFont(name: "Montserrat-Regular", size: 6)
                    titleLabel.text  = "Swipe left to Edit/Delete".localiz()
                    customView.addSubview(titleLabel)
                    self.Tableview_AddressList.tableFooterView = customView
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_DeleteAddress(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let urlString = API_URL + "getaddress"
                    let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                    self.Webservice_GetAddress(url: urlString, params:params)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
extension ManageAddressVC : DismissAlertDeleget
{
    func DimissSucess() {
        
//        let urlString = API_URL + "deleteproduct"
//        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),"cart_id":self.cart_id]
//        self.Webservice_DeleteItemtoCart(url: urlString, params: params)
        
        let urlString = API_URL + "deleteaddress"
        let params: NSDictionary = ["address_id":self.address_id]
        self.Webservice_DeleteAddress(url: urlString, params:params)
        
    }
    
    
}
