//
//  WalletVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 14/09/21.
//

import UIKit
import SwiftyJSON

class WalletCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var img_wallet: UIImageView!
    @IBOutlet weak var view_bg: CornerView!
    @IBOutlet weak var lbl_amount: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_orderno: UILabel!
}
class WalletVC: UIViewController {
    
    @IBOutlet weak var TableView_WalletList: UITableView!
    var pageIndex = 1
    var lastIndex = 0
    var WalletListArray = [[String:String]]()
    @IBOutlet weak var lbl_WalletAmount: UILabel!
    
    @IBOutlet weak var lblstr_transaction: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var btn_Addmoney: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "My Wallet".localiz()
        self.lbl_Title.text = "My Wallet Balance".localiz()
        self.lblstr_transaction.text = "Transaction History".localiz()
        cornerRadius(viewName: self.btn_Addmoney, radius: self.btn_Addmoney.frame.height / 2)
        self.btn_Addmoney.setTitle(UserDefaultManager.getStringFromUserDefaults(key: UD_currency), for: .normal)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.pageIndex = 1
        self.lastIndex = 0
        let urlString = API_URL + "wallet?page=\(self.pageIndex)"
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
        self.Webservice_TransactionList(url: urlString, params:params)
    }
    
    @IBAction func btnTap_Addmoney(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "AddMoneytoWalletVC") as! AddMoneytoWalletVC
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
}
//MARK: Tableview Methods
extension WalletVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)
        
        imagedata.contentMode = .scaleAspectFit
        self.TableView_WalletList.backgroundView = imagedata
        if self.WalletListArray.count == 0 {
            imagedata.image = UIImage(named: "ic_Nodata")
        }
        else {
            imagedata.image = UIImage(named: "")
        }
        return WalletListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TableView_WalletList.dequeueReusableCell(withIdentifier: "WalletCell") as! WalletCell
        let data = self.WalletListArray[indexPath.row]
        if data["transaction_type"]! == "1"
        {
            cell.view_bg.backgroundColor = UIColor.systemRed
            cell.img_wallet.image = UIImage(named:"ic_package_Cancel")
            cell.lbl_Status.text = "Order Cancelled"
            
            let setdate = DateFormater.getBirthDateStringFromDateString(givenDate:data["date"]!)
            cell.lbl_date.text = setdate
            cell.lbl_orderno.text = "Order id : \(data["order_number"]!)"
            let Price = formatter.string(for: data["wallet"]!.toDouble)!
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                cell.lbl_amount.text = "\(Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                
            }
            else {
                
                cell.lbl_amount.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Price)"
            }
        }
        else if data["transaction_type"]! == "2"
        {
            cell.view_bg.backgroundColor = UIColor.systemGreen
            cell.img_wallet.image = UIImage(named:"ic_package_Confirm")
            cell.lbl_Status.text = "Order Confirmed"
            
            let setdate = DateFormater.getBirthDateStringFromDateString(givenDate:data["date"]!)
            cell.lbl_date.text = setdate
            cell.lbl_orderno.text = "Order id : \(data["order_number"]!)"
            let Price = formatter.string(for: data["wallet"]!.toDouble)!
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                cell.lbl_amount.text = "-\(Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                
            }
            else {
                
                cell.lbl_amount.text = "-\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Price)"
            }
        }
        else if data["transaction_type"]! == "3"
        {
            cell.view_bg.backgroundColor = UIColor.systemPurple
            cell.img_wallet.image = UIImage(named:"ic_wallet")
            cell.lbl_Status.text = "Referral Amount"
            
            let setdate = DateFormater.getBirthDateStringFromDateString(givenDate:data["date"]!)
            cell.lbl_date.text = setdate
            cell.lbl_orderno.text = "User Name : \(data["username"]!)"
            let Price = formatter.string(for: data["wallet"]!.toDouble)!
            
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                cell.lbl_amount.text = "\(Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                
            }
            else {
                
                cell.lbl_amount.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Price)"
            }
        }
        else if data["transaction_type"]! == "4"
        {
            cell.view_bg.backgroundColor = UIColor.systemPurple
            cell.img_wallet.image = UIImage(named:"ic_wallet")
            
            
            let setdate = DateFormater.getBirthDateStringFromDateString(givenDate:data["date"]!)
            cell.lbl_date.text = setdate
            cell.lbl_orderno.text = "Wallet Recharge"
            if data["type"] == "3"
            {
                cell.lbl_Status.text = "RazorPay"
            }
            else if data["type"] == "4"
            {
                cell.lbl_Status.text = "Stripe"
            }
            else if data["type"] == "5"
            {
                cell.lbl_Status.text = "Flutterwave"
            }
            else if data["type"] == "6"
            {
                cell.lbl_Status.text = "Paystack"
            }
            let Price = formatter.string(for: data["wallet"]!.toDouble)!
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                cell.lbl_amount.text = "\(Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                
            }
            else {
                
                cell.lbl_amount.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Price)"
            }
        }
        else if data["transaction_type"]! == "5"
        {
            cell.view_bg.backgroundColor = UIColor.systemGreen
            cell.img_wallet.image = UIImage(named:"ic_package_Cancel")
            cell.lbl_Status.text = "Order Return"
            
            let setdate = DateFormater.getBirthDateStringFromDateString(givenDate:data["date"]!)
            cell.lbl_date.text = setdate
            cell.lbl_orderno.text = "Order id : \(data["order_number"]!)"
            let Price = formatter.string(for: data["wallet"]!.toDouble)!
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                cell.lbl_amount.text = "\(Price)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                
            }
            else {
                
                cell.lbl_amount.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(Price)"
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.WalletListArray.count - 1 {
            if self.pageIndex != self.lastIndex {
                self.pageIndex = self.pageIndex + 1
                if self.WalletListArray.count != 0 {
                    let urlString = API_URL + "wallet?page=\(self.pageIndex)"
                    let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                    self.Webservice_TransactionList(url: urlString, params:params)
                }
            }
        }
    }
}
extension WalletVC
{
    func Webservice_TransactionList(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let ResponseData = jsonResponse!["data"].dictionaryValue
                    
                    let WalletPrice = formatter.string(for: jsonResponse!["walletamount"].stringValue.toDouble)!
                    
                    if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                    {
                        self.lbl_WalletAmount.text = "\(WalletPrice)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                        
                    }
                    else {
                        
                        self.lbl_WalletAmount.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(WalletPrice)"
                    }
                    if self.pageIndex == 1 {
                        self.lastIndex = Int(ResponseData["last_page"]!.stringValue)!
                        self.WalletListArray.removeAll()
                    }
                    let ListData = ResponseData["data"]!.arrayValue
                    for data in ListData
                    {
                        let ListObj = ["order_number":data["order_number"].stringValue,"transaction_type":data["transaction_type"].stringValue,"wallet":data["wallet"].stringValue,"date":data["date"].stringValue,"username":data["username"].stringValue,"type":data["type"].stringValue]
                        self.WalletListArray.append(ListObj)
                    }
                    
                    self.TableView_WalletList.delegate = self
                    self.TableView_WalletList.dataSource = self
                    self.TableView_WalletList.reloadData()
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
