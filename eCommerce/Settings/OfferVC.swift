//
//  OfferVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 14/09/21.
//

import UIKit
import SwiftyJSON

class OffersCell:UITableViewCell {
    
    @IBOutlet weak var cell_view: CornerView!
    @IBOutlet weak var lbl_CouponAmount: UILabel!
    @IBOutlet weak var lbl_expire: UILabel!
    @IBOutlet weak var lbl_expireDate: UILabel!
    @IBOutlet weak var lbl_CouponString: UILabel!
}
class OfferVC: UIViewController {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var TableView_OffersList: UITableView!
    var pageIndex = 1
    var lastIndex = 0
    var OffersListArray = [[String:String]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "Offers".localiz()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.pageIndex = 1
        self.lastIndex = 0
        let urlString = API_URL + "coupons"
        self.Webservice_CoponcodeList(url: urlString, params: [:])
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
}
//MARK: Tableview Methods
extension OfferVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)
        
        imagedata.contentMode = .scaleAspectFit
        self.TableView_OffersList.backgroundView = imagedata
        if self.OffersListArray.count == 0 {
            imagedata.image = UIImage(named: "ic_Nodata")
        }
        else {
            imagedata.image = UIImage(named: "")
        }
        return OffersListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TableView_OffersList.dequeueReusableCell(withIdentifier: "OffersCell") as! OffersCell
        cornerRadius(viewName: cell.lbl_CouponAmount, radius: 6.0)
        let data = self.OffersListArray[indexPath.row]
        if data["type"]! == "1"
        {
            cell.lbl_CouponString.text = "Coupon Code Valued at \(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(data["amount"]!) at Order"
        }
        else if data["type"]! == "0"
        {
            cell.lbl_CouponString.text = "Coupon Code Valued at \(data["percentage"]!)% off at Order"
        }
        cell.lbl_CouponAmount.text = data["coupon_name"]!
        let setdate = DateFormater.getBirthDateStringFromDateString(givenDate:data["end_date"]!)
        cell.lbl_expireDate.text = setdate
        let items = indexPath.item % 6
        setBorder(viewName: cell.cell_view, borderwidth: 0.4, borderColor: UIColor.lightGray.cgColor, cornerRadius: 8.0)
        if items == 0 {
            cell.cell_view.backgroundColor = APP_COLOR1
        }
        else if items == 1 {
            cell.cell_view.backgroundColor = APP_COLOR2
        }
        else if items == 2 {
            cell.cell_view.backgroundColor = APP_COLOR3
        }
        else if items == 3 {
            cell.cell_view.backgroundColor = APP_COLOR4
        }
        else if items == 4 {
            cell.cell_view.backgroundColor = APP_COLOR5
        }
        else if items == 5 {
            cell.cell_view.backgroundColor = APP_COLOR6
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.OffersListArray.count - 1 {
            if self.pageIndex != self.lastIndex {
                self.pageIndex = self.pageIndex + 1
                if self.OffersListArray.count != 0 {
                    let urlString = API_URL + "coupons?page=\(self.pageIndex)"
                    self.Webservice_CoponcodeList(url: urlString, params: [:])
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
extension OfferVC
{
    func Webservice_CoponcodeList(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "GET", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let ResponseData = jsonResponse!["data"].dictionaryValue
                    if self.pageIndex == 1 {
                        self.lastIndex = Int(ResponseData["last_page"]!.stringValue)!
                        self.OffersListArray.removeAll()
                    }
                    let ListData = ResponseData["data"]!.arrayValue
                    for data in ListData
                    {
                        let ListObj = ["coupon_name":data["coupon_name"].stringValue,"type":data["type"].stringValue,"percentage":data["percentage"].stringValue,"amount":data["amount"].stringValue,"start_date":data["start_date"].stringValue,"end_date":data["end_date"].stringValue]
                        self.OffersListArray.append(ListObj)
                    }
                    
                    self.TableView_OffersList.delegate = self
                    self.TableView_OffersList.dataSource = self
                    self.TableView_OffersList.reloadData()
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
