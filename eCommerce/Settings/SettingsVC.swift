//
//  SettingsVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 14/05/21.
//

import UIKit
import LanguageManager_iOS
import SwiftyJSON

class SettingsVC: UIViewController {

    @IBOutlet weak var btn_ChangePassword: UIButton!
    @IBOutlet weak var btn_MyAddress: UIButton!
    @IBOutlet weak var btn_ChangeLayout: UIButton!
    @IBOutlet weak var btn_MyWallet: UIButton!
    @IBOutlet weak var btn_Offers: UIButton!
    @IBOutlet weak var btn_switch: UISwitch!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var btn_Refer: UIButton!
    @IBOutlet weak var btn_Notification: UIButton!
    var isnotification = String()
    
    override func viewDidLoad() {
        
        self.lbl_title.text = "Settings".localiz()
        self.btn_ChangePassword.setTitle("Change Password".localiz(), for: .normal)
        self.btn_MyWallet.setTitle("Delete Account".localiz(), for: .normal)
        self.btn_Offers.setTitle("Offers".localiz(), for: .normal)
        self.btn_MyAddress.setTitle("My Address".localiz(), for: .normal)
       // self.btn_Notification.setTitle("Get Notification".localiz(), for: .normal)
        self.btn_Refer.setTitle("Refer & Earn".localiz(), for: .normal)
        self.btn_ChangeLayout.setTitle("Change Language".localiz(), for: .normal)
        
        if self.isnotification == "1"
        {
            self.btn_switch.setOn(true, animated: true)
        }
        else{
            self.btn_switch.setOn(false, animated: true)
        }
        super.viewDidLoad()
        if UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "en" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "N/A" {
            self.btn_ChangePassword.titleLabel!.textAlignment = .left
            self.btn_MyAddress.titleLabel!.textAlignment = .left
            self.btn_ChangeLayout.titleLabel!.textAlignment = .left
            self.btn_MyWallet.titleLabel!.textAlignment = .left
            self.btn_Offers.titleLabel!.textAlignment = .left
        }
        else {
            self.btn_ChangePassword.titleLabel!.textAlignment = .right
            self.btn_MyAddress.titleLabel!.textAlignment = .right
            self.btn_ChangeLayout.titleLabel!.textAlignment = .right
            self.btn_MyWallet.titleLabel!.textAlignment = .right
            self.btn_Offers.titleLabel!.textAlignment = .right
        }
    }
    
    @IBAction func btntap_Switch(_ sender: UISwitch) {
        if self.isnotification == "1"
        {
            let urlString = API_URL + "changenotificationstatus"
            let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                        "notification_status":"2"]
            self.Webservice_isNotification(url: urlString, params: params)
        }
        else
        {
            let urlString = API_URL + "changenotificationstatus"
            let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                        "notification_status":"1"]
            self.Webservice_isNotification(url: urlString, params: params)
        }
        
    }
    @IBAction func btnTap_back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    @IBAction func btnTap_Myaddress(_ sender: UIButton) {
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
            appNavigation.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.windows[0].rootViewController = appNavigation
        }
        else {
            let vc = self.storyboard?.instantiateViewController(identifier: "ManageAddressVC") as! ManageAddressVC
            vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
        }
        
    }
    @IBAction func btnTap_ReferEarn(_ sender: UIButton) {
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
            appNavigation.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.windows[0].rootViewController = appNavigation
        }
        else {
            let vc = self.storyboard?.instantiateViewController(identifier: "ReferandEarnVC") as! ReferandEarnVC
            vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
        }
        
    }
   
    @IBAction func btnTap_Layout(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Select Application Layout".localiz(), preferredStyle: .actionSheet)
        let photoLibraryAction = UIAlertAction(title: "English", style: .default) { (action) in
            let selectedLanguage: Languages = .en
            UserDefaultManager.setStringToUserDefaults(value: "en", key: UD_isSelectLng)
            LanguageManager.shared.setLanguage(language: selectedLanguage,
                                               viewControllerFactory: { title -> UIViewController in
                                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                                let objVC = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                                                
                                                return objVC
            }) { view in
                view.transform = CGAffineTransform(scaleX: 2, y: 2)
                view.alpha = 0
            }
        }
        let cameraAction = UIAlertAction(title: "Arabic", style: .default) { (action) in
            let selectedLanguage: Languages = .ar
            UserDefaultManager.setStringToUserDefaults(value: "ar", key: UD_isSelectLng)
            LanguageManager.shared.setLanguage(language: selectedLanguage,
                                               viewControllerFactory: { title -> UIViewController in
                                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                                let objVC = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                                                return objVC
            }) { view in
                view.transform = CGAffineTransform(scaleX: 2, y: 2)
                view.alpha = 0
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel".localiz(), style: .cancel)
        alert.addAction(photoLibraryAction)
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnTap_MyWallet(_ sender: UIButton) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
            appNavigation.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.windows[0].rootViewController = appNavigation
        }
        else {
            /* let vc = self.storyboard?.instantiateViewController(identifier: "WalletVC") as! WalletVC
            self.present(vc, animated: true)*/
            
            let alertVC = UIAlertController(title: "", message: "Are you sure to delete account?".localiz(), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Yes".localiz(), style: .default) { (action) in
               
                let urlString = API_URL + "deleteaccount"
                let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_deleteAccount(url: urlString, params: params)
                
            }
            let noAction = UIAlertAction(title: "No".localiz(), style: .destructive)
            alertVC.addAction(noAction)
            alertVC.addAction(yesAction)
            self.present(alertVC,animated: true,completion: nil)
        }
        
       
          
    }
    
    @IBAction func btnTap_Offers(_ sender: UIButton) {
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
            appNavigation.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.windows[0].rootViewController = appNavigation
        }
        else {
            let vc = self.storyboard?.instantiateViewController(identifier: "OfferVC") as! OfferVC
            vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
        }
    }
    
    @IBAction func btnTap_ChangePassword(_ sender: UIButton) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
            appNavigation.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.windows[0].rootViewController = appNavigation
        }
        else {
            let vc = self.storyboard?.instantiateViewController(identifier: "ChangePassswordVC") as! ChangePassswordVC
            vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
        }
       
    }
    
}
extension SettingsVC {
    func Webservice_isNotification(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    
                    
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    
    func Webservice_deleteAccount(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    
                    UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
                    UserDefaults.standard.set("", forKey:UD_userId)
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                    nav.navigationBar.isHidden = true
                    UIApplication.shared.windows[0].rootViewController = nav
                    
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
