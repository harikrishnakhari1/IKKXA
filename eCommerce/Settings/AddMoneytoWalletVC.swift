//
//  AddMoneytoWalletVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 16/11/21.
//

import UIKit
import SwiftyJSON
import Razorpay
import RaveSDK
class AddMoneytoWalletVC: UIViewController {

    @IBOutlet weak var btn_proceed: UIButton!
    @IBOutlet weak var lblstr_selectpayment: UILabel!
    @IBOutlet weak var lblstr_Wallet: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_currency: UILabel!
    @IBOutlet weak var Tableview_PaymentList: UITableView!
    @IBOutlet weak var txt_Money: UITextField!
    var PaymentListArray = [[String:String]]()
    var razorpay : RazorpayCheckout!
    var Flutterwave_public_key = String()
    var Flutterwave_encryption_key = String()
    var Paystack_key = String()
    var environment = String()
    var paymentType = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btn_proceed.setTitle("Procced to Payment".localiz(), for: .normal)
        self.lbl_title.text = "Add Money".localiz()
        self.lblstr_Wallet.text = "Add Money to Wallet".localiz()
        self.lblstr_selectpayment.text = "Select payment method".localiz()
        
        self.lbl_currency.text = UserDefaultManager.getStringFromUserDefaults(key: UD_currency)
        
//        self.txt_Money.resignFirstResponder()
        self.txt_Money.becomeFirstResponder()
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "en" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "N/A" {
            self.txt_Money!.textAlignment = .left
        }
        else {
            self.txt_Money!.textAlignment = .right
        }
        
        self.txt_Money.addTarget(self, action: #selector(myTextFieldDidChange), for: .editingChanged)
        
        let urlString = API_URL + "paymentlist"
        let params: NSDictionary = [
            "user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
        self.Webservice_PaymentList(url: urlString, params: params)
        
        
        
    }
    @objc func myTextFieldDidChange(_ textField: UITextField) {

        if let amountString = textField.text?.currencyInputFormatting() {
            textField.text = amountString
        }
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
    @IBAction func btnTap_ProccedtoPayment(_ sender: UIButton) {
        if self.txt_Money.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter amount!".localiz())
        }
        else if Double(self.txt_Money.text!.replacingOccurrences(of:",", with: ""))! < 1.0
        {
            showAlertMessage(titleStr: "", messageStr: "Amount must be greater than 1".localiz())
        }
        else if self.paymentType == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please select payment method!".localiz())
        }
        else{
            if self.paymentType == "1"
            {
                self.showPaymentForm()
            }
            else if self.paymentType == "2" {
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "CardInfoVC") as! CardInfoVC
                VC.modalPresentationStyle = .overFullScreen
                VC.modalTransitionStyle = .crossDissolve
                VC.delegate = self
                self.present(VC,animated: true,completion: nil)
            }
            else if self.paymentType == "3" {
                Flutterwave()
            }
            else if self.paymentType == "4" {
                Paystack()
            }
        }
    }
}
//MARK: Tableview Methods
extension AddMoneytoWalletVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return PaymentListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_PaymentList.dequeueReusableCell(withIdentifier: "PaymentCell") as! PaymentCell
        cornerRadius(viewName: cell.img_Payment, radius: 4.0)
        let data = self.PaymentListArray[indexPath.row]
        let environment = data["environment"]!
        setBorder(viewName: cell.cell_view, borderwidth: 0.4, borderColor: UIColor.lightGray.cgColor, cornerRadius: 8.0)
        if data["payment_name"]! == "Stripe"
        {
            cell.img_Payment.image = UIImage(named:"ic_stripe")
            cell.lbl_PaymentName.text = "\(data["payment_name"]!) Payment"
//            if environment == "1" {
//                STPPaymentConfiguration.shared.publishableKey = data["test_public_key"]!
//            }
//            else {
//                STPPaymentConfiguration.shared.publishableKey = data["live_public_key"]!
//            }
        }
        else if data["payment_name"]! == "RazorPay"
        {
            cell.img_Payment.image = UIImage(named:"ic_rezorpay")
            cell.lbl_PaymentName.text = "\(data["payment_name"]!) Payment"
            if environment == "1" {
                self.razorpay = RazorpayCheckout.initWithKey(data["test_public_key"]!, andDelegate: self)
            }
            else {
                self.razorpay = RazorpayCheckout.initWithKey(data["live_public_key"]!, andDelegate: self)
            }
        }
        else if data["payment_name"]! == "Flutterwave"
        {
            cell.img_Payment.image = UIImage(named:"ic_Flutterwave")
            cell.lbl_PaymentName.text = "\(data["payment_name"]!) Payment"
            self.environment = environment
            if environment == "1" {
                self.Flutterwave_public_key = data["test_public_key"]!
                self.Flutterwave_encryption_key = data["encryption_key"]!
            }
            else {
                self.Flutterwave_public_key = data["test_public_key"]!
                self.Flutterwave_encryption_key = data["encryption_key"]!
            }
            
        }
        else if data["payment_name"]! == "Paystack"
        {
            cell.img_Payment.image = UIImage(named:"ic_Paystack")
            cell.lbl_PaymentName.text = "\(data["payment_name"]!) Payment"
            if environment == "1" {
                self.Paystack_key = data["test_public_key"]!
            }
            else {
                self.Paystack_key = data["live_public_key"]!
            }
        }
        if data["isselect"]! == "1" {
            cell.img_Checkmark.image = UIImage(named: "ic_checkmark")
            cell.img_Checkmark.isHidden = false
        }
        else {
            cell.img_Checkmark.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<self.PaymentListArray.count {
            var payment = self.PaymentListArray[i]
            payment["isselect"] = "0"
            self.PaymentListArray.remove(at: i)
            self.PaymentListArray.insert(payment, at: i)
        }
        
        var data = self.PaymentListArray[indexPath.row]
       // payment_type = COD : 1, Wallet : 2, RazorPay : 3, Stripe : 4, Flutterwave : 5
        if data["payment_name"]! == "RazorPay" {
            self.paymentType = "1"
        }
        else if data["payment_name"]! == "Stripe" {
            self.paymentType = "2"
        }
        else if data["payment_name"]! == "Flutterwave" {
            self.paymentType = "3"
        }
        else if data["payment_name"]! == "Paystack" {
            self.paymentType = "4"
        }
        data["isselect"]! = "1"
        self.PaymentListArray.remove(at: indexPath.row)
        self.PaymentListArray.insert(data, at: indexPath.row)
        
        self.Tableview_PaymentList.reloadData()
        
    }
}
extension AddMoneytoWalletVC
{
    func Webservice_PaymentList(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let responseData = jsonResponse!["paymentlist"].arrayValue
                    
                    for data in responseData {
                        if data["payment_name"].stringValue != "COD" && data["payment_name"].stringValue != "Wallet"
                        {
                            let productObj = ["payment_name":data["payment_name"].stringValue,"test_public_key":data["test_public_key"].stringValue,"live_public_key":data["live_public_key"].stringValue,"environment":data["environment"].stringValue,"isselect":"0","encryption_key":data["encryption_key"].stringValue]
                            self.PaymentListArray.append(productObj)
                        }
                        
                    }
                    
                    self.Tableview_PaymentList.delegate = self
                    self.Tableview_PaymentList.dataSource = self
                    self.Tableview_PaymentList.reloadData()
                    
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    
    func Webservice_Addmoney(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                   // showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                    self.dismiss(animated: false)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}

extension AddMoneytoWalletVC : RavePayProtocol
{
    func Flutterwave() {
        let config = RaveConfig.sharedConfig()
        config.country = "US" // Country Code
        config.currencyCode = "USD" // Currency
        config.email = UserDefaultManager.getStringFromUserDefaults(key: UD_userEmail)
        if environment == "1"
        {
            config.isStaging = false
        }
        else{
            config.isStaging = true
        }
        
//        config.isStaging = false // Toggle this for staging and live environment
        config.phoneNumber = UserDefaultManager.getStringFromUserDefaults(key: UD_userMobile) //Phone number
        config.transcationRef = "ref" // transaction ref
        config.firstName = UserDefaultManager.getStringFromUserDefaults(key: UD_userName) // first name
        config.lastName = "" //lastname
        config.meta = [["metaname":"sdk", "metavalue":"ios"]]
        
        config.publicKey = Flutterwave_public_key //Public key
        config.encryptionKey = Flutterwave_encryption_key //Encryption key
        let controller = NewRavePayViewController()
        let nav = UINavigationController(rootViewController: controller)
        controller.amount = self.txt_Money.text!.replacingOccurrences(of: ",", with: "") // Amount
        controller.delegate = self
        self.present(nav, animated: true)
    }
   
    func onDismiss() {
        print("Dismiss")
    }
    func tranasctionSuccessful(flwRef: String?, responseData: [String : Any]?) {
        
        print(responseData?.description ?? "Nothing here")
        let paymentid = responseData!["flwRef"] as! String
            print(paymentid)
        let urlString = API_URL + "recharge"
        let params: NSDictionary = ["user_id":UserDefaults.standard.value(forKey: UD_userId) as! String,"recharge_amount":self.txt_Money.text!.replacingOccurrences(of: ",", with: ""),"payment_id":paymentid,"payment_type":"5"]
        self.Webservice_Addmoney(url: urlString, params: params)
        
//        Optional(["charged_amount": 100, "id": 2519137, "merchantfee": 0, "retry_attempt": <null>, "customerId": 1404045, "fraud_status": ok, "device_fingerprint": 5E5A5BDA-D24A-4A51-906F-835FD7424252, "vbvrespcode": 00, "IP": ::ffff:127.0.0.1, "orderRef": URF_EAEFCA76CECECD68BE_2768537, "acctvalrespmsg": <null>, "chargeResponseCode": 00, "authModelUsed": noauth-saved-card, "txRef": ref, "deletedAt": <null>, "appfee": 3.8, "paymentPage": <null>, "amount": 100, "paymentId": 6528166, "paymentType": card, "charge_type": normal, "settlement_token": <null>, "chargeToken": {
//            "embed_token" = "flw-t1nf-6fc8b66cb7bce7ddebbe7669fa7987e9-k3n";
//            "user_token" = 6fc8b;
//        }, "customer": {
//            AccountId = 217124;
//            createdAt = "2021-09-30T06:18:34.000Z";
//            customertoken = "<null>";
//            deletedAt = "<null>";
//            email = "jek@yopmail.com";
//            fullName = "jek ";
//            id = 1404045;
//            phone = 12121212122222;
//            updatedAt = "2021-09-30T06:18:34.000Z";
//        }, "modalauditid": ee664c937977f17130f5b926d7679e32, "createdAt": 2021-09-30T06:19:28.000Z, "getpaidBatchId": <null>, "currency": USD, "chargeResponseMessage": Approved, "updatedAt": 2021-09-30T06:19:28.000Z, "flwRef": FLW-M03K-52b42a3dc4e76be47aa113aa58400a76, "raveRef": <null>, "redirectUrl": http://127.0.0.1, "is_live": 0, "status": successful, "narration": gravity777, "merchantbearsfee": 1, "paymentPlan": <null>, "cycle": one-time, "acctvalrespcode": <null>, "authurl": N/A, "AccountId": 217124, "vbvrespmessage": Approved])
    }

    func tranasctionFailed(flwRef: String?, responseData: [String : Any]?) {
        print(responseData?.description ?? "Nothing here")
    }
    
    func Paystack() {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "PaystackCardInfoVC") as! PaystackCardInfoVC
        VC.modalPresentationStyle = .overFullScreen
        VC.modalTransitionStyle = .crossDissolve
        VC.delegate = self
        VC.paystackPublicKey = self.Paystack_key
        VC.TotalAmount = Int(Double(self.txt_Money.text!.replacingOccurrences(of: ",", with: ""))! * 100)
        self.present(VC,animated: true,completion: nil)
    }
}


extension AddMoneytoWalletVC : CardDataVCDelegate
{
    func refreshCardData(card_number: String, card_exp_month: String, card_exp_year: String, card_cvc: String) {
//        let ItemPrice = formatter.string(for: self.txt_Amount.text!.replacingOccurrences(of: UD_currency, with: "").toDouble)

        let urlString = API_URL + "recharge"
        let params: NSDictionary = ["user_id":UserDefaults.standard.value(forKey: UD_userId) as! String,"recharge_amount":self.txt_Money.text!.replacingOccurrences(of: ",", with: ""),"payment_type":"4", "card_number":card_number,"card_exp_month":card_exp_month,"card_exp_year":card_exp_year,"card_cvc":card_cvc]
        self.Webservice_Addmoney(url: urlString, params: params)
    }
    
    
}
//MARK: Functions
extension AddMoneytoWalletVC : RazorpayPaymentCompletionProtocol,RazorpayProtocol {
    
    func showPaymentForm(){
        let options: [String:Any] = [
            "amount" : "\(Double(self.txt_Money.text!.replacingOccurrences(of: ",", with: ""))! * 100)", //mandatory in paise like:- 1000 paise ==  10 rs
            "currency": "INR",
            "description": "Wallet recharge",
            "image": "",
            "name": "e-Commerce user",
            "prefill": [
                "contact": UserDefaultManager.getStringFromUserDefaults(key: UD_userMobile),
                "email": UserDefaultManager.getStringFromUserDefaults(key: UD_userEmail)
            ],
            "theme": [
                "color": "#2291FF"
            ]
        ]
        razorpay?.open(options)
    }
    
    func onPaymentError(_ code: Int32, description str: String) {
        let alertController = UIAlertController(title: "", message: str, preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: "Okay".localiz(), style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        let urlString = API_URL + "recharge"
        let params: NSDictionary = ["user_id":UserDefaults.standard.value(forKey: UD_userId) as! String,"recharge_amount":self.txt_Money.text!.replacingOccurrences(of: ",", with: ""),"payment_id":payment_id,"payment_type":"3"]
        self.Webservice_Addmoney(url: urlString, params: params)
    }

}
extension AddMoneytoWalletVC : PaystackCardDataVCDelegate
{
    func refreshPaystackCardData(TokenID: String) {
        let urlString = API_URL + "recharge"
        let params: NSDictionary = ["user_id":UserDefaults.standard.value(forKey: UD_userId) as! String,"recharge_amount":self.txt_Money.text!.replacingOccurrences(of: ",", with: ""),"payment_id":TokenID,"payment_type":"6"]
        self.Webservice_Addmoney(url: urlString, params: params)
    }
    
    
    
}
extension String {

    // formatting text for currency textField
    func currencyInputFormatting() -> String {

        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
//        formatter.currencySymbol = UserDefaultManager.getStringFromUserDefaults(key: UD_currency)
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2

        var amountWithPrefix = self

        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.charactersArray.count), withTemplate: "")

        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: (double / 100))

        // if first number is 0 or all numbers were deleted
        guard number != 0 as NSNumber else {
            return ""
        }

        return formatter.string(from: number)!
    }
}
