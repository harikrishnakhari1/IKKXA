//
//  ChangePassswordVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 14/05/21.
//

import UIKit
import SwiftyJSON

class ChangePassswordVC: UIViewController {
    //MARK: Outlets
    @IBOutlet weak var btn_Reset: UIButton!
    @IBOutlet weak var txt_confirmPassword: UITextField!
    @IBOutlet weak var txt_NewPassword: UITextField!
    @IBOutlet weak var txt_oldPassword: UITextField!
    @IBOutlet weak var lbl_titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txt_NewPassword.placeholder = "New password".localiz()
        self.txt_oldPassword.placeholder = "Old password".localiz()
        self.txt_confirmPassword.placeholder = "Confirm password".localiz()
        
        self.lbl_titleLabel.text = "Change Password".localiz()
        self.btn_Reset.setTitle("Reset".localiz(), for: .normal)
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "en" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "N/A"
        {
            self.txt_confirmPassword.textAlignment = .left
            self.txt_NewPassword.textAlignment = .left
            self.txt_oldPassword.textAlignment = .left
        }
        else
        {
            self.txt_confirmPassword.textAlignment = .right
            self.txt_NewPassword.textAlignment = .right
            self.txt_oldPassword.textAlignment = .right
        }
    }

   
}

//MARK: Actions
extension ChangePassswordVC {
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
    @IBAction func btnTap_Reset(_ sender: UIButton) {
        if self.txt_oldPassword.text! == "" || self.txt_NewPassword.text == "" || self.txt_confirmPassword.text! == "" {
            showAlertMessage(titleStr: "", messageStr: "Please enter all details".localiz())
        }
        else if self.txt_NewPassword.text! != self.txt_confirmPassword.text! {
            showAlertMessage(titleStr: "", messageStr: "New password & Confirm password must be same".localiz())
        }
        else {
            let urlString = API_URL + "changepassword"
            let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                        "old_password":self.txt_oldPassword.text!,
                                        "new_password":self.txt_NewPassword.text!]
            self.Webservice_ChangePassword(url: urlString, params: params)
        }
    }
}

//MARK: Webservices
extension ChangePassswordVC
{
    func Webservice_ChangePassword(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                    self.dismiss(animated: false)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
