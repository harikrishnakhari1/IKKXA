//
//  ReferandEarnVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 25/02/22.
//

import UIKit

class ReferandEarnVC: UIViewController {

    @IBOutlet weak var lbl_refercode: UILabel!
    @IBOutlet weak var btn_Submit: UIButton!
    @IBOutlet weak var lbl_referstring: UILabel!
    
    var referralAmount = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        cornerRadius(viewName: self.btn_Submit, radius: 6.0)
        let ItemPrice = formatter.string(for: UserDefaultManager.getStringFromUserDefaults(key: UD_referral_amount).toDouble)
        self.referralAmount = ItemPrice!
        if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
        {
            lbl_referstring.text = "Share this code with a friend and you both could be eligible for \(self.referralAmount)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency)) bonus amount under our Referral Program."
        }
        else{
            lbl_referstring.text = "Share this code with a friend and you both could be eligible for\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(self.referralAmount) bonus amount under our Referral Program."
        }
        self.lbl_refercode.text = UserDefaultManager.getStringFromUserDefaults(key: UD_userReferCode)
    }
    
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
    @IBAction func btnTap_Submit(_ sender: UIButton) {
        let activityViewController = UIActivityViewController(activityItems: ["Use this code \(self.lbl_refercode.text!) to register with eCommerce App & get \(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(referralAmount) bonus amount"] , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }


}
