//
//  HelpContactusVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 14/05/21.
//

import UIKit
import SwiftyJSON

class HelpContactusVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var txt_firstName: UITextField!
    @IBOutlet weak var txt_lastName: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_subject: UITextField!
    
    @IBOutlet weak var txt_mobile: UITextField!
    @IBOutlet weak var textview_Message: UITextView!
    @IBOutlet weak var lbl_Email: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var lbl_Call: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_quickcontactus: UILabel!
    @IBOutlet weak var btn_Send: UIButton!
    var contactInfo = [String : JSON]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "Help & Contact Us".localiz()
        self.lbl_quickcontactus.text = "Quick Contact Us".localiz()
        self.btn_Send.setTitle("Send".localiz(), for: .normal)
        
        self.textview_Message.delegate = self
        self.textview_Message.text = "Message".localiz()
        self.textview_Message.textColor = UIColor.lightGray
        self.lbl_Call.text = self.contactInfo["contact"]!.stringValue
        self.lbl_Email.text = self.contactInfo["email"]!.stringValue
        self.lbl_Address.text = self.contactInfo["address"]!.stringValue
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Message"
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
    @IBAction func btnTap_Send(_ sender: UIButton) {
        
        if self.txt_firstName.text! == "" || self.txt_email.text! == "" || self.txt_lastName.text! == "" || self.txt_subject.text! == "" || self.txt_mobile.text! == "" || self.textview_Message.text == "Message" {
            showAlertMessage(titleStr: "", messageStr: "Please enter all details".localiz())
        }
        else if self.txt_email.text!.isEmail == false {
            showAlertMessage(titleStr: "", messageStr: "Please enter valid email".localiz())
        }
        else{
            let urlString = API_URL + "help"
            let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                        "first_name":self.txt_firstName.text!,
                                        "last_name":self.txt_lastName.text!,
                                        "mobile":self.txt_mobile.text!,
                                        "email":self.txt_email.text!,
                                        "subject":self.txt_subject.text!,
                                        "message":self.textview_Message.text!]
            self.Webservice_Help(url: urlString, params: params)
        }
    }
    
}
extension HelpContactusVC
{
    func Webservice_Help(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    self.dismiss(animated: false)
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
