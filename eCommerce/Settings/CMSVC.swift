//
//  CMSVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 30/11/21.
//

import UIKit
import SwiftyJSON

class CMSVC: UIViewController {
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var text_Description: UITextView!
    var setTitle = String()
    var isType = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.isType == "terms" {
            self.lbl_Title.text = "Terms & conditions".localiz()
          
        }
        else if self.isType == "privacy" {
            self.lbl_Title.text = "Privacy policy".localiz()
           
        }
        else if self.isType == "wepayment" {
            self.lbl_Title.text = "Payment Gateway"
           
        }
        else{
            self.isType = "About us".localiz()
            self.lbl_Title.text = "About us".localiz()
        }
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let urlString = API_URL + "cmspages"
        self.Webservice_Cmspages(url: urlString, params:[:])
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }

}
extension CMSVC
{
    func Webservice_Cmspages(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "GET", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    if self.isType == "privacy"
                    {
                        self.text_Description.text = jsonResponse!["privacypolicy"].stringValue
                    }
                    else if self.isType == "terms"{
                        self.text_Description.text = jsonResponse!["termsconditions"].stringValue
                    }
                    else{
                        self.text_Description.text = jsonResponse!["about"].stringValue
                    }
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
