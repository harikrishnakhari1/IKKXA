//
//  WebViewVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 24/04/21.
//

import UIKit
import WebKit
import MBProgressHUD
import SwiftyJSON

class WebViewVC: UIViewController,WKNavigationDelegate,WKUIDelegate {
    
    //MARK: Outlets
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var webView: WKWebView!
    
    //MARK: Variables
    var webUrl = String()
    var setTitle = String()
    var iswebview = String()
    
    // Added By Shoaib
    var grandTotal = String()
    var callBack: ((_ name: String)-> Void)?

    
    //MARK: Viewcontroller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.iswebview == "terms" {
            self.lbl_title.text = "Terms & conditions".localiz()
            let url = NSURL(string:TERMSCONDTIONS_URL)
            let request = URLRequest(url: url! as URL)
            webView.uiDelegate = self
            webView.navigationDelegate = self
            webView.allowsBackForwardNavigationGestures = true
            webView.load(request)
        }
        else if self.iswebview == "privacy" {
            self.lbl_title.text = "Privacy policy".localiz()
            let url = NSURL(string:PRIVACY_URL)
            let request = URLRequest(url: url! as URL)
            webView.uiDelegate = self
            webView.navigationDelegate = self
            webView.allowsBackForwardNavigationGestures = true
            webView.load(request)
        }
        else if self.iswebview == "wepayment" {
            self.lbl_title.text = "Payment Gateway".localiz()
            let url = NSURL(string:WEPAYMENT_URL + grandTotal)
            let request = URLRequest(url: url! as URL)
            webView.uiDelegate = self
            webView.navigationDelegate = self
            webView.allowsBackForwardNavigationGestures = true
            webView.load(request)
        }
        else {
            self.lbl_title.text = "About us".localiz()
            let url = NSURL(string:ABOUTUS_URL)
            let request = URLRequest(url: url! as URL)
            webView.uiDelegate = self
            webView.navigationDelegate = self
            webView.allowsBackForwardNavigationGestures = true
            webView.load(request)
        }
    }
}

//MARK: Functions
extension WebViewVC {
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
      

    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        print(#function)
        let xUrl = webView.url!
        let cUrl = xUrl.absoluteString
        print("didStartProvisionalNavigation - " + cUrl)

    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        MBProgressHUD.hide(for:self.view, animated: true)
        let xUrl = webView.url!
        let cUrl = xUrl.absoluteString
        print("didFinish - " + cUrl)
        
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
            // dismiss indicator
          
            // if url is not valid {
            //    decisionHandler(.cancel)
            // }
        let xUrl = webView.url!
        let cUrl = xUrl.absoluteString
         print("ccccc  decidePolicyFor - " + cUrl)
        
        let url = cUrl
        let queryItems = URLComponents(string: url)?.queryItems
        let param1 = queryItems?.filter({$0.name == "status"}).first
        let param2 = queryItems?.filter({$0.name == "message"}).first

        // status = failed/paid
        let status = param1?.value!
        let msg = param2?.value!
        
        if(status == "failed"){
            
            showAlertMessage(titleStr: "Payment Failed", messageStr: msg!)
            let vc1 = self.storyboard?.instantiateViewController(identifier: "PaymentVC") as! PaymentVC
            vc1.modalPresentationStyle = .fullScreen
            self.present(vc1, animated: true)
            
        }else if(status == "paid"){
            
            print("success hua bhai")
            callBack?("Paid")
            self.dismiss(animated: false)
    
        }

            decisionHandler(.allow)
        }
}

//MARK: Actions
extension WebViewVC {
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
}

