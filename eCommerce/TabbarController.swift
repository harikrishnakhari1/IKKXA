//
//  TabbarController.swift
//  eCommerce
//
//  Created by mac on 16/03/23.
//

import UIKit

protocol TabbarControllerDelegate: NSObject {
    func tabbarController(_ viewController: TabbarController, selected option: TabbarOption)
}

class TabbarController: UIViewController {

    weak var delegate: TabbarControllerDelegate?

    @IBOutlet var optionButtons:[UIButton]!
    @IBOutlet var optionButtonImages:[UIImageView]!
    @IBOutlet var homeLabel:UILabel!
    @IBOutlet var categoryLabel:UILabel!
    @IBOutlet var cartLabel:UILabel!
    @IBOutlet var favLabel:UILabel!
    @IBOutlet var profileLabel:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func tabbarOptionClicked(_ sender: UIButton) {
        guard let option = TabbarOption(rawValue: sender.tag) else { return }
        self.delegate?.tabbarController(self, selected: option)
    }
}
