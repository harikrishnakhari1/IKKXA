//
//  MyAccountVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 26/04/21.
//

import UIKit
import SDWebImage
import SwiftyJSON


class MyAccountVC: UIViewController {
    
    let kVersion        = "CFBundleShortVersionString"
    let kBuildNumber    = "CFBundleVersion"

    @IBOutlet weak var btn_Editprofile: UIButton!
    @IBOutlet weak var btn_Settings: UIButton!
    @IBOutlet weak var btn_Logout: UIButton!
    
    @IBOutlet weak var height_logout: NSLayoutConstraint!
    @IBOutlet weak var lbl_Email: UILabel!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var img_profile: UIImageView!
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var btn_help: UIButton!
    @IBOutlet weak var btn_About: UIButton!
    @IBOutlet weak var btn_privacy: UIButton!
    var contactInfo = [String : JSON]()
    var notification_status = String()
    @IBOutlet weak var lbl_app_version: UILabel!
    @IBOutlet weak var stackview: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "My Account".localiz()
        self.btn_Settings.setTitle("Settings".localiz(), for: .normal)
        self.btn_privacy.setTitle("Privacy policy".localiz(), for: .normal)
        self.btn_About.setTitle("About us".localiz(), for: .normal)
        self.btn_help.setTitle("Help & Contact Us".localiz(), for: .normal)
        self.btn_Logout.setTitle("Logout".localiz(), for: .normal)
        
        cornerRadius(viewName: self.img_profile, radius: self.img_profile.frame.height / 2)
        if UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "en" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "N/A" {
            self.btn_Editprofile.titleLabel!.textAlignment = .left
            self.btn_Settings.titleLabel!.textAlignment = .left
            self.btn_help.titleLabel!.textAlignment = .left
            self.btn_About.titleLabel!.textAlignment = .left
            self.btn_privacy.titleLabel!.textAlignment = .left
        }
        else {
            self.btn_Editprofile.titleLabel!.textAlignment = .right
            self.btn_help.titleLabel!.textAlignment = .right
            self.btn_About.titleLabel!.textAlignment = .right
            self.btn_privacy.titleLabel!.textAlignment = .right
            self.btn_Settings.titleLabel!.textAlignment = .right
        }
        
        lbl_app_version.text = "App version \(getVersion())"
        print("app version \(getVersion())")

    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A"
        {
            self.btn_Logout.isHidden = true
            self.height_logout.constant = 0.0
            self.lbl_userName.text = "Guest User"
            self.lbl_Email.text = ""
            self.stackview.isHidden = true
        }
        else{
            self.btn_Logout.isHidden = false
            self.height_logout.constant = 50.0
            self.stackview.isHidden = false
            let urlString = API_URL + "getprofile"
            let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
            self.Webservice_GetProfile(url: urlString, params: params)
        }
        
    }
    @IBAction func btnTap_Fb(_ sender: UIButton) {
        guard let url = URL(string: contactInfo["facebook"]!.stringValue) else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func btnTap_insta(_ sender: UIButton) {
        guard let url = URL(string: contactInfo["instagram"]!.stringValue) else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func btnTap_linkdian(_ sender: UIButton) {
        guard let url = URL(string: contactInfo["linkedin"]!.stringValue) else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func btnTap_twiter(_ sender: UIButton) {
        guard let url = URL(string: contactInfo["twitter"]!.stringValue) else { return }
        UIApplication.shared.open(url)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case is TabbarController:
            let tabbarController = segue.destination as? TabbarController
            tabbarController?.delegate = self
            
        default:
            break
        }
    }
    
}
// MARK:- Button Action
extension MyAccountVC
{
    @IBAction func btnTap_Privacy(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "CMSVC") as! CMSVC
        vc.isType = "privacy"
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    
    @IBAction func btnTap_Help(_ sender: UIButton) {
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
            appNavigation.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.windows[0].rootViewController = appNavigation
        }
        else {
            let vc = self.storyboard?.instantiateViewController(identifier: "HelpContactusVC") as! HelpContactusVC
            vc.contactInfo = self.contactInfo
            vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
        }
    }
    
    @IBAction func btnTap_Aboutus(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "CMSVC") as! CMSVC
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    
    @IBAction func btnTap_EditProfile(_ sender: UIButton) {
        
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
            appNavigation.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.windows[0].rootViewController = appNavigation
        }
        else {
            let vc = self.storyboard?.instantiateViewController(identifier: "EditProfileVC") as! EditProfileVC
            vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
        }
        
    }
    @IBAction func btnTap_Settings(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "SettingsVC") as! SettingsVC
        vc.isnotification = self.notification_status
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    @IBAction func btnTap_Logout(_ sender: UIButton) {
        let alertVC = UIAlertController(title: "", message: "Are you sure to logout from this app?".localiz(), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes".localiz(), style: .default) { (action) in
            UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
            UserDefaults.standard.set("", forKey:UD_userId)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            UIApplication.shared.windows[0].rootViewController = nav
        }
        let noAction = UIAlertAction(title: "No".localiz(), style: .destructive)
        alertVC.addAction(noAction)
        alertVC.addAction(yesAction)
        self.present(alertVC,animated: true,completion: nil)
    }
}
//MARK: Webservices
extension MyAccountVC {
    func Webservice_GetProfile(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let responseData = jsonResponse!["data"].dictionaryValue
//                    self.img_Profile.sd_setImage(with: URL(string: responseData["profile_pic"]!.stringValue), placeholderImage: UIImage(named: "ic_placeholder"))
//                    self.txt_Name.text = responseData["name"]?.stringValue
//                    self.txt_Email.text = responseData["email"]?.stringValue
//                    self.txt_Mobile.text = responseData["mobile"]?.stringValue
                    
                    self.contactInfo = jsonResponse!["contactinfo"].dictionaryValue
                    
                    self.lbl_userName.text = responseData["name"]?.stringValue
                    self.lbl_Email.text = responseData["email"]?.stringValue
                    self.img_profile.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    self.img_profile.sd_setImage(with: URL(string: responseData["profile_pic"]!.stringValue), placeholderImage: UIImage(named: "ic_placeholder"))
                   // self.notification_status = responseData["notification_status"]!.stringValue
                    
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
extension MyAccountVC: TabbarControllerDelegate
{
    func tabbarController(_ viewController: TabbarController, selected option: TabbarOption) {
        switch option {
        case .home:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.homeViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .category:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.categoryViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .cart:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.cartViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .wishlist:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.wishlistViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .profile:
            break
        }
    }
    
}
