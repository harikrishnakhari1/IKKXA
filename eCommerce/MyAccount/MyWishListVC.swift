//
//  MyWishListVC.swift
//  eCommerce
//
//  Created by DREAMWORLD on 07/08/21.
//

import UIKit
import SwiftyJSON
import SDWebImage

class MyWishListVC: UIViewController {

    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var CollectionView_WishList: UICollectionView!
    var WishListArray = [[String:String]]()
    var pageIndex = 1
    var lastIndex = 0
    var product_id = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = "My WishList".localiz()
    }
    
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
            appNavigation.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.windows[0].rootViewController = appNavigation
        }
        else {
            self.pageIndex = 1
            self.lastIndex = 0
            let urlString = API_URL + "getwishlist?page=\(self.pageIndex)"
            let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
            self.Webservice_MyWishitemList(url: urlString,params: params)
        }
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case is TabbarController:
            let tabbarController = segue.destination as? TabbarController
            tabbarController?.delegate = self
            
        default:
            break
        }
    }
    
}
extension MyWishListVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        
        let rect = CGRect(x: 0, y: 0, width: 10, height: 10)
        let imagedata = UIImageView(frame: rect)

        imagedata.contentMode = .scaleAspectFit
        self.CollectionView_WishList.backgroundView = imagedata
        if self.WishListArray.count == 0 {
            imagedata.image = UIImage(named: "ic_like")
        }
        else {
            imagedata.image = UIImage(named: "")
        }
        return WishListArray.count
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.CollectionView_WishList.dequeueReusableCell(withReuseIdentifier: "MostpopularCell", for: indexPath) as! MostpopularCell
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.cornerRadius = 8.0
        cell.layer.borderWidth = 0.4
        let data = self.WishListArray[indexPath.item]
        let is_variation = data["is_variation"]!
        let is_wishlist = data["is_wishlist"]!
        if is_wishlist == "1"
        {
            cell.btn_favorite.setImage(UIImage(named: "ic_heartfill"), for: .normal)
            cell.btn_favorite.tintColor = UIColor.red
            
        }
        else{
            cell.btn_favorite.setImage(UIImage(named: "ic_heart"), for: .normal)
            cell.btn_favorite.tintColor = UIColor.darkGray
        }
        if is_variation == "1"
        {
            let ProductPrice = formatter.string(for: data["variation_price"]!.toDouble)
            let ProductDiscountPrice = formatter.string(for: data["discounted_variation_price"]!.toDouble)
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                cell.lbl_itemprice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                cell.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
            }
            else {
                cell.lbl_itemprice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                cell.lbl_DiscountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
            }
            
        }
        else{
            let ProductPrice = formatter.string(for: data["product_price"]!.toDouble)
            let ProductDiscountPrice = formatter.string(for: data["discounted_price"]!.toDouble)
            if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
            {
                cell.lbl_itemprice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                cell.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
            }
            else{
                cell.lbl_itemprice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                cell.lbl_DiscountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
            }
        }
        cell.lbl_itemName.text = data["product_name"]!
        cell.img_item.sd_imageTransition = .fade
        let items = indexPath.item % 6
        cell.img_item.sd_setImage(with: URL(string: data["image_url"]!), placeholderImage: UIImage(named: "b_0\(items)"))
        cell.btn_favorite.tag = indexPath.row
        cell.btn_favorite.addTarget(self, action:#selector(btnTap_Favorites), for: .touchUpInside)
        cell.lbl_ratingcount.text = data["arg_ratting"]!
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width - 30.0) / 2, height: ((UIScreen.main.bounds.width - 30.0) / 2) * 1.90)
    }
   
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == self.WishListArray.count - 1 {
            if self.pageIndex != self.lastIndex {
                self.pageIndex = self.pageIndex + 1
                if self.WishListArray.count != 0 {
                    let urlString = API_URL + "getwishlist?page=\(self.pageIndex)"
                    let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                    self.Webservice_MyWishitemList(url: urlString,params: params)
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = self.WishListArray[indexPath.item]
        let vc = storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
        vc.item_id = data["id"]!
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    
    @objc func btnTap_Favorites(sender:UIButton!) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A"
        {
            UserDefaults.standard.set("", forKey:UD_userId)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            UIApplication.shared.windows[0].rootViewController = nav
        }
        else
        {
            
            let data = self.WishListArray[sender.tag]
            self.product_id = data["id"]!
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConformationAlertVC") as! ConformationAlertVC
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.deleget = self
            vc.isSelectedTypes = "2"
            self.present(vc,animated: true,completion: nil)
            
            
        }
    }
    
}
extension MyWishListVC
{
    func Webservice_MyWishitemList(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let StoreData = jsonResponse!["data"].dictionaryValue
                    if self.pageIndex == 1 {
                        self.lastIndex = Int(StoreData["last_page"]!.stringValue)!
                        self.WishListArray.removeAll()
                    }
                    let StoreListData = StoreData["data"]!.arrayValue
                    for data in StoreListData
                    {
                        let imageUrl = data["productimage"].dictionaryValue
                        let variation = data["variation"].dictionaryValue
                        let rattings = data["rattings"].arrayValue
                        var arg_rattings = String()
                        
                        if rattings.count == 0
                        {
                            arg_rattings = "0.0"
                        }
                        else{
                            let ProductRatting = formatterRatting.string(for: rattings[0]["avg_ratting"].stringValue.toDouble)
                            arg_rattings = ProductRatting!
                        }
                        var StoreListObj = [String:String]()
                        if variation.count != 0
                        {
                            StoreListObj = ["id":data["id"].stringValue,"product_name":data["product_name"].stringValue,"product_price":data["product_price"].stringValue,"discounted_price":data["discounted_price"].stringValue,"is_variation":data["is_variation"].stringValue,"is_wishlist":data["is_wishlist"].stringValue,"image_url":imageUrl["image_url"]!.stringValue,"variation_id":variation["id"]!.stringValue,"variation_price":variation["price"]!.stringValue,"discounted_variation_price":variation["discounted_variation_price"]!.stringValue,"variation":variation["variation"]!.stringValue,"variation_qty":variation["qty"]!.stringValue,"arg_ratting":arg_rattings]
                        }
                        else{
                            StoreListObj = ["id":data["id"].stringValue,"product_name":data["product_name"].stringValue,"product_price":data["product_price"].stringValue,"discounted_price":data["discounted_price"].stringValue,"is_variation":data["is_variation"].stringValue,"is_wishlist":data["is_wishlist"].stringValue,"image_url":imageUrl["image_url"]!.stringValue,"variation_id":"","variation_price":"","discounted_variation_price":"","variation":"","variation_qty":"","arg_ratting":"0.0"]
                            
                        }
                        self.WishListArray.append(StoreListObj)
                    }
                    
                    self.CollectionView_WishList.delegate = self
                    self.CollectionView_WishList.dataSource = self
                    self.CollectionView_WishList.reloadData()
                }
                else if responseCode == "0" {
                    self.CollectionView_WishList.delegate = self
                    self.CollectionView_WishList.dataSource = self
                    self.CollectionView_WishList.reloadData()
                }
                else
                {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_RemoveWishList(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    
//                    var data = self.WishListArray[sender]
//                    data["is_wishlist"]! = "0"
//                    self.WishListArray.remove(at: sender)
//                    self.WishListArray.insert(data, at: sender)
//                    self.CollectionView_WishList.reloadData()
                    self.pageIndex = 1
                    self.lastIndex = 0
                    let urlString = API_URL + "getwishlist?page=\(self.pageIndex)"
                    let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                    self.Webservice_MyWishitemList(url: urlString,params: params)
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
extension MyWishListVC : DismissAlertDeleget
{
    func DimissSucess() {
        
        let urlString = API_URL + "removefromwishlist"
        let params: NSDictionary = ["product_id":self.product_id,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
        self.Webservice_RemoveWishList(url: urlString,params: params)
    }
    
    
}
extension MyWishListVC: TabbarControllerDelegate
{
    func tabbarController(_ viewController: TabbarController, selected option: TabbarOption) {
        switch option {
        case .home:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.homeViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .category:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.categoryViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .cart:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.cartViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .wishlist:
            break
        case .profile:
            if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A" {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.logInViewController)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false)
            } else {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.profileViewController)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false)
            }
        }
    }
    
}
