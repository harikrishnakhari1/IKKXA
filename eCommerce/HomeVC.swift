//
//  HomeVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 24/04/21.
//

import UIKit
import SwiftyJSON
import SDWebImage
import LanguageManager_iOS

class BannerCell : UICollectionViewCell
{
    @IBOutlet weak var img_banner: UIImageView!
}
class CategoriesCell : UICollectionViewCell
{
    @IBOutlet weak var img_Category: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var cell_view: CornerView!
}
class VendorsCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var img_vendor: UIImageView!
    
    @IBOutlet weak var cell_view: CornerView!
}
class MostpopularCell : UICollectionViewCell
{
    @IBOutlet weak var lbl_DiscountPrice: UILabel!
    @IBOutlet weak var lbl_ratingcount: UILabel!
    @IBOutlet weak var lbl_itemprice: UILabel!
    @IBOutlet weak var lbl_itemName: UILabel!
    @IBOutlet weak var btn_favorite: UIButton!
    @IBOutlet weak var img_item: UIImageView!
    @IBOutlet weak var cell_view: UIView!
}
class HomeVC: UIViewController {
    
    @IBOutlet weak var CollectionView_Banner: UICollectionView!
    @IBOutlet weak var CollectionView_Banner2: UICollectionView!
    @IBOutlet weak var CollectionView_Banner3: UICollectionView!
    @IBOutlet weak var CollectionView_Categories: UICollectionView!
    @IBOutlet weak var CollectionView_Mostpopular: UICollectionView!
    @IBOutlet weak var CollectionView_TrendingNow: UICollectionView!
    @IBOutlet weak var CollectionView_VendorsList: UICollectionView!
    @IBOutlet weak var CollectionView_SubCategories: UICollectionView!
    
    @IBOutlet weak var CollectionView_Brand: UICollectionView!
    @IBOutlet weak var CollectionView_BrandMostPopular: UICollectionView!
    @IBOutlet weak var img_CenterBanner: UIImageView!
    @IBOutlet weak var img_LargeBanner: UIImageView!
    @IBOutlet weak var img_Language: UIImageView!
    @IBOutlet weak var img_Reddot: UIImageView!
    
    @IBOutlet weak var CollectionView01_Height: NSLayoutConstraint!
    @IBOutlet weak var CollectionView02_Height: NSLayoutConstraint!
    @IBOutlet weak var CollectionView03_Height: NSLayoutConstraint!
    
    @IBOutlet weak var lblstr_title: UILabel!
    @IBOutlet weak var lblstr_Categories: UILabel!
    @IBOutlet weak var lblstr_Featured: UILabel!
    @IBOutlet weak var lblstr_Vendors: UILabel!
    @IBOutlet weak var lblstr_NewArrivals: UILabel!
    @IBOutlet weak var lblstr_Brand: UILabel!
    @IBOutlet weak var lblstr_HotDeals: UILabel!
    
    
    @IBOutlet weak var categoryToSpace: NSLayoutConstraint!
    @IBOutlet weak var hotdeals_viewAll: UIButton!
    @IBOutlet weak var brand_viewAll: UIButton!
    @IBOutlet weak var new_arrival_viewAll: UIButton!
    @IBOutlet weak var vendor_viewAll: UIButton!
    @IBOutlet weak var feature_btn_ViewAll: UIButton!
    @IBOutlet weak var btn_viewAll: UIButton!
    // MARK:- Veriable
    var TopBannerListArray = [JSON]()
    var CenterBannerListArray = [JSON]()
    var MainCategoryArray = [[String:String]]()
    var timer: Timer?
    var FeaturedProductsArray = [JSON]()
    var HotProductsArray = [JSON]()
    var NewproductsArray = [JSON]()
    var VendorsArray = [JSON]()
    var BrandsArray = [JSON]()
    var leftbanner = [JSON]()
    var largeBanner = [JSON]()
    var SilderBanner = [JSON]()
    var cat_id = String()
    var sections = [Section]()
    var selectedSection = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.lblstr_title.text = "E-Commerce".localiz()
        self.lblstr_title.text = "IKKXA".localiz()
        self.lblstr_Featured.text = "Featured Products".localiz()
        self.lblstr_Vendors.text = "Vendors".localiz()
        self.lblstr_NewArrivals.text = "New Arrivals".localiz()
        self.lblstr_Brand.text = "Brand".localiz()
        self.lblstr_HotDeals.text = "Hot Deals".localiz()
        self.feature_btn_ViewAll.setTitle("View All".localiz(), for: .normal)
        self.vendor_viewAll.setTitle("View All".localiz(), for: .normal)
        self.new_arrival_viewAll.setTitle("View All".localiz(), for: .normal)
        
        self.brand_viewAll.setTitle("View All".localiz(), for: .normal)
        self.hotdeals_viewAll.setTitle("View All".localiz(), for: .normal)
        
        self.img_Reddot.isHidden = true
        cornerRadius(viewName: self.img_CenterBanner, radius: 6.0)
        self.startTimer()
        // MARK:- Top Banner Api
        let urlString = API_URL + "banner"
        let selectedLanguage = Languages(rawValue: UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng))
        self.img_Language.image = UIImage(named: selectedLanguage == .ar ? "English" : "Arabic")
        self.Webservice_GetTopBanner(url: urlString,params: [:])
        
    }
    
    
    @IBAction func btnTap_Squarebanner(_ sender: UIButton) {
        if self.leftbanner[0]["type"].stringValue == "category"
        {
            let vc = self.storyboard?.instantiateViewController(identifier: "SubCategoryVC") as! SubCategoryVC
            vc.cat_id = self.leftbanner[0]["cat_id"].stringValue
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        }
        else{
            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
            vc.item_id = self.leftbanner[0]["product_id"].stringValue
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        }
    }
    @IBAction func btnTap_LargeBanner(_ sender: UIButton) {
        if self.largeBanner[0]["type"].stringValue == "category"
        {
            let vc = self.storyboard?.instantiateViewController(identifier: "SubCategoryVC") as! SubCategoryVC
            vc.cat_id = self.largeBanner[0]["cat_id"].stringValue
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        }
        else{
            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
            vc.item_id = self.largeBanner[0]["product_id"].stringValue
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case is TabbarController:
            let tabbarController = segue.destination as? TabbarController
            tabbarController?.delegate = self
            
        default:
            break
        }
    }
}
// MARK:- Button Actions
extension HomeVC
{
    @IBAction func btnTap_ViewAllCategory(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ViewAllCategoryVC") as! ViewAllCategoryVC
        vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
    }
    @IBAction func btnTap_FeaturedProduct(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ViewallitemVC") as! ViewallitemVC
        vc.istype = "featured_products"
        vc.navtitle = "Featured Products".localiz()
        vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
    }
    @IBAction func btnTap_NewArrivals(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ViewallitemVC") as! ViewallitemVC
        vc.istype = "new_products"
        vc.navtitle = "New Arrivals".localiz()
        vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
    }
    @IBAction func btnTap_HotDeals(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ViewallitemVC") as! ViewallitemVC
        vc.istype = "hot_products"
        vc.navtitle = "Hot Deals".localiz()
        vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
    }
    @IBAction func btnTap_ViewallVendor(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ViewAllStoreVC") as! ViewAllStoreVC
        vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
    }
    @IBAction func btnTap_ViewallBrands(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ViewAllBrandVC") as! ViewAllBrandVC
        vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
    }
    
    @IBAction func btnTap_Search(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "SearchVC") as! SearchVC
        vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
    }
    @IBAction func btnTap_Notifications(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "NotificationsVC") as! NotificationsVC
        vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
    }
    @IBAction func btnTap_Language(_ sender: UIButton) {
        let selectedLanguage: Languages = UserDefaultManager.getStringFromUserDefaults(key: UD_isSelectLng) == "ar" ? .en : .ar
        UserDefaultManager.setStringToUserDefaults(value: selectedLanguage.rawValue, key: UD_isSelectLng)
        LanguageManager.shared.setLanguage(language: selectedLanguage, viewControllerFactory: { title -> UIViewController in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            return objVC
        }) { view in
            view.transform = CGAffineTransform(scaleX: 2, y: 2)
            view.alpha = 0
        }
    }
    
}
// MARK:- Functions
extension HomeVC: TabbarControllerDelegate
{
    func tabbarController(_ viewController: TabbarController, selected option: TabbarOption) {
        switch option {
        case .home:
            break
        case .category:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.categoryViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .cart:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.cartViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .wishlist:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.wishlistViewController)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        case .profile:
            if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A" {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.logInViewController)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false)
            } else {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Identifiers.profileViewController)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false)
            }
        }
    }
    
    func startTimer() {
        Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true);
        Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(scrollToNextBannerCell), userInfo: nil, repeats: true);
        Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(scrollToNextBannerCell2), userInfo: nil, repeats: true);
    }
    @objc func scrollToNextCell() {
        //get cell size
        let cellSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        //get current content Offset of the Collection view
        let contentOffset = CollectionView_Banner.contentOffset
        
        if CollectionView_Banner.contentSize.width <= CollectionView_Banner.contentOffset.x + cellSize.width
        {
            CollectionView_Banner.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
            
        } else {
            CollectionView_Banner.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
        }
    }
    @objc func scrollToNextBannerCell() {
        //get cell size
        let cellSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        //get current content Offset of the Collection view
        let contentOffset = CollectionView_Banner2.contentOffset
        
        if CollectionView_Banner2.contentSize.width <= CollectionView_Banner2.contentOffset.x + cellSize.width
        {
            CollectionView_Banner2.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
            
        } else {
            CollectionView_Banner2.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
        }
    }
    @objc func scrollToNextBannerCell2() {
        //get cell size
        let cellSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        //get current content Offset of the Collection view
        let contentOffset = CollectionView_Banner3.contentOffset
        
        if CollectionView_Banner3.contentSize.width <= CollectionView_Banner3.contentOffset.x + cellSize.width
        {
            CollectionView_Banner3.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
            
        } else {
            CollectionView_Banner3.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
        }
    }
}
// MARK:- CollectionView Deleget methods

extension HomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.CollectionView_Banner
        {
            let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.CollectionView_Banner.bounds.size.width, height: self.CollectionView_Banner.bounds.size.height))
            let messageLabel = UILabel(frame: rect)
            messageLabel.textColor = UIColor.lightGray
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            messageLabel.sizeToFit()
            self.CollectionView_Banner.backgroundView = messageLabel;
            if self.SilderBanner.count == 0 {
                // messageLabel.text = "No data found".localiz()
                //self.CollectionView_Banner.backgroundColor = UIColor.blue
                self.categoryToSpace.constant = 5
                
            }
            else {
                messageLabel.text = ""
            }
            return SilderBanner.count
        }
        else if collectionView == self.CollectionView_Categories
        {
            let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.CollectionView_Categories.bounds.size.width, height: self.CollectionView_Categories.bounds.size.height))
            return MainCategoryArray.count
        }
        else if collectionView == self.CollectionView_SubCategories
        {
            let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.CollectionView_SubCategories.bounds.size.width, height: self.CollectionView_SubCategories.bounds.size.height))
            return sections.count
        }
        else if collectionView == self.CollectionView_Banner2
        {
            let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.CollectionView_Banner2.bounds.size.width, height: self.CollectionView_Banner2.bounds.size.height))
            let messageLabel = UILabel(frame: rect)
            messageLabel.textColor = UIColor.lightGray
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            messageLabel.sizeToFit()
            self.CollectionView_Banner2.backgroundView = messageLabel
            if self.TopBannerListArray.count == 0 {
                messageLabel.text = "No data found".localiz()
            }
            else {
                messageLabel.text = ""
            }
            return TopBannerListArray.count
        }
        else if collectionView == self.CollectionView_Banner3
        {
            let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.CollectionView_Banner3.bounds.size.width, height: self.CollectionView_Banner3.bounds.size.height))
            let messageLabel = UILabel(frame: rect)
            messageLabel.textColor = UIColor.lightGray
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            messageLabel.sizeToFit()
            self.CollectionView_Banner3.backgroundView = messageLabel
            if self.CenterBannerListArray.count == 0 {
                messageLabel.text = "No data found".localiz()
            }
            else {
                messageLabel.text = ""
            }
            return CenterBannerListArray.count
        }
        else if collectionView == self.CollectionView_VendorsList
        {
            let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.CollectionView_VendorsList.bounds.size.width, height: self.CollectionView_VendorsList.bounds.size.height))
            let messageLabel = UILabel(frame: rect)
            messageLabel.textColor = UIColor.lightGray
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            messageLabel.sizeToFit()
            self.CollectionView_VendorsList.backgroundView = messageLabel
            if self.VendorsArray.count == 0 {
                messageLabel.text = "No data found".localiz()
            }
            else {
                messageLabel.text = ""
            }
            return VendorsArray.count
        }
        else if collectionView == self.CollectionView_Mostpopular{
            
            let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.CollectionView_Mostpopular.bounds.size.width, height: self.CollectionView_Mostpopular.bounds.size.height))
            let messageLabel = UILabel(frame: rect)
            messageLabel.textColor = UIColor.lightGray
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            messageLabel.sizeToFit()
            self.CollectionView_Mostpopular.backgroundView = messageLabel;
            if self.FeaturedProductsArray.count == 0 {
                messageLabel.text = "No data found".localiz()
            }
            else {
                messageLabel.text = ""
            }
            return FeaturedProductsArray.count
        }
        else if collectionView == self.CollectionView_TrendingNow{
            let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.CollectionView_TrendingNow.bounds.size.width, height: self.CollectionView_TrendingNow.bounds.size.height))
            let messageLabel = UILabel(frame: rect)
            messageLabel.textColor = UIColor.lightGray
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            messageLabel.sizeToFit()
            self.CollectionView_TrendingNow.backgroundView = messageLabel
            if self.NewproductsArray.count == 0 {
                messageLabel.text = "No data found".localiz()
            }
            else {
                messageLabel.text = ""
            }
            return NewproductsArray.count
        }
        else if collectionView == self.CollectionView_Brand{
            let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.CollectionView_Brand.bounds.size.width, height: self.CollectionView_Brand.bounds.size.height))
            let messageLabel = UILabel(frame: rect)
            messageLabel.textColor = UIColor.lightGray
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            messageLabel.sizeToFit()
            self.CollectionView_Brand.backgroundView = messageLabel
            if self.BrandsArray.count == 0 {
                messageLabel.text = "No data found".localiz()
            }
            else {
                messageLabel.text = ""
            }
            return BrandsArray.count
        }
        else if collectionView == self.CollectionView_BrandMostPopular{
            let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.CollectionView_BrandMostPopular.bounds.size.width, height: self.CollectionView_BrandMostPopular.bounds.size.height))
            let messageLabel = UILabel(frame: rect)
            messageLabel.textColor = UIColor.lightGray
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            messageLabel.sizeToFit()
            self.CollectionView_BrandMostPopular.backgroundView = messageLabel
            if self.HotProductsArray.count == 0 {
                messageLabel.text = "No data found".localiz()
            }
            else {
                messageLabel.text = ""
            }
            return HotProductsArray.count
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.CollectionView_Banner
        {
            let cell = self.CollectionView_Banner.dequeueReusableCell(withReuseIdentifier: "BannerCell", for: indexPath) as! BannerCell
            cornerRadius(viewName: cell.img_banner, radius: 6.0)
            let data = self.SilderBanner[indexPath.item]
            cell.img_banner.sd_imageTransition = .fade
            let items = indexPath.item % 6
            cell.img_banner.sd_setImage(with: URL(string: data["image_url"].stringValue), placeholderImage: UIImage(named: "b_0\(items)"))
            cell.heightAnchor
            
            return cell
        }
        else if collectionView == self.CollectionView_Categories
        {
            let cell = self.CollectionView_Categories.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
            DispatchQueue.main.async {
                cornerRadius(viewName: cell.img_Category, radius: 35)
            }
            let data = self.MainCategoryArray[indexPath.row]
            cell.lbl_name.text! = data["innersubcategory_name"] ?? ""
//            cell.img_Category.sd_imageTransition = .fade
//            let items = indexPath.item % 6
            cell.img_Category.sd_setImage(with: URL(string: data["innersubcategoryImage"] ?? ""))
        
            return cell
        }
        else if collectionView == self.CollectionView_SubCategories
        {
            let cell = self.CollectionView_SubCategories.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
            let data = self.sections[indexPath.item]
            cell.lbl_name.text! = data.category
//            cell.img_Category.sd_imageTransition = .fade
//            let items = indexPath.item % 6
            cell.img_Category.sd_setImage(with: URL(string: data.categoryImage))
        
            return cell
        }
        else if collectionView == self.CollectionView_Mostpopular{
            
            let cell = self.CollectionView_Mostpopular.dequeueReusableCell(withReuseIdentifier: "MostpopularCell", for: indexPath) as! MostpopularCell
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.cornerRadius = 8.0
            cell.layer.borderWidth = 0.4
            let data = self.FeaturedProductsArray[indexPath.item]
            let productimage = data["productimage"].dictionaryValue
            let variation = data["variation"].dictionaryValue
            cell.lbl_itemName.text = data["product_name"].stringValue
            cell.img_item.sd_imageTransition = .fade
            let items = indexPath.item % 6
            cell.img_item.sd_setImage(with: URL(string: productimage["image_url"]!.stringValue), placeholderImage: UIImage(named: "b_0\(items)"))
            let is_variation = data["is_variation"].stringValue
            let is_wishlist = data["is_wishlist"].stringValue
            let avg_ratting = data["rattings"].arrayValue
            if avg_ratting.count != 0
            {
                let ProductRatting = formatterRatting.string(for: avg_ratting[0]["avg_ratting"].stringValue.toDouble)
                cell.lbl_ratingcount.text = "\(ProductRatting!)"
            }
            else{
                cell.lbl_ratingcount.text = "0.0"
            }
            if is_wishlist == "1"
            {
                cell.btn_favorite.setImage(UIImage(named: "ic_heartfill"), for: .normal)
                cell.btn_favorite.tintColor = UIColor.red
                
            }
            else{
                cell.btn_favorite.setImage(UIImage(named: "ic_heart"), for: .normal)
                cell.btn_favorite.tintColor = UIColor.darkGray
            }
            if is_variation == "1"
            {
                let ProductPrice = formatter.string(for: variation["price"]!.stringValue.toDouble)
                let ProductDiscountPrice = formatter.string(for: variation["discounted_variation_price"]!.stringValue.toDouble)
                
                /*************
                 By Shoaib 05 Sep 2022
                 If product price and discount price same hide discount value
                 **/
                
                if(ProductPrice == ProductDiscountPrice){
                    cell.lbl_itemprice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    cell.lbl_DiscountPrice.isHidden = true
                }
                
                if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                {
                    cell.lbl_itemprice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    cell.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
                    
                }
                else {
                    cell.lbl_itemprice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    cell.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
                }
                
            }
            else{
                let ProductPrice = formatter.string(for: data["product_price"].stringValue.toDouble)
                let ProductDiscountPrice = formatter.string(for: data["discounted_price"].stringValue.toDouble)
                if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                {
                    cell.lbl_itemprice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    cell.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
                }
                else{
                    cell.lbl_itemprice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                    cell.lbl_DiscountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
                }
                
            }
            cell.btn_favorite.tag = indexPath.row
            cell.btn_favorite.addTarget(self, action:#selector(btnTap_Favorites_Mostpopular), for: .touchUpInside)
            return cell
        }
        else if collectionView == self.CollectionView_Banner2
        {
            let cell = self.CollectionView_Banner2.dequeueReusableCell(withReuseIdentifier: "BannerCell", for: indexPath) as! BannerCell
            cornerRadius(viewName: cell.img_banner, radius: 6.0)
            let data = self.TopBannerListArray[indexPath.item]
            cell.img_banner.sd_imageTransition = .fade
            let items = indexPath.item % 6
            cell.img_banner.sd_setImage(with: URL(string: data["image_url"].stringValue), placeholderImage: UIImage(named: "b_0\(items)"))
            
            return cell
        }
        else if collectionView == self.CollectionView_Banner3
        {
            let cell = self.CollectionView_Banner2.dequeueReusableCell(withReuseIdentifier: "BannerCell", for: indexPath) as! BannerCell
            cornerRadius(viewName: cell.img_banner, radius: 6.0)
            let data = self.CenterBannerListArray[indexPath.item]
            cell.img_banner.sd_imageTransition = .fade
            let items = indexPath.item % 6
            cell.img_banner.sd_setImage(with: URL(string: data["image_url"].stringValue), placeholderImage: UIImage(named: "b_0\(items)"))
            
            return cell
        }
        else if collectionView == self.CollectionView_VendorsList{
            let cell = self.CollectionView_VendorsList.dequeueReusableCell(withReuseIdentifier: "VendorsCell", for: indexPath) as! VendorsCell
            cornerRadius(viewName: cell.img_vendor, radius: 8.0)
            let data = self.VendorsArray[indexPath.item]
            cell.lbl_name.text! = data["name"].stringValue
            cell.img_vendor.sd_imageTransition = .fade
            let items = indexPath.item % 6
            cell.img_vendor.sd_setImage(with: URL(string: data["image_url"].stringValue), placeholderImage: UIImage(named: "b_0\(items)"))
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.cornerRadius = 8.0
            cell.layer.borderWidth = 0.4
            
            cell.cell_view.backgroundColor = UIColor.clear
            
            //            if items == 0 {
            //                cell.cell_view.backgroundColor = APP_COLOR1
            //            }
            //            else if items == 1 {
            //                cell.cell_view.backgroundColor = APP_COLOR2
            //            }
            //            else if items == 2 {
            //                cell.cell_view.backgroundColor = APP_COLOR3
            //            }
            //            else if items == 3 {
            //                cell.cell_view.backgroundColor = APP_COLOR4
            //            }
            //            else if items == 4 {
            //                cell.cell_view.backgroundColor = APP_COLOR5
            //            }
            //            else if items == 5 {
            //                cell.cell_view.backgroundColor = APP_COLOR6
            //            }
            
            return cell
        }
        else if collectionView == self.CollectionView_TrendingNow{
            let cell = self.CollectionView_TrendingNow.dequeueReusableCell(withReuseIdentifier: "MostpopularCell", for: indexPath) as! MostpopularCell
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.cornerRadius = 8.0
            cell.layer.borderWidth = 0.4
            let data = self.NewproductsArray[indexPath.item]
            let productimage = data["productimage"].dictionaryValue
            let variation = data["variation"].dictionaryValue
            let is_variation = data["is_variation"].stringValue
            let is_wishlist = data["is_wishlist"].stringValue
            let avg_ratting = data["rattings"].arrayValue
            if avg_ratting.count != 0
            {
                let ProductRatting = formatterRatting.string(for: avg_ratting[0]["avg_ratting"].stringValue.toDouble)
                cell.lbl_ratingcount.text = "\(ProductRatting!)"
            }
            else{
                cell.lbl_ratingcount.text = "0.0"
            }
            if is_wishlist == "1"
            {
                cell.btn_favorite.setImage(UIImage(named: "ic_heartfill"), for: .normal)
                cell.btn_favorite.tintColor = UIColor.red
                
            }
            else{
                cell.btn_favorite.setImage(UIImage(named: "ic_heart"), for: .normal)
                cell.btn_favorite.tintColor = UIColor.darkGray
            }
            if is_variation == "1"
            {
                let ProductPrice = formatter.string(for: variation["price"]!.stringValue.toDouble)
                let ProductDiscountPrice = formatter.string(for: variation["discounted_variation_price"]!.stringValue.toDouble)
                if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                {
                    cell.lbl_itemprice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    cell.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
                }
                else {
                    cell.lbl_itemprice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                    cell.lbl_DiscountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
                }
                
            }
            else{
                let ProductPrice = formatter.string(for: data["product_price"].stringValue.toDouble)
                let ProductDiscountPrice = formatter.string(for: data["discounted_price"].stringValue.toDouble)
                if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                {
                    cell.lbl_itemprice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    cell.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
                }
                else{
                    cell.lbl_itemprice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                    cell.lbl_DiscountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
                }
                
            }
            cell.lbl_itemName.text = data["product_name"].stringValue
            
            
            cell.img_item.sd_imageTransition = .fade
            let items = indexPath.item % 6
            cell.img_item.sd_setImage(with: URL(string: productimage["image_url"]!.stringValue), placeholderImage: UIImage(named: "b_0\(items)"))
            
            cell.btn_favorite.tag = indexPath.row
            cell.btn_favorite.addTarget(self, action:#selector(btnTap_Favorites_TrendingNow), for: .touchUpInside)
            
            return cell
        }
        else if collectionView == self.CollectionView_Brand {
            
            let cell = self.CollectionView_Brand.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
            cornerRadius(viewName: cell.cell_view, radius: 6.0)
            let data = self.BrandsArray[indexPath.item]
            cell.lbl_name.text! = data["brand_name"].stringValue
            cell.img_Category.sd_imageTransition = .fade
            let items = indexPath.item % 6
            cell.img_Category.sd_setImage(with: URL(string: data["image_url"].stringValue), placeholderImage: UIImage(named: "b_0\(items)"))
            cornerRadius(viewName: cell.cell_view, radius: cell.cell_view.frame.height / 2)
            if items == 0 {
                cell.cell_view.backgroundColor = APP_COLOR1
            }
            else if items == 1 {
                cell.cell_view.backgroundColor = APP_COLOR2
            }
            else if items == 2 {
                cell.cell_view.backgroundColor = APP_COLOR3
            }
            else if items == 3 {
                cell.cell_view.backgroundColor = APP_COLOR4
            }
            else if items == 4 {
                cell.cell_view.backgroundColor = APP_COLOR5
            }
            else if items == 5 {
                cell.cell_view.backgroundColor = APP_COLOR6
            }
            return cell
        }
        
        else if collectionView == self.CollectionView_BrandMostPopular {
            let cell = self.CollectionView_BrandMostPopular.dequeueReusableCell(withReuseIdentifier: "MostpopularCell", for: indexPath) as! MostpopularCell
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.cornerRadius = 8.0
            cell.layer.borderWidth = 0.4
            let data = self.HotProductsArray[indexPath.item]
            let productimage = data["productimage"].dictionaryValue
            let variation = data["variation"].dictionaryValue
            cell.lbl_itemName.text = data["product_name"].stringValue
            cell.img_item.sd_imageTransition = .fade
            let items = indexPath.item % 6
            cell.img_item.sd_setImage(with: URL(string: productimage["image_url"]!.stringValue), placeholderImage: UIImage(named: "b_0\(items)"))
            
            let is_variation = data["is_variation"].stringValue
            let is_wishlist = data["is_wishlist"].stringValue
            let avg_ratting = data["rattings"].arrayValue
            if avg_ratting.count != 0
            {
                let ProductRatting = formatterRatting.string(for: avg_ratting[0]["avg_ratting"].stringValue.toDouble)
                cell.lbl_ratingcount.text = "\(ProductRatting!)"
            }
            else{
                cell.lbl_ratingcount.text = "0.0"
            }
            if is_wishlist == "1"
            {
                cell.btn_favorite.setImage(UIImage(named: "ic_heartfill"), for: .normal)
                cell.btn_favorite.tintColor = UIColor.red
                
            }
            else{
                cell.btn_favorite.setImage(UIImage(named: "ic_heart"), for: .normal)
                cell.btn_favorite.tintColor = UIColor.darkGray
            }
            if is_variation == "1"
            {
                let ProductPrice = formatter.string(for: variation["price"]?.stringValue.toDouble)
                let ProductDiscountPrice = formatter.string(for: variation["discounted_variation_price"]?.stringValue.toDouble)
                if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                {
                    cell.lbl_itemprice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    cell.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
                }
                else {
                    cell.lbl_itemprice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                    cell.lbl_DiscountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
                }
                
            }
            else{
                let ProductPrice = formatter.string(for: data["product_price"].stringValue.toDouble)
                let ProductDiscountPrice = formatter.string(for: data["discounted_price"].stringValue.toDouble)
                if UserDefaultManager.getStringFromUserDefaults(key: UD_currency_position) == "right"
                {
                    cell.lbl_itemprice.text = "\(ProductPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))"
                    cell.lbl_DiscountPrice.attributedText = "\(ProductDiscountPrice!)\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))".strikethrough
                }
                else{
                    cell.lbl_itemprice.text = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductPrice!)"
                    cell.lbl_DiscountPrice.attributedText = "\(UserDefaultManager.getStringFromUserDefaults(key: UD_currency))\(ProductDiscountPrice!)".strikethrough
                }
                
                
            }
            cell.btn_favorite.tag = indexPath.row
            cell.btn_favorite.addTarget(self, action:#selector(btnTap_Favorites_BrandMostPopular), for: .touchUpInside)
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.CollectionView_Banner{
            return CGSize(width: UIScreen.main.bounds.width, height: 230)
        }
        else if collectionView == self.CollectionView_Categories{
            return CGSize(width: 80, height: 110)
        }
        else if collectionView == self.CollectionView_SubCategories{
            return CGSize(width: 120, height: 120)
        }
        else if collectionView == self.CollectionView_Mostpopular{
            //            return CGSize(width: (UIScreen.main.bounds.width - 42.0) / 1.5, height: ((UIScreen.main.bounds.width - 42.0) / 1.5) * 1.75)
            return CGSize(width: (UIScreen.main.bounds.width - 30.0) / 2, height: ((UIScreen.main.bounds.width - 30.0) / 2) * 1.90)
        }
        else if collectionView == self.CollectionView_Banner2
        {
            return CGSize(width: UIScreen.main.bounds.width, height: 200)
        }
        else if collectionView == self.CollectionView_Banner3
        {
            return CGSize(width: UIScreen.main.bounds.width, height: 200)
        }
        else if collectionView == self.CollectionView_VendorsList{
            return CGSize(width: 120, height: 120)
        }
        else if collectionView == self.CollectionView_TrendingNow{
            return CGSize(width: (UIScreen.main.bounds.width - 30.0) / 2, height: ((UIScreen.main.bounds.width - 30.0) / 2) * 1.90)
        }
        else if collectionView == self.CollectionView_Brand{
            return CGSize(width: 100, height: 100)
        }
        else if collectionView == self.CollectionView_BrandMostPopular{
            return CGSize(width: (UIScreen.main.bounds.width - 30.0) / 2, height: ((UIScreen.main.bounds.width - 30.0) / 2) * 1.90)
        }
        else
        {
            return CGSize.zero
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.CollectionView_Mostpopular
        {
            
            let data = self.FeaturedProductsArray[indexPath.item]
            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
            vc.item_id = data["id"].stringValue
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        }
        else if collectionView == self.CollectionView_TrendingNow
        {
            let data = self.NewproductsArray[indexPath.item]
            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
            vc.item_id = data["id"].stringValue
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        }
        else if collectionView == self.CollectionView_BrandMostPopular
        {
            let data = self.HotProductsArray[indexPath.item]
            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
            vc.item_id = data["id"].stringValue
            
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        }
        else if collectionView == self.CollectionView_VendorsList
        {
            let data = self.VendorsArray[indexPath.item]
            let vc = self.storyboard?.instantiateViewController(identifier: "StoreitemListVC") as! StoreitemListVC
            vc.vendor_id = data["id"].stringValue
            vc.StoreName = data["name"].stringValue
            vc.imageURL = data["image_url"].stringValue
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        }
        else if collectionView == self.CollectionView_Brand
        {
            let data = self.BrandsArray[indexPath.item]
            let vc = self.storyboard?.instantiateViewController(identifier: "BrandWiseitemVC") as! BrandWiseitemVC
            vc.Brand_id = data["id"].stringValue
            vc.StoreName = data["brand_name"].stringValue
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
        }
        else if collectionView == self.CollectionView_Categories
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewallitemVC") as! ViewallitemVC
            vc.innersubcategory_id = self.MainCategoryArray[indexPath.row]["id"]!
            vc.istype = self.MainCategoryArray[indexPath.row]["innersubcategory_name"]!
            vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
        }
        else if collectionView == self.CollectionView_SubCategories
        {
            self.selectedSection = indexPath.row
            self.MainCategoryArray.removeAll()
            self.MainCategoryArray.append(contentsOf: self.sections[self.selectedSection].subcategory)
            CollectionView_Categories.reloadData()
        }
        else if collectionView == CollectionView_Banner2
        {
            
            let data = self.TopBannerListArray[indexPath.item]
            if data["type"].stringValue == "category"
            {
                let vc = self.storyboard?.instantiateViewController(identifier: "SubCategoryVC") as! SubCategoryVC
                vc.cat_id = data["cat_id"].stringValue
                vc.category_name = data["category_name"].stringValue
                vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
            }
            else{
                let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
                vc.item_id = data["product_id"].stringValue
                vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
            }
            
        }
        else if collectionView == CollectionView_Banner
        {
            
            let data = self.SilderBanner[indexPath.item]
            guard let url = URL(string: data["link"].stringValue) else { return }
            UIApplication.shared.open(url)
            
        }
        else if collectionView == CollectionView_Banner3
        {
            
            let data = self.CenterBannerListArray[indexPath.item]
            if data["type"].stringValue == "category"
            {
                let vc = self.storyboard?.instantiateViewController(identifier: "SubCategoryVC") as! SubCategoryVC
                vc.cat_id = data["cat_id"].stringValue
                vc.category_name = data["category_name"].stringValue
                vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
            }
            else{
                let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
                vc.item_id = data["product_id"].stringValue
                vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false)
            }
            
        }
        
    }
    @objc func btnTap_Favorites_TrendingNow(sender:UIButton!) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A"
        {
            UserDefaults.standard.set("", forKey:UD_userId)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            UIApplication.shared.windows[0].rootViewController = nav
        }
        else
        {
            let data = self.NewproductsArray[sender.tag]
            let is_wishlist = data["is_wishlist"].stringValue
            if is_wishlist == "1"
            {
                let urlString = API_URL + "removefromwishlist"
                let params: NSDictionary = ["product_id":data["id"].stringValue,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_RemoveWishList(url: urlString,params: params, sender: sender.tag, isSelect: "Newproducts")
            }
            else{
                let urlString = API_URL + "addtowishlist"
                let params: NSDictionary = ["product_id":data["id"].stringValue,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_AddtoWishList(url: urlString,params: params, sender: sender.tag, isSelect: "Newproducts")
            }
        }
    }
    @objc func btnTap_Favorites_BrandMostPopular(sender:UIButton!) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A"
        {
            UserDefaults.standard.set("", forKey:UD_userId)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            UIApplication.shared.windows[0].rootViewController = nav
        }
        else
        {
            let data = self.HotProductsArray[sender.tag]
            let is_wishlist = data["is_wishlist"].stringValue
            if is_wishlist == "1"
            {
                let urlString = API_URL + "removefromwishlist"
                let params: NSDictionary = ["product_id":data["id"].stringValue,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_RemoveWishList(url: urlString,params: params, sender: sender.tag, isSelect: "HotProducts")
            }
            else{
                let urlString = API_URL + "addtowishlist"
                let params: NSDictionary = ["product_id":data["id"].stringValue,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_AddtoWishList(url: urlString,params: params, sender: sender.tag, isSelect: "HotProducts")
            }
        }
    }
    
    @objc func btnTap_Favorites_Mostpopular(sender:UIButton!) {
        if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" || UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "N/A"
        {
            UserDefaults.standard.set("", forKey:UD_userId)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            UIApplication.shared.windows[0].rootViewController = nav
        }
        else
        {
            let data = self.FeaturedProductsArray[sender.tag]
            let is_wishlist = data["is_wishlist"].stringValue
            if is_wishlist == "1"
            {
                let urlString = API_URL + "removefromwishlist"
                let params: NSDictionary = ["product_id":data["id"].stringValue,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_RemoveWishList(url: urlString,params: params, sender: sender.tag, isSelect: "FeaturedProduct")
            }
            else{
                let urlString = API_URL + "addtowishlist"
                let params: NSDictionary = ["product_id":data["id"].stringValue,"user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                self.Webservice_AddtoWishList(url: urlString,params: params, sender: sender.tag, isSelect: "FeaturedProduct")
            }
        }
    }
}
//MARK: Webservices
extension HomeVC
{
    func Webservice_GetTopBanner(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "GET", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                print(jsonResponse!)
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    self.TopBannerListArray = jsonResponse!["topbanner"].arrayValue
                    self.CenterBannerListArray = jsonResponse!["bottombanner"].arrayValue
                    self.largeBanner = jsonResponse!["largebanner"].arrayValue
                    self.leftbanner = jsonResponse!["leftbanner"].arrayValue
                    self.SilderBanner = jsonResponse!["sliders"].arrayValue
                    let popupbanner = jsonResponse!["popupbanner"].arrayValue
                    
                    if self.largeBanner.count != 0
                    {
                        self.img_LargeBanner.sd_imageTransition = .fade
                        self.img_LargeBanner.sd_setImage(with: URL(string: self.largeBanner[0]["image_url"].stringValue), placeholderImage: UIImage(named: "b_00"))
                    }
                    if self.leftbanner.count != 0
                    {
                        self.img_CenterBanner.sd_imageTransition = .fade
                        self.img_CenterBanner.sd_setImage(with: URL(string: self.leftbanner[0]["image_url"].stringValue), placeholderImage: UIImage(named: "b_00"))
                    }
                    if popupbanner.count != 0
                    {
                        if UserDefaultManager.getStringFromUserDefaults(key:UD_DismisHomeBanner) == "" ||   UserDefaultManager.getStringFromUserDefaults(key:UD_DismisHomeBanner) == "N/A"
                        {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpBannerVC") as! PopUpBannerVC
                            vc.modalPresentationStyle = .overFullScreen
                            vc.modalTransitionStyle = .crossDissolve
                            vc.imgurl = popupbanner[0]["image_url"].stringValue
                            self.present(vc,animated: true,completion: nil)
                        }
                    }
                    self.CollectionView_Banner.delegate = self
                    self.CollectionView_Banner.dataSource = self
                    self.CollectionView_Banner.reloadData()
                    
                    self.CollectionView_Banner2.delegate = self
                    self.CollectionView_Banner2.dataSource = self
                    self.CollectionView_Banner2.reloadData()
                    
                    self.CollectionView_Banner3.delegate = self
                    self.CollectionView_Banner3.dataSource = self
                    self.CollectionView_Banner3.reloadData()
                    
                    // MARK:- Category Api
                    let urlString = API_URL + "category"
                    self.Webservice_GetCategory(url: urlString,params: [:])
                }
                else if responseCode == "2" {
                    
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_GetCategory(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "GET", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            print(jsonResponse)
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    self.CollectionView_Categories.delegate = self
                    self.CollectionView_Categories.dataSource = self
                    self.CollectionView_Categories.reloadData()
                    self.cat_id = jsonResponse!["data"].arrayValue.first?["id"].stringValue ?? ""
                    self.Webservice_GetSubCategory()
                    let urlString = API_URL + "homefeeds"
                    let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId)]
                    self.Webservice_HomeFeedapi(url: urlString,params: params)
                }
                else if responseCode == "2" {
                    
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_HomeFeedapi(url:String, params:NSDictionary) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    
                    UserDefaultManager.setStringToUserDefaults(value: jsonResponse!["currency"].stringValue, key: UD_currency)
                    UserDefaultManager.setStringToUserDefaults(value: jsonResponse!["currency_position"].stringValue, key: UD_currency_position)
                    UserDefaultManager.setStringToUserDefaults(value: jsonResponse!["referral_amount"].stringValue, key: UD_referral_amount)
                    self.FeaturedProductsArray = jsonResponse!["featured_products"].arrayValue
                    self.HotProductsArray = jsonResponse!["hot_products"].arrayValue
                    self.NewproductsArray = jsonResponse!["new_products"].arrayValue
                    self.BrandsArray = jsonResponse!["brands"].arrayValue
                    self.VendorsArray = jsonResponse!["vendors"].arrayValue
                    //                    print(responseData)
                    if jsonResponse!["notifications"].stringValue == "0"
                    {
                        self.img_Reddot.isHidden = true
                    }
                    else
                    {
                        self.img_Reddot.isHidden = false
                    }
                    self.CollectionView_Mostpopular.delegate = self
                    self.CollectionView_Mostpopular.dataSource = self
                    self.CollectionView_Mostpopular.reloadData()
                    self.CollectionView01_Height.constant = ((UIScreen.main.bounds.width - 30.0) / 2) * 1.90
                    
                    self.CollectionView_TrendingNow.delegate = self
                    self.CollectionView_TrendingNow.dataSource = self
                    self.CollectionView_TrendingNow.reloadData()
                    self.CollectionView02_Height.constant = ((UIScreen.main.bounds.width - 30.0) / 2) * 1.90
                    
                    
                    self.CollectionView_BrandMostPopular.delegate = self
                    self.CollectionView_BrandMostPopular.dataSource = self
                    self.CollectionView_BrandMostPopular.reloadData()
                    
                    self.CollectionView03_Height.constant = ((UIScreen.main.bounds.width - 30.0) / 2) * 1.90
                    
                    self.CollectionView_VendorsList.delegate = self
                    self.CollectionView_VendorsList.dataSource = self
                    self.CollectionView_VendorsList.reloadData()
                    
                    self.CollectionView_Brand.delegate = self
                    self.CollectionView_Brand.dataSource = self
                    self.CollectionView_Brand.reloadData()
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_AddtoWishList(url:String, params:NSDictionary,sender:Int,isSelect:String) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    if isSelect == "FeaturedProduct"
                    {
                        var data = self.FeaturedProductsArray[sender]
                        data["is_wishlist"].stringValue = "1"
                        self.FeaturedProductsArray.remove(at: sender)
                        self.FeaturedProductsArray.insert(data, at: sender)
                        self.CollectionView_Mostpopular.reloadData()
                    }
                    else if isSelect == "Newproducts"
                    {
                        var data = self.NewproductsArray[sender]
                        data["is_wishlist"].stringValue = "1"
                        self.NewproductsArray.remove(at: sender)
                        self.NewproductsArray.insert(data, at: sender)
                        self.CollectionView_TrendingNow.reloadData()
                    }
                    else if isSelect == "HotProducts"
                    {
                        var data = self.HotProductsArray[sender]
                        data["is_wishlist"].stringValue = "1"
                        self.HotProductsArray.remove(at: sender)
                        self.HotProductsArray.insert(data, at: sender)
                        self.CollectionView_BrandMostPopular.reloadData()
                    }
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    func Webservice_RemoveWishList(url:String, params:NSDictionary,sender:Int,isSelect:String) -> Void {
        WebServices().CallGlobalAPI(url: url, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    if isSelect == "FeaturedProduct"
                    {
                        var data = self.FeaturedProductsArray[sender]
                        data["is_wishlist"].stringValue = "0"
                        self.FeaturedProductsArray.remove(at: sender)
                        self.FeaturedProductsArray.insert(data, at: sender)
                        self.CollectionView_Mostpopular.reloadData()
                    }
                    else if isSelect == "Newproducts"
                    {
                        var data = self.NewproductsArray[sender]
                        data["is_wishlist"].stringValue = "0"
                        self.NewproductsArray.remove(at: sender)
                        self.NewproductsArray.insert(data, at: sender)
                        self.CollectionView_TrendingNow.reloadData()
                    }
                    else if isSelect == "HotProducts"
                    {
                        var data = self.HotProductsArray[sender]
                        data["is_wishlist"].stringValue = "0"
                        self.HotProductsArray.remove(at: sender)
                        self.HotProductsArray.insert(data, at: sender)
                        self.CollectionView_BrandMostPopular.reloadData()
                    }
                    
                }
                
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
    
    func Webservice_GetSubCategory() -> Void {
        let urlString = API_URL + "subcategory"
        let params: NSDictionary = ["cat_id":self.cat_id]
        WebServices().CallGlobalAPI(url: urlString, headers: [:], parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ strErrorMessage:String) in
            
            if strErrorMessage.count != 0 {
                showAlertMessage(titleStr: "", messageStr: strErrorMessage)
            }
            else {
                let responseCode = jsonResponse!["status"].stringValue
                if responseCode == "1" {
                    let AllSubcategoryArray = jsonResponse!["data"]["subcategory"].arrayValue
                    
                    for data in AllSubcategoryArray
                    {
                        var innersubcategory = [[String:String]]()
                        for innercate in data["innersubcategory"].arrayValue
                        {
                            let obj = ["innersubcategory_name":innercate["innersubcategory_name"].stringValue, "innersubcategoryImage":innercate["innersubcategory_image"].stringValue,"id":innercate["id"].stringValue]
                            innersubcategory.append(obj)
                        }
                        
                        self.sections.append(contentsOf: [Section(category: data["subcategory_name"].stringValue, categoryImage: data["subcategory_image"].stringValue, subcategory: innersubcategory, expanded: false)])
                        self.MainCategoryArray.removeAll()
                        self.MainCategoryArray.append(contentsOf: self.sections[self.selectedSection].subcategory)
                    }
                    self.CollectionView_Categories.reloadData()
                    self.CollectionView_SubCategories.reloadData()
                }
                else if responseCode == "2" {
                  
                }
                else {
                    showAlertMessage(titleStr: "", messageStr: jsonResponse!["message"].stringValue)
                }
            }
        }
    }
}
