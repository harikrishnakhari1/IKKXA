//
//  TabBarVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 24/04/21.
//

import UIKit

class TabBarVC: UITabBarController,UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
        let backgroundView = UIView(frame: tabBar.frame)

        backgroundView.backgroundColor = UIColor.red

        self.tabBar.insertSubview(backgroundView, at: 0)

        // Do any additional setup after loading the view.
    }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        let selectedIndex = tabBarController.viewControllers?.firstIndex(of: viewController)!
        let selectedIndexlast = tabBarController.viewControllers?.lastIndex(of: viewController)!
        
        if selectedIndex == 0 {
            print("first tab bar was selected")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            let TabViewController = storyBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
            
            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
            appNavigation.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.windows[0].rootViewController = TabViewController
        }
        
//        else if selectedIndexlast == 4 {
//            print("first tab bar was selected")
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let objVC = storyBoard.instantiateViewController(withIdentifier: "MyAccountVC") as! MyAccountVC
//            self.present(objVC, animated: true)
////            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
////            appNavigation.setNavigationBarHidden(true, animated: true)
////            UIApplication.shared.windows[0].rootViewController = appNavigation
////            let TabViewController = storyBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
////            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
////            appNavigation.setNavigationBarHidden(true, animated: true)
////            UIApplication.shared.windows[0].rootViewController = TabViewController
//        }
        else {
            //do whatever
        }
    }
    

}
