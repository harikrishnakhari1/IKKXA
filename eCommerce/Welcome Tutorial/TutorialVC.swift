//
//  TutorialVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 24/04/21.
//

import UIKit

//MARK: Tutorial Collection cell
class TutorialCollectionCell: UICollectionViewCell {
    @IBOutlet weak var imgTutorial: UIImageView!
}

class TutorialVC: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var collection_tutorial: UICollectionView!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    //MARK: Variables
    var tutorialArr = [["image":"s1"],["image":"s2"],["image":"s3"],["image":"s4"]]
    //MARK: Viewcontroller lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnSkip.setTitle("   "+"Skip".localiz()+"   ", for: .normal)
        cornerRadius(viewName: self.btnSkip, radius: 8.0)
    }
}

//MARK: Actions
extension TutorialVC {
    @IBAction func btnSkip_Clicked(_ sender: UIButton) {
        UserDefaults.standard.set("1", forKey: UD_isTutorial)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        UIApplication.shared.windows[0].rootViewController = nav
    }
}

//MARK: Collectionview methods
extension TutorialVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tutorialArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collection_tutorial.dequeueReusableCell(withReuseIdentifier: "TutorialCollectionCell", for: indexPath) as! TutorialCollectionCell
        cell.imgTutorial.image = UIImage(named: self.tutorialArr[indexPath.item]["image"]!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height + 20.0)
    }
}

//MARK: Functions
extension TutorialVC {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = self.collection_tutorial.contentOffset
        visibleRect.size = self.collection_tutorial.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = self.collection_tutorial.indexPathForItem(at: visiblePoint) else { return }
        self.pageControl.currentPage = indexPath.item
        if indexPath.item == 3 {
            self.btnSkip.setTitle("   "+"Start".localiz()+"   ", for: .normal)
        }
        else {
            self.btnSkip.setTitle("   "+"Skip".localiz()+"   ", for: .normal)
        }
    }
}
