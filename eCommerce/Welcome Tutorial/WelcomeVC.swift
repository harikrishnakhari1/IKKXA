//
//  WelcomeVC.swift
//  eCommerce
//
//  Created by GRAVITY_INFOTECH on 24/04/21.
//

import UIKit

class WelcomeVC: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var btn_skip: UIButton!
    @IBOutlet weak var btn_login: UIButton!
    @IBOutlet weak var btn_signup: UIButton!
    @IBOutlet weak var lbl_strnoaacount: UILabel!
    @IBOutlet weak var lbl_strwelcome: UILabel!
    
    //MARK: Viewcontroller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer!.isEnabled = false
        cornerRadius(viewName: self.btn_skip, radius: 8.0)
        cornerRadius(viewName: self.btn_login, radius: 8.0)
        self.lbl_strwelcome.text = "You stay home we are always here".localiz()
        self.btn_login.setTitle("LOGIN".localiz(), for: .normal)
        self.btn_skip.setTitle("SKIP & CONTINUE".localiz(), for: .normal)
        self.btn_signup.setTitle("Signup".localiz(), for: .normal)
        self.lbl_strnoaacount.text = "Don't have an account?".localiz()
    }
}

//MARK: Actions
extension WelcomeVC {
    @IBAction func btnTap_Signup(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "SignupVC") as! SignupVC
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
    
    @IBAction func btnTap_skip(_ sender: UIButton) {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userName)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        objVC.modalPresentationStyle = .fullScreen
        self.present(objVC, animated: true)
    }
    
    @IBAction func btnTap_Login(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "LoginVC") as! LoginVC
        vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: false)
    }
}
